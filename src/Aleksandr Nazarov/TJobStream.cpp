#include "TJobStream.h"

bool TJobStream::StatsValid(Statistic const *Stats)
{
	if ( !Stats->Valid || Stats->NumberOfJob < 0 || Stats->ChanceToCreateNewJob > 100 || Stats->ChanceToCreateNewJob <= 0
		|| Stats->HzOfProc <= 0 || Stats->MaxDurabilityOfJob < 0 || Stats->MinDurabilityOfJob < 0
		|| Stats->MaxDurabilityOfJob < Stats->MinDurabilityOfJob || Stats->QueueQuality <= 0 ||
		Stats->CountElementsInQueue < 0 || Stats->NumberOfJobInProc < 0 || Stats->HowLong <= 0 ||
		Stats->TimeWithNoJob < 0 || Stats->TimeWithJob < 0 || Stats->ProcTime < 0 || Stats->CountOfLostJobs < 0 ||
		Stats->QueueQuality < 0 || Stats->CountElementsInQueue < 0 || Stats->ChanceToDoneJob <= 0 || Stats->ChanceToDoneJob > 100  ||
		Stats->MinChanceToDoneJob <= 0 || Stats->MaxChanceToDoneJob <= 0 || Stats->MaxChanceToDoneJob < Stats->MinChanceToDoneJob ||
		Stats->MinChanceToDoneJob > 100 || Stats->MaxChanceToDoneJob > 100)
		return 0;

	return 1;
}

void TJobStream::PrintStats(Statistic const *Stats)
{
	cout << "���������� ������� ��� ����������: " << Stats->NumberOfJob << endl;
	cout << "������������������ ����������(� Hz): " << Stats->HzOfProc << endl;
	cout << "����� ������� ������ ����������(� ������): " << Stats->HowLong << endl;
	cout << "������������ ������������ ������� ����������(� ������): " << Stats->MaxDurabilityOfJob << endl;
	cout << "����������� ������������ ������� ����������(� ������): " << Stats->MinDurabilityOfJob << endl;
	cout << "����� ����� ������� ����������(� ������): " << Stats->TimeWithNoJob << endl;
	cout << "����� ����� ������ ���������� ��� �������(� ������): " << Stats->TimeWithJob << endl;
	cout << "����� ����� ������ ����������(� ������): " << Stats->ProcTime << endl;
	cout << "���������� ���������� ����� ��� ����������: " << Stats->CountOfLostJobs << endl;
	cout << "������ �������: " << Stats->QueueQuality << endl;
	cout << "���������� ����� � ������� � ������ ������: " << Stats->CountElementsInQueue << endl;
	cout << "����� ������� � ���������� � ������ ������: " << Stats->NumberOfJobInProc << endl;
	cout << "���� ������ ������� ����������(� %): " << Stats->ChanceToCreateNewJob << endl;
	cout << "���� ��������� ������� ��� ������� ��������(� %): " << Stats->ChanceToDoneJob << endl;
	cout << "����������� ���� �� ���������� ��� �������(� %): " << Stats->MinChanceToDoneJob << endl;
	cout << "������������ ���� �� ���������� ��� �������(� %): " << Stats->MaxChanceToDoneJob << endl;
	if (Stats->NumberOfJobInProc != 0)
		cout << "������� ������������ �������: " << ((int)(Stats->TimeWithJob / Stats->NumberOfJobInProc)) << endl;
	if (Stats->NumberOfJob != 0)
	{
		double temp = (((double)Stats->CountOfLostJobs / (double)Stats->NumberOfJob) * (double)100);
		cout << "������� �������: " << (int)temp << endl;
	}
	else
		cout << "������� �������: 0" << endl;
	if (Stats->ProcTime != 0)
	{
		double temp = (((double)Stats->TimeWithNoJob / (double)Stats->ProcTime) * (double)100);
		cout << "������� �������: " << (int)temp << endl;
	}
	else
		cout << "������� �������: 0" << endl;
}

double TJobStream::StrToDouble(string str)
{
	double result = 0;
	int i;
	bool point = false;
	for (i = 0; i < str.length(); i++)
	{
		if ((str.at(i) == '.') && point) throw "wrong value";
		if (str.at(i) == '.') point = true;
	}
	if (point)
	{
		for (int p = 1, i = (str.find('.') + 1); i < str.length(); i++, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, -p);
			else
				throw "wrong value";
		}
		for (int p = 0, i = (str.find('.') - 1); i >= 0; i--, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, p);
			else
				throw "wrong value";
		}
	}
	else
		for (int p = 0, i = str.length() - 1; i >= 0; i--, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, p);
			else
				throw "wrong value";
		}
	return result;
}

void TJobStream::GenerateNewJob(int Chance)
{
	if ((rand() % 100) <= Chance)
	{
		if (Jobs.IsFull())
		{
			Stats->CountOfLostJobs++;
		}
		else
		{
			int temp;
			if (Stats->MaxChanceToDoneJob != Stats->MinChanceToDoneJob)
				temp = (rand() % (Stats->MaxChanceToDoneJob - Stats->MinChanceToDoneJob)) + Stats->MinChanceToDoneJob;
			else
				temp = Stats->MinChanceToDoneJob;
			if (temp == 0)
				temp++;
			Jobs.Put(temp);
			Stats->CountElementsInQueue++;
		}
		Stats->NumberOfJob++;
	}
}

void TJobStream::RunJob()
{
	cout << "��������..." << endl;
	int Durability;
	Stats->NumberOfJob = Stats->MaxDurabilityOfJob = Stats->MinDurabilityOfJob = Stats->TimeWithNoJob = Stats->TimeWithJob =
		Stats->ProcTime = Stats->CountElementsInQueue = Stats->CountOfLostJobs = Stats->NumberOfJobInProc = 0;
	TProc Proc(Stats->HzOfProc);

	if (!StatsValid())
		throw "Statistic or setting have a error(s)";

	while (Stats->ProcTime < Stats->HowLong)
	{
		if (!Jobs.IsEmpty())
		{
			Durability = 0;
			Stats->CountElementsInQueue--;
			Stats->NumberOfJobInProc++;
			Stats->ChanceToDoneJob = Jobs.Get();
			while (!Proc.RunProc(Stats->ChanceToDoneJob))
			{
				Durability++;
				Stats->TimeWithJob++;
				Stats->ProcTime++;
				GenerateNewJob();
			}
			Stats->TimeWithJob++;
			Durability++;
			if (Stats->MinDurabilityOfJob == 0)
				Stats->MinDurabilityOfJob = Durability;
			if (Stats->MinDurabilityOfJob > Durability)
				Stats->MinDurabilityOfJob = Durability;
			if (Stats->MaxDurabilityOfJob < Durability)
				Stats->MaxDurabilityOfJob = Durability;
		}
		else
			Stats->TimeWithNoJob++;
		Stats->ProcTime++;
		GenerateNewJob();
	}
	cout << "������" << endl;
}