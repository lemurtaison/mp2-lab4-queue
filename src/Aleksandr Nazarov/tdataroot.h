// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tdataroot.h - Copyright (c) Гергель В.П. 28.07.2000 (06.08)
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - базовый (абстрактный) класс - версия 3.2
//   память выделяется динамически или задается методом SetMem

#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"
#include <iostream>

using namespace std;

#define DefMemSize   25  // размер памяти по умолчанию

#define DataEmpty  -101  // СД пуста
#define DataFull   -102  // СД переполнена
#define DataNoMem  -103  // нет памяти

enum TMemType { MEM_HOLDER, MEM_RENTER };

template <class ValType>
class TDataRoot : public TDataCom
{
protected:
	ValType* pMem;      // память для СД
	int MemSize;      // размер памяти для СД
	int DataCount;    // количество элементов в СД
	TMemType MemType; // режим управления памятью

	void SetMem(void *p, int Size);             // задание памяти
public:
	virtual ~TDataRoot();
	TDataRoot(int Size = DefMemSize);
	virtual bool IsEmpty(void) const;           // контроль пустоты СД
	virtual bool IsFull(void) const;           // контроль переполнения СД
	virtual void  Put(const ValType &Val) = 0; // добавить значение
	virtual ValType Get(void) = 0; // извлечь значение

								 // служебные методы
	virtual int  IsValid() = 0;                 // тестирование структуры
	virtual void Print() = 0;                 // печать значений

											  // дружественные классы
	friend class TMultiStack;
	friend class TSuperMultiStack;
	friend class TComplexMultiStack;
};

template <class ValType>
void TDataRoot<ValType>::SetMem(void *p, int Size) // задание памяти
{
	if (Size <= 0) throw "wrong size to setmem";
	if (MemType != MEM_RENTER)
	{
		MemType = MEM_RENTER;
		delete[] pMem;
	}
	pMem = (TElem *)p;
	MemSize = Size;
}

template <class ValType>
TDataRoot<ValType>::TDataRoot(int Size) : TDataCom()
{
	if (Size == 0)
	{
		MemType = MEM_RENTER;
		pMem = nullptr;
		MemSize = 0;
	}
	else
	{
		MemType = MEM_HOLDER;
		if (Size < 0) throw "wrong size of data";
		DataCount = 0;
		MemSize = Size;
		pMem = nullptr;
		pMem = new ValType[Size];
		if (pMem == nullptr) throw "not enough memory to build data";
		memset(pMem, 0, sizeof(ValType)*MemSize);
	}
}

template <class ValType>
TDataRoot<ValType>::~TDataRoot()
{
	if (MemType == MEM_HOLDER) delete[] pMem;
	else pMem = nullptr;
}

template <class ValType>
bool TDataRoot<ValType>::IsEmpty(void) const   // контроль пустоты СД
{
	return DataCount == 0;
}

template <class ValType>
bool TDataRoot<ValType>::IsFull(void) const    // контроль переполнения СД
{
	return DataCount == MemSize;
}


#endif