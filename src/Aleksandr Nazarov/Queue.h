
#include "..\..\mp2-lab3-stack\mp2-lab3-stack\TDataRoot.h"
#include <iostream>

using namespace std;

enum QueueMemType { MEM_STATIC, MEM_DYNAMIC };

template<class ValType>
class Queue : public TDataRoot<ValType>
{
protected:
	int Li, Hi;                   //��������� �� ������ � ����� �������
	int MaxMem;					  //������������ ���������� ������, ������� ����� �������� ������� 
	int STDDATASETMEM;			  //�����, �� ������� ������������� ������ ������, ���� ������������ ������������ ���������� �������
	QueueMemType QMemType;        //��� ������������� ������(MEM_STATIC - �����������, MEM_DYNAMIC - ������������)
	void Reconstruction(int Size);//����� ������������� ���������� ������
	int GetNextIndex(int &index); //����� ������ ���������� �������
public:
	Queue(int MaxMem = INT_MAX, QueueMemType QMemType = MEM_DYNAMIC);  //����������� �����������, �������� MaxMem - ������������ ���������� ������, ������� ����� �������� �������, QMemType - ��� ������������� ������
	Queue(const Queue<ValType> &q);									   //����������� �����������
	void Put(const ValType &val);										
	ValType Get();
	int  IsValid() { return !(IsFull() || DataCount > MaxMem || DataCount < 0 || Hi < 0 || Li < 0); }                 // ������������ ���������
	bool IsFull() { return DataCount == MaxMem; }
	void Print()																									  //����� �������� � �������
	{
			for (int i = Li; i == Hi; GetNextIndex(i))
				cout << pMem[i] << " ";
			cout << endl;
	}

};

template<class ValType>
Queue<ValType>::Queue(int MaxMem = INT_MAX, QueueMemType MemType = MEM_DYNAMIC) :
	TDataRoot((MaxMem >= DefMemSize || QMemType == MEM_STATIC) ? DefMemSize : MaxMem), Li(-1), Hi(-1)
{
	if (MaxMem < 0)
		throw "wrong MaxMem";
	(*this).MaxMem = MaxMem;
	STDDATASETMEM = sqrt(MaxMem);
}

template<class ValType>
Queue<ValType>::Queue(const Queue<ValType> &q) : Hi(q.Hi), Li(q.Li), 
TDataRoot(s.Size), DataCount(q.DataCount), STDDATASETMEM(q.STDDATASETMEM)
{
	for (int i = 0; i < MemSize; i++)
		pMem[i] = s.pMem[i];
}

template<class ValType>
void Queue<ValType>::Put(const ValType &val)
{
	if (TDataRoot<ValType>::IsFull() && QMemType == MEM_DYNAMIC)
		if (MaxMem >= MemSize + STDDATASETMEM)
			Reconstruction(STDDATASETMEM);
		else 
			Reconstruction(MaxMem - MemSize);
	if (IsFull())
		throw SetRetCode(DataFull);
	pMem[GetNextIndex(Hi)] = val;
	DataCount++;
}

template<class ValType>
ValType Queue<ValType>::Get()
{
	if (IsEmpty())
		throw SetRetCode(DataEmpty);
	DataCount--;
	return pMem[GetNextIndex(Li)];
}

template<class ValType>
int Queue<ValType>::GetNextIndex(int &index)
{
	if (++index >= MemSize)
		index = 0;
	return index;
}

template<class ValType>
void Queue<ValType>::Reconstruction(int Size)
{
	if (MemType == MEM_HOLDER && MemSize + Size <= MaxMem)
	{
		ValType *Temp;
		Temp = new ValType[MemSize + Size];
		for (int i = 0, f = Li; i < MemSize; i++, GetNextIndex(f))
			Temp[i] = pMem[f];
		delete[] pMem;
		pMem = Temp;
		Li = 0;
		Hi = MemSize - 1;
		MemSize += Size;
	}
	else
		throw SetRetCode(DataFull);
}