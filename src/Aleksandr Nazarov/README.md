# Лабораторная работа №4. Очереди

## Цели и задачи

Лабораторная работа направлена на практическое освоение динамической структуры данных Очередь. С этой целью в лабораторной работе изучаются различные варианты структуры хранения очереди и разрабатываются методы и программы решения задач с использованием очередей. В качестве области приложений выбрана тема эффективной организации выполнения потока заданий на вычислительных системах.

Очередь характеризуется таким порядком обработки значений, при котором вставка новых элементов производится в конец очереди, а извлечение – из начала. Подобная организация данных широко встречается в различных приложениях. В качестве примера использования очереди предлагается задача разработки системы имитации однопроцессорной ЭВМ. Рассматриваемая в рамках лабораторной работы схема имитации является одной из наиболее простых моделей обслуживания заданий в вычислительной системе и обеспечивает тем самым лишь начальное ознакомление с проблемами моделирования и анализа эффективности функционирования реальных вычислительных систем.

Очередь (англ. queue), – схема запоминания информации, при которой каждый вновь поступающий ее элемент занимает крайнее положение (конец очереди). При выдаче информации из очереди выдается элемент, расположенный в очереди первым (начало очереди), а оставшиеся элементы продвигаются к началу; следовательно, элемент, поступивший первым, выдается первым.

##Поставленные задачи

Для вычислительной системы (ВС) с одним процессором и однопрограммным последовательным режимом выполнения поступающих заданий требуется разработать программную систему для имитации процесса обслуживания заданий в ВС. По результатам проводимых вычислительных экспериментов система имитации должна выводить информацию об условиях проведения эксперимента и полученные в результате имитации показатели функционирования вычислительной системы.

##План работы

 - Реализация структуры хранения данных Queue на основе имеющейся структуры TDataRoot.
 - Проверка работоспособности класса Queue с помощью Google Test Framework.
 - Реализация модели имитации системы выполнения задач процессора.
 - Анализ реализованной модели имитации системы выполнения задач процессора на основе полученных результатов.

##Используемые инструменты
 - Система контроля версий Git.
 - Фреймворк для написания автоматических тестов Google Test.
 - Среда разработки Microsoft Visual Studio 2015 Professional Edition.

## Реализация структуры хранения данных Queue на основе имеющейся структуры TDataRoot.
Для реализации очереди используется одномерный динамический или статический массив, в зависимости от параметров передаваемых конструктору. В связи с характером обработки значений, располагаемых в очереди, для указания хранимых в очереди данных необходимо иметь два указателя – на начало и конец очереди. Эти указатели увеличивают свое значение: один при вставке, другой при извлечении элемента. Класс реализуется на шаблонах.

###Файл Queue.h: 

```C++

#include "..\..\mp2-lab3-stack\mp2-lab3-stack\TDataRoot.h"
#include <iostream>

using namespace std;

enum QueueMemType { MEM_STATIC, MEM_DYNAMIC };

template<class ValType>
class Queue : public TDataRoot<ValType>
{
protected:
	int Li, Hi;                   //Указатели на начало и конец очереди
	int MaxMem;					  //Максимальное количество памяти, которое может занимать очередь 
	int STDDATASETMEM;			  //Число, на которое увеличивается размер памяти, если используется динамическая реализация очереди
	QueueMemType QMemType;        //Тип распределения памяти(MEM_STATIC - статический, MEM_DYNAMIC - динамический)
	void Reconstruction(int Size);//Метод динамического увеличения памяти
	int GetNextIndex(int &index); //Метод взятия следующего индекса
public:
	Queue(int MaxMem = INT_MAX, QueueMemType QMemType = MEM_DYNAMIC);  //Стандартный конструктор, параметр MaxMem - максимальное количество памяти, которое может занимать очередь, QMemType - тип распределения памяти
	Queue(const Queue<ValType> &q);									   //Конструктор копирования
	void Put(const ValType &val);										
	ValType Get();
	int  IsValid() { return !(IsFull() || DataCount > MaxMem || DataCount < 0 || Hi < 0 || Li < 0); }                 // тестирование структуры
	bool IsFull() { return DataCount == MaxMem; }
	void Print()																									  //вывод значений в очереди
	{
			for (int i = Li; i == Hi; GetNextIndex(i))
				cout << pMem[i] << " ";
			cout << endl;
	}

};

template<class ValType>
Queue<ValType>::Queue(int MaxMem = INT_MAX, QueueMemType MemType = MEM_DYNAMIC) :
	TDataRoot((MaxMem >= DefMemSize || QMemType == MEM_STATIC) ? DefMemSize : MaxMem), Li(-1), Hi(-1)
{
	if (MaxMem < 0)
		throw "wrong MaxMem";
	(*this).MaxMem = MaxMem;
	STDDATASETMEM = sqrt(MaxMem);
}

template<class ValType>
Queue<ValType>::Queue(const Queue<ValType> &q) : Hi(q.Hi), Li(q.Li), 
TDataRoot(s.Size), DataCount(q.DataCount), STDDATASETMEM(q.STDDATASETMEM)
{
	for (int i = 0; i < MemSize; i++)
		pMem[i] = s.pMem[i];
}

template<class ValType>
void Queue<ValType>::Put(const ValType &val)
{
	if (TDataRoot<ValType>::IsFull() && QMemType == MEM_DYNAMIC)
		if (MaxMem >= MemSize + STDDATASETMEM)
			Reconstruction(STDDATASETMEM);
		else 
			Reconstruction(MaxMem - MemSize);
	if (IsFull())
		throw SetRetCode(DataFull);
	pMem[GetNextIndex(Hi)] = val;
	DataCount++;
}

template<class ValType>
ValType Queue<ValType>::Get()
{
	if (IsEmpty())
		throw SetRetCode(DataEmpty);
	DataCount--;
	return pMem[GetNextIndex(Li)];
}

template<class ValType>
int Queue<ValType>::GetNextIndex(int &index)
{
	if (++index >= MemSize)
		index = 0;
	return index;
}

template<class ValType>
void Queue<ValType>::Reconstruction(int Size)
{
	if (MemType == MEM_HOLDER && MemSize + Size <= MaxMem)
	{
		ValType *Temp;
		Temp = new ValType[MemSize + Size];
		for (int i = 0, f = Li; i < MemSize; i++, GetNextIndex(f))
			Temp[i] = pMem[f];
		delete[] pMem;
		pMem = Temp;
		Li = 0;
		Hi = MemSize - 1;
		MemSize += Size;
	}
	else
		throw SetRetCode(DataFull);
}
```

##Проверка работоспособности класса TQueue
Методы класса Queue были протестированы с помощью фреймворка Google Test.

###Файл test_queue.cpp

```C++
#include "..\..\mp2-lab3-stack\mp2-lab3-stack\gtest.h"
#include "Queue.h"

TEST(Queue, can_create_Queue_with_positive_length)      //Проверка возможности создать очередь
{
	ASSERT_NO_THROW(Queue<int> a(2));
}
TEST(Queue, cant_create_Queue_with_negative_length)		//Проверка невозможности создать очередь, если параметр огр. памяти отрицателен 
{
	ASSERT_ANY_THROW(Queue<int> a(-5));
}
TEST(Queue, function_IsFull_return_right_value)			//проверка метода IsFull
{
	Queue<int> a(1);
	EXPECT_EQ(0, a.IsFull());
	a.Put(1);
	EXPECT_EQ(1, a.IsFull());
}
TEST(Queue, function_IsEmpty_return_right_value)		//проверка метода IsEmpty
{
	Queue<int> a(1);
	EXPECT_EQ(1, a.IsEmpty());
	a.Put(1);
	EXPECT_EQ(0, a.IsEmpty());
}
TEST(Queue, can_put_any_value_into_queue)				//Проверка метода Put
{
	Queue<int> a(2);
	ASSERT_NO_THROW(a.Put(5));
	ASSERT_NO_THROW(a.Put(2));
	EXPECT_EQ(0, a.IsEmpty());
}
TEST(Queue, can_get_any_value_from_queue)				//Проверка метода Get
{
	Queue<int> a(2);
	a.Put(5);
	a.Put(2);
	EXPECT_EQ(5, a.Get());
	EXPECT_EQ(2, a.Get());
	EXPECT_EQ(1, a.IsEmpty());
}
TEST(Queue, cant_put_in_overflow_memory)				//Проверка невозможности положить значение в переполненную очередь
{
	Queue<int> a(1);
	ASSERT_NO_THROW(a.Put(100));
	ASSERT_ANY_THROW(a.Put(100));
}
TEST(Queue, can_reconstructed_memory)					//Проверка метода reconstruction
{
	Queue<int> a(100);
	for (int i = 0; i < 100; i++)
		ASSERT_NO_THROW(a.Put(100));
}
TEST(Queue, can_revers_structure_of_queue)				//Проверка случая, когда в массиве pMem конец очереди находится перед началом очереди 
{
	Queue<int> a(2);
	a.Put(4);
	a.Put(8);
	a.Get();
	ASSERT_NO_THROW(a.Put(4));
	EXPECT_EQ(8, a.Get());
	EXPECT_EQ(4, a.Get());
}
TEST(Queue, size_test_of_queue)							//Проверка работоспособности заполнения очереди большим количеством данных
{
	Queue<int> a;
	for (int i = 0; i < 1000000; i++)
		ASSERT_NO_THROW(a.Put(i));
}
```

###Результаты тестов

![tests](images/test_queue.jpg)

## Реализация модели имитации системы выполнения задач процессора

План реализации модели имитации системы выполнения задач процессора состоит в следующем:

 - Реализация класса TProc.
 - Реализация класса TJobStream.
 - Реализация прикладного приложения для работы с этими классами.

###Реализация класса TProc

Класс TProc имеет всего один конструктор и один метод. Метод RunProc получает в качестве аргумента шанс завершения задания на данном такте в процентах, а возвращает переменную типа bool, где true, если задание еще не закончено, или false, если задание закончено.

####Файл TProc.h

```C++
#include <string>
#include <windows.h>

class TProc
{
protected:
	int Hz;
public:
	TProc(int Hz)
	{
		(this)->Hz = Hz;
	}
	bool RunProc(int ChanceToDone) { return (rand() % 100 <= ChanceToDone) ? true : false; }
};
```

###Реализация класса TJobStream

Класс TJobStream содержит в себе экземпляр класса TProc и указатель на структуру Statistic. Структура Statistic содержит в себе настройки и статистику процессора, структура описана в тех же файлах, что и класс TJobStream. Главная задача  класса TJobStream - управление заданиями и сбор статистики.

####Файл TJobStream.h

```C++
#include "TProc.h"
#include "Queue.h"
#include <windows.h>
#include <string>
#include <iostream>

struct Statistic					//Статистика и настройки
{
		int NumberOfJob;                //количество заданий для процессора
		int	HzOfProc;					//Производительность процессора
		int	HowLong;					//Порог времени работы процессора
		int	MaxDurabilityOfJob;			//Максимальная длительность работы процессора
		int	MinDurabilityOfJob;			//Минимальная длительность работы процессора
		int	TimeWithNoJob;				//Общее время простоя процессора
		int	TimeWithJob;				//Общее время работы процессора без простоя
		int	ProcTime;					//Общее время работы процессора
		int	CountOfLostJobs;			//Количество потерянных работ для процессора
		int	QueueQuality;				//Размер очереди
		int	CountElementsInQueue;		//Количество работ в очереди в данный момент
		int	NumberOfJobInProc;			//Номер работы в процессоре в данный момент
		int	ChanceToCreateNewJob;		//Шанс выдачи работы процессору
		int ChanceToDoneJob;			//Шанс закончить работу для данного процесса
		int	MinChanceToDoneJob;         //Минимальный шанс на завершение для работ
		int	MaxChanceToDoneJob;			//Максимальный шанс на завершение для работ
		bool Valid, Quit;				//флаги управления                                                 
};

class TJobStream
{
protected:
	Statistic *Stats;		 // Статистика, настройки и управление
	Queue<int> Jobs;         // очередь работ
	void GenerateNewJob(int Chance);   // генерация работы
	void GenerateNewJob() { GenerateNewJob(Stats->ChanceToCreateNewJob); }
public:
	TJobStream(Statistic *st) : Jobs(st->QueueQuality, MEM_STATIC) { Stats = st; }  
	static void PrintStats(Statistic const *Stats);                               // вывод на экран структуры статистики
	void PrintStats() { TJobStream::PrintStats(Stats); }                          // вывод на экран структуры статистики внутри объекта
	static bool StatsValid(Statistic const *Stats);                               // проверка структуры статистики на валидность
	bool StatsValid() { return StatsValid(Stats); }								  // проверка структуры статистики на валидность внутри объекта
	static double StrToDouble(string str);										  // перевод из строки в double
	void RunJob();																  // запуск процессора и генерация работ
};

```

####Файл TJobStream.cpp

```C++
#include "TJobStream.h"

bool TJobStream::StatsValid(Statistic const *Stats)
{
	if ( !Stats->Valid || Stats->NumberOfJob < 0 || Stats->ChanceToCreateNewJob > 100 || Stats->ChanceToCreateNewJob <= 0
		|| Stats->HzOfProc <= 0 || Stats->MaxDurabilityOfJob < 0 || Stats->MinDurabilityOfJob < 0
		|| Stats->MaxDurabilityOfJob < Stats->MinDurabilityOfJob || Stats->QueueQuality <= 0 ||
		Stats->CountElementsInQueue < 0 || Stats->NumberOfJobInProc < 0 || Stats->HowLong <= 0 ||
		Stats->TimeWithNoJob < 0 || Stats->TimeWithJob < 0 || Stats->ProcTime < 0 || Stats->CountOfLostJobs < 0 ||
		Stats->QueueQuality < 0 || Stats->CountElementsInQueue < 0 || Stats->ChanceToDoneJob <= 0 || Stats->ChanceToDoneJob > 100  ||
		Stats->MinChanceToDoneJob <= 0 || Stats->MaxChanceToDoneJob <= 0 || Stats->MaxChanceToDoneJob < Stats->MinChanceToDoneJob ||
		Stats->MinChanceToDoneJob > 100 || Stats->MaxChanceToDoneJob > 100)
		return 0;

	return 1;
}

void TJobStream::PrintStats(Statistic const *Stats)
{
	cout << "Количество заданий для процессора: " << Stats->NumberOfJob << endl;
	cout << "Производительность процессора(в Hz): " << Stats->HzOfProc << endl;
	cout << "Порог времени работы процессора(в тактах): " << Stats->HowLong << endl;
	cout << "Максимальная длителькость задания процессора(в тактах): " << Stats->MaxDurabilityOfJob << endl;
	cout << "Минимальная длительность задания процессора(в тактах): " << Stats->MinDurabilityOfJob << endl;
	cout << "Общее время простоя процессора(в тактах): " << Stats->TimeWithNoJob << endl;
	cout << "Общее время работы процессора без простоя(в тактах): " << Stats->TimeWithJob << endl;
	cout << "Общее время работы процессора(в тактах): " << Stats->ProcTime << endl;
	cout << "Количество потерянных работ для процессора: " << Stats->CountOfLostJobs << endl;
	cout << "Размер очереди: " << Stats->QueueQuality << endl;
	cout << "Количество работ в очереди в данный момент: " << Stats->CountElementsInQueue << endl;
	cout << "Номер задания в процессоре в данный момент: " << Stats->NumberOfJobInProc << endl;
	cout << "Шанс выдачи задания процессору(в %): " << Stats->ChanceToCreateNewJob << endl;
	cout << "Шанс закончить задание для данного процесса(в %): " << Stats->ChanceToDoneJob << endl;
	cout << "Минимальный шанс на завершение для задания(в %): " << Stats->MinChanceToDoneJob << endl;
	cout << "Максимальный шанс на завершение для задания(в %): " << Stats->MaxChanceToDoneJob << endl;
	if (Stats->NumberOfJobInProc != 0)
		cout << "Средняя длительность задания: " << ((int)(Stats->TimeWithJob / Stats->NumberOfJobInProc)) << endl;
	if (Stats->NumberOfJob != 0)
	{
		double temp = (((double)Stats->CountOfLostJobs / (double)Stats->NumberOfJob) * (double)100);
		cout << "Процент отказов: " << (int)temp << endl;
	}
	else
		cout << "Процент отказов: 0" << endl;
	if (Stats->ProcTime != 0)
	{
		double temp = (((double)Stats->TimeWithNoJob / (double)Stats->ProcTime) * (double)100);
		cout << "Процент простоя: " << (int)temp << endl;
	}
	else
		cout << "Процент простоя: 0" << endl;
}

double TJobStream::StrToDouble(string str)
{
	double result = 0;
	int i;
	bool point = false;
	for (i = 0; i < str.length(); i++)
	{
		if ((str.at(i) == '.') && point) throw "wrong value";
		if (str.at(i) == '.') point = true;
	}
	if (point)
	{
		for (int p = 1, i = (str.find('.') + 1); i < str.length(); i++, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, -p);
			else
				throw "wrong value";
		}
		for (int p = 0, i = (str.find('.') - 1); i >= 0; i--, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, p);
			else
				throw "wrong value";
		}
	}
	else
		for (int p = 0, i = str.length() - 1; i >= 0; i--, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, p);
			else
				throw "wrong value";
		}
	return result;
}

void TJobStream::GenerateNewJob(int Chance)
{
	if ((rand() % 100) <= Chance)
	{
		if (Jobs.IsFull())
		{
			Stats->CountOfLostJobs++;
		}
		else
		{
			int temp;
			if (Stats->MaxChanceToDoneJob != Stats->MinChanceToDoneJob)
				temp = (rand() % (Stats->MaxChanceToDoneJob - Stats->MinChanceToDoneJob)) + Stats->MinChanceToDoneJob;
			else
				temp = Stats->MinChanceToDoneJob;
			if (temp == 0)
				temp++;
			Jobs.Put(temp);
			Stats->CountElementsInQueue++;
		}
		Stats->NumberOfJob++;
	}
}

void TJobStream::RunJob()
{
	cout << "загрузка..." << endl;
	int Durability;
	Stats->NumberOfJob = Stats->MaxDurabilityOfJob = Stats->MinDurabilityOfJob = Stats->TimeWithNoJob = Stats->TimeWithJob =
		Stats->ProcTime = Stats->CountElementsInQueue = Stats->CountOfLostJobs = Stats->NumberOfJobInProc = 0;
	TProc Proc(Stats->HzOfProc);

	if (!StatsValid())
		throw "Statistic or setting have a error(s)";

	while (Stats->ProcTime < Stats->HowLong)
	{
		if (!Jobs.IsEmpty())
		{
			Durability = 0;
			Stats->CountElementsInQueue--;
			Stats->NumberOfJobInProc++;
			Stats->ChanceToDoneJob = Jobs.Get();
			while (!Proc.RunProc(Stats->ChanceToDoneJob))
			{
				Durability++;
				Stats->TimeWithJob++;
				Stats->ProcTime++;
				GenerateNewJob();
			}
			Stats->TimeWithJob++;
			Durability++;
			if (Stats->MinDurabilityOfJob == 0)
				Stats->MinDurabilityOfJob = Durability;
			if (Stats->MinDurabilityOfJob > Durability)
				Stats->MinDurabilityOfJob = Durability;
			if (Stats->MaxDurabilityOfJob < Durability)
				Stats->MaxDurabilityOfJob = Durability;
		}
		else
			Stats->TimeWithNoJob++;
		Stats->ProcTime++;
		GenerateNewJob();
	}
	cout << "готово" << endl;
}
```

###Реализация прикладного приложения для работы с этими классами

Прикладное приложение выполняет функцию настройки процессора, запуска процессора и вывода статистики. Вывод статистики может выполнятся в виде потокового вывода на экран или в бинарный файл. Так же имеется возможность загрузки этой статистики из бинарного файла. 

####Файл QueueTestkit.cpp

```C++
#include <iostream>
#include <string>
#include <conio.h>
#include "TJobStream.h"
#include <fstream>

using namespace std;

void help()
{
	cout << "command list: " << endl;
	cout << "'save' \"path\" - save statistic and setting" << endl;
	cout << "'qsave' \"path\" - save statistic and setting, then quit" << endl;
	cout << "'load' \"path\" - load statistic and setting" << endl;
	cout << "'run' - run processor" << endl;
	cout << "'q' or 'quit' - exit of programm" << endl;
	cout << "'create' - write setting" << endl;
	cout << "'variouschange' or 'vc' - change min/maxChanceToDoneJob and ChanceToCreateNewJob" << endl;
	cout << "'showstats' or 'ss' - show statistic" << endl;
}

void SaveStatistic(string str, Statistic Stats)
{
	ofstream out(str, ios::binary | ios::out);
	out.write((char*)&Stats, sizeof(Stats));
	out.close();
}

void variouschange(Statistic &Stats)
{
	string console;
		try
		{
			cout << "Write chance to create new job in one tact(in %) : ";
			getline(cin, console);
			if (console == "q" || console == "quit")
				Stats.Quit = true;
			Stats.ChanceToCreateNewJob = (int)TJobStream::StrToDouble(console);
			console.clear();
			cout << "Write maximum chance to done new job(in %): ";
			getline(cin, console);
			if (console == "q" || console == "quit")
				Stats.Quit = true;
			Stats.MaxChanceToDoneJob = (int)TJobStream::StrToDouble(console);
			console.clear();
			cout << "Write minimum chance to done new job(in %): ";
			getline(cin, console);
			if (console == "q" || console == "quit")
				Stats.Quit = true;
			Stats.MinChanceToDoneJob = (int)TJobStream::StrToDouble(console);
			console.clear();
		}
		catch (char *err)
		{
			cout << err << endl;
		}
		if (!TJobStream::StatsValid(&Stats))
			Stats.Valid = false;
}

void SaveAndQuit(string str, Statistic Stats)
{
	ofstream out(str, ios::binary | ios::out);
	if (out.fail())
		cout << "Error of save function" << endl;
	else
		out.write((char*)&Stats, sizeof(Stats));
	out.close();
	Stats.Quit = true;
}

void LoadStatistic(string str, Statistic &Stats)
{
	ifstream in(str, ios::binary | ios::out);
	if (in.fail())
		cout << "wrong path: " << str << endl;
	else
		in.read((char*)&Stats, sizeof(Stats));
	in.close();
}

void CreateAndWriteSetting(Statistic &Stats)
{
	string console;

	Stats.NumberOfJob = Stats.MaxDurabilityOfJob = Stats.MinDurabilityOfJob = Stats.TimeWithNoJob = Stats.TimeWithJob =
		Stats.ProcTime = Stats.CountElementsInQueue = Stats.CountOfLostJobs = Stats.NumberOfJobInProc = 0;
	Stats.ChanceToDoneJob = 1;
	Stats.Valid = false;

	do
	{
		try
		{
			cout << "Write processor's speed(in Hz): ";
			getline(cin, console);
			if (console == "q" || console == "quit")
			{
				Stats.Quit = true;
				break;
			}
			Stats.HzOfProc = (int)TJobStream::StrToDouble(console);
			console.clear();
			cout << "How long processor work: ";
			getline(cin, console);
			if (console == "q" || console == "quit")
			{
				Stats.Quit = true;
				break;
			}
			Stats.HowLong = (int)TJobStream::StrToDouble(console);
			console.clear();
			cout << "Write maximum chance to done new job(in %): ";
			getline(cin, console);
			if (console == "q" || console == "quit")
			{
				Stats.Quit = true;
				break;
			}
			Stats.MaxChanceToDoneJob = (int)TJobStream::StrToDouble(console);
			console.clear();
			cout << "Write minimum chance to done new job(in %): ";
			getline(cin, console);
			if (console == "q" || console == "quit")
			{
				Stats.Quit = true;
				break;
			}
			Stats.MinChanceToDoneJob = (int)TJobStream::StrToDouble(console);
			console.clear();
			if (Stats.QueueQuality <= 0)
			{
				cout << "Write queue quality: ";
				getline(cin, console);
				if (console == "q" || console == "quit")
				{
					Stats.Quit = true;
					break;
				}
				Stats.QueueQuality = (int)TJobStream::StrToDouble(console);
				console.clear();
			}
			cout << "Write chance to create new job in one tact(in %) : ";
			getline(cin, console);
			if (console == "q" || console == "quit")
			{
				Stats.Quit = true;
				break;
			}
			Stats.ChanceToCreateNewJob = (int)TJobStream::StrToDouble(console);
			console.clear();
			Stats.Valid = true;
			if (!TJobStream::StatsValid(&Stats))
			{
				Stats.Valid = false;
				throw "wrong setting";
			}
			TJobStream::PrintStats(&Stats);
		}
		catch (char *err)
		{
			cout << err << endl;
		}

	} while (!TJobStream::StatsValid(&Stats));
}

int main()
{
	string console;
	Statistic Stats;

	setlocale(LC_CTYPE, "Russian");

	Stats.Valid = false;
	Stats.Quit = false;
	Stats.QueueQuality = 0;

	while (!Stats.Quit && !Stats.Valid)
	{
		cout << "Write 'load \"path\"' to load setting or 'create' to write setting by yourself" << endl;
		getline(cin, console);
		
		if (console.find_first_of("load") == 0 && console.find_first_of("\"") != string::npos && console.find_last_of("\"") != console.find_first_of("\""))
			LoadStatistic(console.substr(console.find_first_of("\"") + 1, console.find_last_of("\"") - console.find_first_of("\"") - 1), Stats);
		
		else if (console == "help" || console == "h")
			help();

		else if (console == "q" || console == "quit")
			Stats.Quit = true;

		else if (console == "create")
			CreateAndWriteSetting(Stats);

		else
			cout << "unknown command: " << console << endl << "for command list write 'h' or 'help'" << endl;
	}
	
	if (!Stats.Valid)
		return 0;

	TJobStream Stream(&Stats);

	while (!Stats.Quit)
	{
		getline(cin, console);

		if (console.find_first_of("save") == 0 && console.find_first_of("\"") != string::npos && console.find_last_of("\"") != console.find_first_of("\""))
			SaveStatistic(console.substr(console.find_first_of("\"") + 1, console.find_last_of("\"") - console.find_first_of("\"") - 1), Stats);

		else if (console.find_first_of("qsave") == 0 && console.find_first_of("\"") != string::npos && console.find_last_of("\"") != console.find_first_of("\""))
			SaveAndQuit(console.substr(console.find_first_of("\"") + 1, console.find_last_of("\"") - console.find_first_of("\"") - 1), Stats);

		else if (console == "q" || console == "quit")
			Stats.Quit = true;

		else if (console == "help" || console == "h")
			help();

		else if (console == "showstats" || console == "ss")
			Stream.PrintStats();

		else if (console.find_first_of("load") == 0 && console.find_first_of("\"") != string::npos && console.find_last_of("\"") != console.find_first_of("\""))
			LoadStatistic(console.substr(console.find_first_of("\"") + 1, console.find_last_of("\"") - console.find_first_of("\"") - 1), Stats);

		else if (console == "run")
		{
			if (Stream.StatsValid())
				Stream.RunJob();
			else
				cout << "Worng process setting, write 'create' to change setting" << endl;
		}

		else if (console == "create")
			CreateAndWriteSetting(Stats);

		else if (console == "variouschange" || console == "vc")
			variouschange(Stats);

		else if (console == "load")
			cout << "load \"path\"" << endl;

		else if (console == "save")
			cout << "No path specified. Exapmle: " << endl << "save \"path\"" << endl;

		else if (console == "qsave")
			cout << "No path specified. Exapmle: " << endl << "qsave \"path\"" << endl;

		else
			cout << "unknown command: " << console << endl << "for command list write 'h' or 'help'" << endl;
	}

	return 0;
}

```

####Пример использования прикладного приложения

![app1](images/Main1.jpg)
![app2](images/Main2.jpg)

##Анализ реализованной модели имитации системы выполнения задач процессора на основе полученных результатов

На основе полученных результатов при выполнении прикладной программы, были составлены следующие таблицы:

![table](images/research.jpg)

Исходя из первой и второй таблицы можно сделать вывод, что процессор работает максимально эффективно, если вероятность создания задания равна вероятности завершения задания. Когда вероятность создания задания превышает вероятность завершения задания происходит переполнение очереди, и, как следствие, потеря большого количество заданий. В случае, если вероятность завершения задания превышает вероятность создания задания, из-за отсутствия заданий в очереди, процессор начинает длительное время простаивать.

Третья таблица показывает нам зависимость вероятности завершения задания и их средней длительности. Из этого можно сделать вывод, что исходя из вероятности завершения задания можно приблизительно вычислить, какая возможная длительность будет у задания, а так же наглядно показывает степень увеличения длительности в зависимости от степени уменьшения вероятности.  

##Выводы

 В ходе выполнения данной работы были получены навыки работы со структурой данных очередь (Queue).
 Путем наследования от разработанного ранее класса TDataRoot был реализован класс Queue, чья работоспособность была протестирована с помощью Google Test Framework. В ходе теста было установлено, что программа верно решает поставленную задачу.
 Реализованный класс Queue был использован при написании модели имитации выполнения задач вычислительной системой (процессором).