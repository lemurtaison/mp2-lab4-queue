#include "TProc.h"
#include "Queue.h"
#include <windows.h>
#include <string>
#include <iostream>

struct Statistic					//���������� � ���������
{
		int NumberOfJob;                //���������� ������� ��� ����������
		int	HzOfProc;					//������������������ ����������
		int	HowLong;					//����� ������� ������ ����������
		int	MaxDurabilityOfJob;			//������������ ������������ ������ ����������
		int	MinDurabilityOfJob;			//����������� ������������ ������ ����������
		int	TimeWithNoJob;				//����� ����� ������� ����������
		int	TimeWithJob;				//����� ����� ������ ���������� ��� �������
		int	ProcTime;					//����� ����� ������ ����������
		int	CountOfLostJobs;			//���������� ���������� ����� ��� ����������
		int	QueueQuality;				//������ �������
		int	CountElementsInQueue;		//���������� ����� � ������� � ������ ������
		int	NumberOfJobInProc;			//����� ������ � ���������� � ������ ������
		int	ChanceToCreateNewJob;		//���� ������ ������ ����������
		int ChanceToDoneJob;			//���� ��������� ������ ��� ������� ��������
		int	MinChanceToDoneJob;         //����������� ���� �� ���������� ��� �����
		int	MaxChanceToDoneJob;			//������������ ���� �� ���������� ��� �����
		bool Valid, Quit;				//����� ����������                                                 
};

class TJobStream
{
protected:
	Statistic *Stats;		 // ����������, ��������� � ����������
	Queue<int> Jobs;         // ������� �����
	void GenerateNewJob(int Chance);   // ��������� ������
	void GenerateNewJob() { GenerateNewJob(Stats->ChanceToCreateNewJob); }
public:
	TJobStream(Statistic *st) : Jobs(st->QueueQuality, MEM_STATIC) { Stats = st; }  
	static void PrintStats(Statistic const *Stats);                               // ����� �� ����� ��������� ����������
	void PrintStats() { TJobStream::PrintStats(Stats); }                          // ����� �� ����� ��������� ���������� ������ �������
	static bool StatsValid(Statistic const *Stats);                               // �������� ��������� ���������� �� ����������
	bool StatsValid() { return StatsValid(Stats); }								  // �������� ��������� ���������� �� ���������� ������ �������
	static double StrToDouble(string str);										  // ������� �� ������ � double
	void RunJob();																  // ������ ���������� � ��������� �����
};
