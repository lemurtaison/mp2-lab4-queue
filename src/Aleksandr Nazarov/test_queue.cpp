#include "..\..\mp2-lab3-stack\mp2-lab3-stack\gtest.h"
#include "Queue.h"

TEST(Queue, can_create_Queue_with_positive_length)      //�������� ����������� ������� �������
{
	ASSERT_NO_THROW(Queue<int> a(2));
}
TEST(Queue, cant_create_Queue_with_negative_length)		//�������� ������������� ������� �������, ���� �������� ���. ������ ����������� 
{
	ASSERT_ANY_THROW(Queue<int> a(-5));
}
TEST(Queue, function_IsFull_return_right_value)			//�������� ������ IsFull
{
	Queue<int> a(1);
	EXPECT_EQ(0, a.IsFull());
	a.Put(1);
	EXPECT_EQ(1, a.IsFull());
}
TEST(Queue, function_IsEmpty_return_right_value)		//�������� ������ IsEmpty
{
	Queue<int> a(1);
	EXPECT_EQ(1, a.IsEmpty());
	a.Put(1);
	EXPECT_EQ(0, a.IsEmpty());
}
TEST(Queue, can_put_any_value_into_queue)				//�������� ������ Put
{
	Queue<int> a(2);
	ASSERT_NO_THROW(a.Put(5));
	ASSERT_NO_THROW(a.Put(2));
	EXPECT_EQ(0, a.IsEmpty());
}
TEST(Queue, can_get_any_value_from_queue)				//�������� ������ Get
{
	Queue<int> a(2);
	a.Put(5);
	a.Put(2);
	EXPECT_EQ(5, a.Get());
	EXPECT_EQ(2, a.Get());
	EXPECT_EQ(1, a.IsEmpty());
}
TEST(Queue, cant_put_in_overflow_memory)				//�������� ������������� �������� �������� � ������������� �������
{
	Queue<int> a(1);
	ASSERT_NO_THROW(a.Put(100));
	ASSERT_ANY_THROW(a.Put(100));
}
TEST(Queue, can_reconstructed_memory)					//�������� ������ reconstruction
{
	Queue<int> a(100);
	for (int i = 0; i < 100; i++)
		ASSERT_NO_THROW(a.Put(100));
}
TEST(Queue, can_revers_structure_of_queue)				//�������� ������, ����� � ������� pMem ����� ������� ��������� ����� ������� ������� 
{
	Queue<int> a(2);
	a.Put(4);
	a.Put(8);
	a.Get();
	ASSERT_NO_THROW(a.Put(4));
	EXPECT_EQ(8, a.Get());
	EXPECT_EQ(4, a.Get());
}
TEST(Queue, size_test_of_queue)							//�������� ����������������� ���������� ������� ������� ����������� ������
{
	Queue<int> a;
	for (int i = 0; i < 1000000; i++)
		ASSERT_NO_THROW(a.Put(i));
}