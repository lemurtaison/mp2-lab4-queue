#include <iostream>
#include <string>
#include <conio.h>
#include "TJobStream.h"
#include <fstream>

using namespace std;

void help()
{
	cout << "command list: " << endl;
	cout << "'save' \"path\" - save statistic and setting" << endl;
	cout << "'qsave' \"path\" - save statistic and setting, then quit" << endl;
	cout << "'load' \"path\" - load statistic and setting" << endl;
	cout << "'run' - run processor" << endl;
	cout << "'q' or 'quit' - exit of programm" << endl;
	cout << "'create' - write setting" << endl;
	cout << "'variouschange' or 'vc' - change min/maxChanceToDoneJob and ChanceToCreateNewJob" << endl;
	cout << "'showstats' or 'ss' - show statistic" << endl;
}

void SaveStatistic(string str, Statistic Stats)
{
	ofstream out(str, ios::binary | ios::out);
	out.write((char*)&Stats, sizeof(Stats));
	out.close();
}

void variouschange(Statistic &Stats)
{
	string console;
		try
		{
			cout << "Write chance to create new job in one tact(in %) : ";
			getline(cin, console);
			if (console == "q" || console == "quit")
				Stats.Quit = true;
			Stats.ChanceToCreateNewJob = (int)TJobStream::StrToDouble(console);
			console.clear();
			cout << "Write maximum chance to done new job(in %): ";
			getline(cin, console);
			if (console == "q" || console == "quit")
				Stats.Quit = true;
			Stats.MaxChanceToDoneJob = (int)TJobStream::StrToDouble(console);
			console.clear();
			cout << "Write minimum chance to done new job(in %): ";
			getline(cin, console);
			if (console == "q" || console == "quit")
				Stats.Quit = true;
			Stats.MinChanceToDoneJob = (int)TJobStream::StrToDouble(console);
			console.clear();
		}
		catch (char *err)
		{
			cout << err << endl;
		}
		if (!TJobStream::StatsValid(&Stats))
			Stats.Valid = false;
}

void SaveAndQuit(string str, Statistic Stats)
{
	ofstream out(str, ios::binary | ios::out);
	if (out.fail())
		cout << "Error of save function" << endl;
	else
		out.write((char*)&Stats, sizeof(Stats));
	out.close();
	Stats.Quit = true;
}

void LoadStatistic(string str, Statistic &Stats)
{
	ifstream in(str, ios::binary | ios::out);
	if (in.fail())
		cout << "wrong path: " << str << endl;
	else
		in.read((char*)&Stats, sizeof(Stats));
	in.close();
}

void CreateAndWriteSetting(Statistic &Stats)
{
	string console;

	Stats.NumberOfJob = Stats.MaxDurabilityOfJob = Stats.MinDurabilityOfJob = Stats.TimeWithNoJob = Stats.TimeWithJob =
		Stats.ProcTime = Stats.CountElementsInQueue = Stats.CountOfLostJobs = Stats.NumberOfJobInProc = 0;
	Stats.ChanceToDoneJob = 1;
	Stats.Valid = false;

	do
	{
		try
		{
			cout << "Write processor's speed(in Hz): ";
			getline(cin, console);
			if (console == "q" || console == "quit")
			{
				Stats.Quit = true;
				break;
			}
			Stats.HzOfProc = (int)TJobStream::StrToDouble(console);
			console.clear();
			cout << "How long processor work: ";
			getline(cin, console);
			if (console == "q" || console == "quit")
			{
				Stats.Quit = true;
				break;
			}
			Stats.HowLong = (int)TJobStream::StrToDouble(console);
			console.clear();
			cout << "Write maximum chance to done new job(in %): ";
			getline(cin, console);
			if (console == "q" || console == "quit")
			{
				Stats.Quit = true;
				break;
			}
			Stats.MaxChanceToDoneJob = (int)TJobStream::StrToDouble(console);
			console.clear();
			cout << "Write minimum chance to done new job(in %): ";
			getline(cin, console);
			if (console == "q" || console == "quit")
			{
				Stats.Quit = true;
				break;
			}
			Stats.MinChanceToDoneJob = (int)TJobStream::StrToDouble(console);
			console.clear();
			if (Stats.QueueQuality <= 0)
			{
				cout << "Write queue quality: ";
				getline(cin, console);
				if (console == "q" || console == "quit")
				{
					Stats.Quit = true;
					break;
				}
				Stats.QueueQuality = (int)TJobStream::StrToDouble(console);
				console.clear();
			}
			cout << "Write chance to create new job in one tact(in %) : ";
			getline(cin, console);
			if (console == "q" || console == "quit")
			{
				Stats.Quit = true;
				break;
			}
			Stats.ChanceToCreateNewJob = (int)TJobStream::StrToDouble(console);
			console.clear();
			Stats.Valid = true;
			if (!TJobStream::StatsValid(&Stats))
			{
				Stats.Valid = false;
				throw "wrong setting";
			}
			TJobStream::PrintStats(&Stats);
		}
		catch (char *err)
		{
			cout << err << endl;
		}

	} while (!TJobStream::StatsValid(&Stats));
}

int main()
{
	string console;
	Statistic Stats;

	setlocale(LC_CTYPE, "Russian");

	Stats.Valid = false;
	Stats.Quit = false;
	Stats.QueueQuality = 0;

	while (!Stats.Quit && !Stats.Valid)
	{
		cout << "Write 'load \"path\"' to load setting or 'create' to write setting by yourself" << endl;
		getline(cin, console);
		
		if (console.find_first_of("load") == 0 && console.find_first_of("\"") != string::npos && console.find_last_of("\"") != console.find_first_of("\""))
			LoadStatistic(console.substr(console.find_first_of("\"") + 1, console.find_last_of("\"") - console.find_first_of("\"") - 1), Stats);
		
		else if (console == "help" || console == "h")
			help();

		else if (console == "q" || console == "quit")
			Stats.Quit = true;

		else if (console == "create")
			CreateAndWriteSetting(Stats);

		else
			cout << "unknown command: " << console << endl << "for command list write 'h' or 'help'" << endl;
	}
	
	if (!Stats.Valid)
		return 0;

	TJobStream Stream(&Stats);

	while (!Stats.Quit)
	{
		getline(cin, console);

		if (console.find_first_of("save") == 0 && console.find_first_of("\"") != string::npos && console.find_last_of("\"") != console.find_first_of("\""))
			SaveStatistic(console.substr(console.find_first_of("\"") + 1, console.find_last_of("\"") - console.find_first_of("\"") - 1), Stats);

		else if (console.find_first_of("qsave") == 0 && console.find_first_of("\"") != string::npos && console.find_last_of("\"") != console.find_first_of("\""))
			SaveAndQuit(console.substr(console.find_first_of("\"") + 1, console.find_last_of("\"") - console.find_first_of("\"") - 1), Stats);

		else if (console == "q" || console == "quit")
			Stats.Quit = true;

		else if (console == "help" || console == "h")
			help();

		else if (console == "showstats" || console == "ss")
			Stream.PrintStats();

		else if (console.find_first_of("load") == 0 && console.find_first_of("\"") != string::npos && console.find_last_of("\"") != console.find_first_of("\""))
			LoadStatistic(console.substr(console.find_first_of("\"") + 1, console.find_last_of("\"") - console.find_first_of("\"") - 1), Stats);

		else if (console == "run")
		{
			if (Stream.StatsValid())
				Stream.RunJob();
			else
				cout << "Worng process setting, write 'create' to change setting" << endl;
		}

		else if (console == "create")
			CreateAndWriteSetting(Stats);

		else if (console == "variouschange" || console == "vc")
			variouschange(Stats);

		else if (console == "load")
			cout << "load \"path\"" << endl;

		else if (console == "save")
			cout << "No path specified. Exapmle: " << endl << "save \"path\"" << endl;

		else if (console == "qsave")
			cout << "No path specified. Exapmle: " << endl << "qsave \"path\"" << endl;

		else
			cout << "unknown command: " << console << endl << "for command list write 'h' or 'help'" << endl;
	}

	return 0;
}
