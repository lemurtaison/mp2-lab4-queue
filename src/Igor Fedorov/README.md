# Лабораторная работа №4: Очереди

## Цели и задачи

Лабораторная работа направлена на практическое освоение динамической структуры данных Очередь. С этой целью в лабораторной работе изучаются различные варианты структуры хранения очереди и разрабатываются методы и программы решения задач с использованием очередей. В качестве области приложений выбрана тема эффективной организации выполнения потока заданий на вычислительных системах.
Очередь характеризуется таким порядком обработки значений, при котором вставка новых элементов производится в конец очереди, а извлечение – из начала. Подобная организация данных широко встречается в различных приложениях. В качестве примера использования очереди предлагается задача разработки системы имитации однопроцессорной ЭВМ. Рассматриваемая в рамках лабораторной работы схема имитации является одной из наиболее простых моделей обслуживания заданий в вычислительной системе и обеспечивает тем самым лишь начальное ознакомление с проблемами моделирования и анализа эффективности функционирования реальных вычислительных систем.
Очередь (англ. queue), – схема запоминания информации, при которой каждый вновь поступающий ее элемент занимает крайнее положение (конец очереди). При выдаче информации из очереди выдается элемент, расположенный в очереди первым (начало очереди), а оставшиеся элементы продвигаются к началу; следовательно, элемент, поступивший первым, выдается первым.

### Поставленные задачи
Для вычислительной системы (ВС) с одним процессором и однопрограммным последовательным режимом выполнения поступающих заданий требуется разработать программную систему для имитации процесса обслуживания заданий в ВС.
По результатам проводимых вычислительных экспериментов система имитации должна выводить информацию об условиях проведения эксперимента и полученные в результате имитации показатели функционирования вычислительной системы.

### План работы
1. Реализация структуры хранения данных TQueue на основе имеующейся структуры TStack (TStack.h).
2. Проверка работоспособности класса TQueue с помощью GTest.
3. Реализация модели имитации системы выполнения задач процессора и очереди задач.
4. Интерфейс для передачи статистики в класс с графиком
5. Реализация GUI.


### Используемые инструменты
- Система контроля версий **Git**.
- Фреймворк для написания автоматических тестов **Google Test**.
- Среда разработки **QT Creator**.


## Выполнение работы

### 1. Реализация структуры хранения данных TQueue на основе имеующейся структуры TStack.
Для реализации очереди используется одномерный массив. В связи с характером обработки значений, располагаемых в очереди, для указания хранимых в очереди данных необходимо иметь два указателя – на начало и конец очереди. Эти указатели увеличивают свое значение: один при вставке, другой при извлечении элемента.

**Используемые параметры:**

* *pMem * - Указатель на память, выделенную для кольцевого буфера
* *MemSize* - Размер выделенной памяти
* *MaxMemSize* - Размер памяти, выделяемый по умолчанию, если при создании кольцевого буфера явно не указано требуемое количество элементов памяти
* *DataCount* - Число значений в очереди
* *Hi* - Индекс элемента массива, в котором хранится последний элемент очереди
* *Li* - Индекс элемента массива, в котором хранится первый элемент очереди

1) TStack.h:

```
#ifndef __TSTACK_H__
#define __TSTACK_H__

#include <iostream>
#include "tdataroot.h"

template <class ValType>
class TStack :public TDataRoot<ValType>
{
protected:
	int Hi;// вершина стека
public:
	// конструкторы
    TStack(int Size = DefMemSize):TDataRoot(Size){
        Hi =-1;
    }  // конструктор по умолчанию и с параметром
	TStack(const TStack& St)// конструктор копирования
	{
		if (St.pMem != nullptr)
		{
			MemSize = St.MemSize;
			DataCount = St.DataCount;
			Hi = St.Hi;
			pMem = new ValType[MemSize];
			for (int i = 0; i < DataCount; ++i)
				pMem[i] = St.pMem[i];

			SetRetCode(DataOK);
		}
		else
			SetRetCode(DataNoMem);
		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}

	//методы
    virtual void  Put(const ValType &Val)// добавить значение
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsFull())
		{
            void *p = nullptr;
            SetMem(p, MemSize + DefMemSize);
            pMem[++Hi] = Val;
            DataCount++;
			SetRetCode(DataFull); //DataOk
		}
		else// обычна вставка элемента
		{
			pMem[++Hi] = Val;
			DataCount++;
			SetRetCode(DataOK);
		}

	}
    virtual ValType Get()//извлечь ValType
	{

        if (SetRetCode(GetRetCode()) != DataOK)
            throw GetRetCode();
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
        if (IsEmpty())
			SetRetCode(DataEmpty);
		else 
		{
            ValType res = pMem[Hi--];
            if ((Hi>DefMemSize - 1) && (Hi%DefMemSize == 0))
			{
				void *p = nullptr;
				SetMem(p, MemSize - DefMemSize);
			}
			SetRetCode(DataOK);
            DataCount--;
			return res;
        }
	}
    virtual ValType GetHighElem()// возвращает копию вершины стека
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsEmpty())
			SetRetCode(DataEmpty);
		else
		{
			SetRetCode(DataOK);
			return pMem[Hi];
		}

		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}

	virtual void Print()// печать значений
	{
		if (pMem != nullptr)
		{
            for (int i = 0; i < DataCount; ++i)
                std::cout << i <<" : "<< pMem[i] << ' ' << std::endl;
			SetRetCode(DataOK);
		}
		else
			throw SetRetCode(DataNoMem);

		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}
protected:
	int  IsValid()// тестирование структуры
	{
		int res = 0;
		if (pMem == nullptr)
			res++;
		if (MemSize < DataCount)
			res += 2;
		return res;
	}
};

#endif

```

В связи с тем, что структура хранения очереди во многом аналогична структуре хранения стек, класс для реализации очереди построен наследованием от класса стек. При наследовании достаточно переопределить методы Get и Put. В методе Get изменяется индекс для получения элемента (извлечение значений происходит из начала очереди), метод Put переопределен так, чтобы использовался кольцевой буфер.

3) TQueue.h:

```
#ifndef TQUEUE_H
#define TQUEUE_H
#include "TStack.h"

template<class ValType>
class Tqueue : public TStack<ValType>
{
private:
    int Li; //ук на 1 эл
public:

    Tqueue(int Size = DefMemSize):TStack(Size),Li(0){    }
    Tqueue<ValType>(const Tqueue& qu);
    void Put(const ValType &val);
    ValType Get();
    ValType GetHighElem();
    void Print(){
        TStack::Print();
    }
};

template <class ValType> // конструктор копирования
Tqueue<ValType>::Tqueue(const Tqueue<ValType> &qu):
  TStack<ValType>(qu){
    this->Li = qu.Li;
}

template <class ValType>// put
void Tqueue<ValType>::Put(const ValType &val)
{
    if(!IsFull()){
        if(Hi==MemSize-1){
            Hi = -1;
            TStack::Put(val);
        }
        else{
            TStack::Put(val);
        }
    }else
        SetRetCode(DataFull);
}

template <class ValType>// get
ValType Tqueue<ValType>::Get()
{
    if (SetRetCode(GetRetCode()) != DataOK)
            throw GetRetCode();
    if (pMem == nullptr)
        SetRetCode(DataNoMem);
    else if (IsEmpty())
        SetRetCode(DataEmpty);
    else{
    if(Li==MemSize-1)
        Li = -1;
    ValType res = pMem[Li++];
    DataCount--;
    SetRetCode(DataOK);

    return res;
    }
}
template<class ValType>//get High element
ValType Tqueue<ValType>::GetHighElem()// возвращает копию первого по очереди
{
    if (pMem == nullptr)
           SetRetCode(DataNoMem);
    else if (IsEmpty())
            SetRetCode(DataEmpty);
    else
    {
            SetRetCode(DataOK);
            return pMem[Li];
    }
    if (SetRetCode(GetRetCode()) != DataOK)
            throw GetRetCode();
}


#endif // TQUEUE_H
```	

**Используемые методы:**

* *void Put(const TData &Val);* - Добавить элемент в очередь. Метод наследуется из класса TStack.
* *TData Get();* - Удалить элемент из очереди. При удалении элемента из очереди необходимо возвратить значение из массива по индексу начала очереди, переместить указатель начала очереди и уменьшить количество элементов.
* *bool IsEmpty();* - Проверка очереди на пустоту. Метод наследуется из класса TStack.
* *bool IsFull();* - Проверка очереди на полноту. Метод наследуется из класса TStack.


### 2. Проверка работоспособности класса TQueue

Методы класса **TQueue** были протестированы с помощью фреймворка **Google Test**.

//может создать очередь
```
TEST(Tqueue, can_create_queue)
{
  ASSERT_NO_THROW(Tqueue<int> q(5));
}
//может забрать елемент
TEST(Tqueue, can_get_elem)
{
  Tqueue<int> q(5);
  ASSERT_NO_THROW(q.Get());
}
// может положить элемент
TEST(Tqueue, can_put_elem)
{
   Tqueue<int> q(1);
  ASSERT_NO_THROW(q.Put(5));
}
//put работает
TEST(Tqueue, put_work_isOK)
{
    Tqueue<int> q(1);
    q.Put(5);
  ASSERT_EQ(q.GetHighElem(),5);
}
//get тоже работает
TEST(Tqueue,get_work_isOK )
{
    Tqueue<int> q;
    q.Put(5);
    int a = q.Get();
    ASSERT_EQ(a,5);
}

//может быть и пустой и полной
TEST(Tqueue,isFull_and_isEmpty )
{
    Tqueue<int> q;
    for(int i =0;i<DefMemSize;i++)
        ASSERT_NO_THROW(q.Put(i));
    ASSERT_TRUE(q.IsFull());
    for(int i =0;i<DefMemSize;i++)
        ASSERT_NO_THROW(q.Get());
    ASSERT_TRUE(q.IsEmpty());
}

//может быть не пустой
TEST(Tqueue, queue_isnt_empty)
{
    Tqueue <int> qu;
    qu.Put(1);
    EXPECT_FALSE(qu.IsEmpty());
}


//буфер работет вроде вот
TEST(Tqueue, ring_isCorrect)
{
    Tqueue<int> q;
    for(int i=0;i<24;i++)
        q.Put(i);
        int s;
    for(int i=0;i<10;i++)
        s += q.Get();
    for(int i=24;i<60;i++)
       ASSERT_NO_THROW(q.Put(i));
}

//первым пришел, первым вышел
TEST(TQueue, first_in_first_out)
{
    Tqueue<int> q;
    q.Put(1);
    q.Put(2);
    q.Put(3);
    EXPECT_EQ(1,q.Get());
    EXPECT_EQ(2,q.Get());
    EXPECT_EQ(3,q.Get());
}
```

**Результат**

![](tests.PNG)


	
### 3. Имитация модели вычислительной системы

**Описание алгоритма**

Для моделирования момента появления нового задания используется значение датчика случайных чисел. Если значение датчика меньше некоторого порогового значения q1, 0<=q1<=99, то считается, что на данном такте имитации в вычислительную систему поступает новое задание.
Моделирование момента завершения обработки очередного задания также выполняется по описанной выше схеме. При помощи датчика случайных чисел формируется еще одно случайное значение, и если это значение меньше порогового значения q2, 0<=q2<=99, то принимается, что на данном такте имитации процессор завершил обслуживание очередного задания и готов приступить к обработке задания из очереди ожидания.

Схема имитации процесса поступления и обслуживания заданий в вычислительной системе состоит в следующем:
* Каждое задание в системе представляется однозначным идентификатором - порядковым номером задания.
* Для проведения расчетов фиксируется число тактов работы системы.
* На каждом такте опрашивается состояние потока задач и процессор.
* Регистрация нового задания в вычислительной системе сводится к запоминанию идентификатора задания в очередь ожидания процессора. Ситуация переполнения очереди заданий понимается как нехватку ресурсов вычислительной системы для ввода нового задания (отказ от обслуживания).
* Для моделирования процесса обработки заданий учитывается, что процессор может быть занят обслуживанием очередного задания, либо же может находиться в состоянии ожидания (простоя).
* В случае освобождения процессора предпринимается попытка его загрузки. Для этого извлекается задание из очереди ожидания.
* Простой процессора возникает в случае, когда при завершении обработки очередного задания очередь ожидающих заданий оказывается пустой.
* После проведения всех тактов имитации производится вывод характеристик вычислительной системы:
- количество поступивших в вычислительную систему заданий в течение всего процесса имитации;
- количество отказов в обслуживании заданий из-за переполнения очереди;
- среднее количество тактов выполнения задания;
- количество тактов простоя процессора из-за отсутствия в очереди заданий для обслуживания.

1) TJobStream.h

```
class TJobStream{

private:
    int ID;//номер задачи
    int q1;//вероятность создания задачи
public:
    Tqueue<int> taskSet; //очередь задач
    TJobStream(int maxCreate); // конструктор очереди
    int addTask(); //вероятное добавление задачи
    int getIDtask() const; // получить ID задачи
};
```
	
2) TJobStream.cpp

```
#include <iostream>
#include "tjobstream.h"


TJobStream::TJobStream(int _q1):taskSet(12)// очередь длинной 12
{
    ID=0; 
    q1=_q1;
    srand(time(NULL));
}
int TJobStream::addTask(){
    int result = 0;
    if (rand() % 100 < q1)
        result = ++ID;
    return result;
}
int TJobStream::getIDtask() const
{
    return ID;
}
```
	
3) TProc.h

```
class TProc{
private:
    bool locked;
    int takts,q,tempID;
    int CompCount, CancelCount, WaitCount;
public:
    TJobStream Jstream;
    TProc(int q1,int q2,int takts);
    void work();
    void PrintResult() const;
	//интерфейс 
    int getTakts();
    int getCountRefus();
    int getCountJobs();
    int getCountDowntime();
    int NumTask();
    int NotCompTask();
    int AverComlTask();
    int PwaitTakt(); 
};

```

4) TProc.cpp

```

#include"tproc.h"
#include <iostream>

using namespace std;

TProc::TProc(int q1, int q2,  int t):Jstream(q1),q(q2),takts(t)
{
    srand(time(NULL));
    locked = false;
    CompCount = 0;
    CancelCount = 0;
    WaitCount = 0;
    tempID = 0;
}

void TProc::work(void)
{
    int Id = Jstream.addTask();
    if(Id>tempID){
        tempID = Id;
        if (Jstream.taskSet.IsFull())
            CancelCount++;
        else
            Jstream.taskSet.Put(Id);

    }
    if (locked){
        if ((rand()%100) < q)
            locked = false;
    }
    if(!locked)
    {
        if (Jstream.taskSet.IsEmpty()){
            WaitCount++;
            return;
        }
        else
        {
            int InProc = Jstream.taskSet.Get();
            locked = true;
            CompCount++;
            return;
        }
    }
}
int TProc::getCountDowntime() {
    return  WaitCount;
}
int TProc::getCountJobs() {
    return CompCount;
}
int TProc::getCountRefus() {
    return CancelCount;
}
int TProc::NumTask() {
    return Jstream.getIDtask();
}
int TProc::NotCompTask() {
    return Jstream.getIDtask() - CompCount;
}
int TProc::AverComlTask() {
    if(CompCount!=0){
//        cout << "aver takts/quest " << (float)(takts / CompCount) << endl;
        return (int)(takts / CompCount);
    }else{
//        cout << "aver takts/quest " << 0<< endl;
        return 0;
    }
}
int TProc::PwaitTakt() {
    if(takts!=0)
        return floor(WaitCount* 100 / takts);
     else
        return 0;
}
```

**Графики.**

Подумал и решил использовать QT Charts.

1)gradice.h

```
#ifndef GRAFICE_H
#define GRAFICE_H

#include <QWidget>
#include <iostream>
#include <QMainWindow>
#include <QtCharts/QtCharts>
QT_CHARTS_USE_NAMESPACE
namespace Ui {
class grafice;
}

class grafice : public QMainWindow
{
    Q_OBJECT

public:
    void connectData();
    void refreshData();
    explicit grafice(QWidget *parent = 0);
    void addData(int takt, int X);
    void loadPict(int& tkt);
    ~grafice();

private:
    QChartView *chartView;
    QValueAxis *axisX;
    QValueAxis *axisY;
    QChart *chart = new QChart();
    QLineSeries *series;

    Ui::grafice *ui;
};

#endif // GRAFICE_H
	
#include "grafice.h"
#include "ui_grafice.h"

#include <QMainWindow>
#include <QtCharts/QtCharts>
QT_CHARTS_USE_NAMESPACE
```

2)grafice.cpp

```
grafice::grafice(QWidget *parent) :QMainWindow(parent),
    ui(new Ui::grafice)
{
    ui->setupUi(this);

    series = new QLineSeries(); //набор точек
    chart = new QChart();//обьект графика
    axisX = new QValueAxis; //оси
    axisY = new QValueAxis;
    axisX->setTitleText("Takt");
    axisY->setTitleText("Value");
    axisX->setTickCount(10);

    chartView = new QChartView(chart); // рендер графика
    //add data series ti the chart
    series->setName("line");//line name
    chart->legend()->setAlignment(Qt::AlignBottom);
    chart->setTitle("TProc.h statistic");
    chart->addSeries(series);
    //add axis to the chart
    chart->addAxis(axisX, Qt::AlignBottom);
    chart->addAxis(axisY, Qt::AlignLeft);
    chart->setAxisX(axisX,series);
    chart->setAxisY(axisY,series);
    //create new view
    //place it in this widget
    setCentralWidget(chartView);

}
void grafice::loadPict(int& tkt){ //обновление графика
    axisY->setRange(0,series->at(tkt-1).y());
    axisX->setRange(0,tkt);
    chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    setCentralWidget(chartView);
}
void grafice::addData(int takt,int X){ // загрузка точек в ряд
    series->append(takt,X);
//    std::cout<<takt<<"  "<<ID<<std::endl;
}
void grafice::connectData(){ //обновить график
    chartView->update();
}
void grafice::refreshData(){ //очистить ряд точек
    series->clear();
}

grafice::~grafice() 
{
    delete ui;
}	
```
	
	
*Результаты* 
1)При увеличении размера очереди уменьшается число отказов в обслуживании задачи из-за переполнения очереди. 
2)При фиксированных значениях размера очереди и вероятности поступления нового задания с ростом вероятности выполнения задачи на текущем такте возрастает число выполненных процессором заданий, уменьшается процент отказов в обслуживании и среднее число тактов, требующихся на выполнение конкретного задания, следовательно, растет и количество тактов простоя.
3)При увеличении вероятности выпадания задачи, и фиксации вероятности решения задачи, отказы изза переполнения очереди увеличиваются

## Выводы

Я теперь могу сделать очередь. Показать графики. Создать форму для QT приложения.
Реализованный класс TQueue был использован при написании модели имитации выполнения задач вычислительной системой (процессором).