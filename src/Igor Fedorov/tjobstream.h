#ifndef TJOBSTREAM_H
#define TJOBSTREAM_H
#include <iostream>
#include "tqueue.h"
#include <stdlib.h>

class TJobStream{

private:
    int ID;
    int q1;
public:
    Tqueue<int> taskSet;
    TJobStream(int maxCreate);
    int addTask();
    int getIDtask() const;
};
#endif // TJOBSTREAM_H
