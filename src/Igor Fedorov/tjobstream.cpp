#include <iostream>
#include "tjobstream.h"
#include "time.h"


TJobStream::TJobStream(int _q1):taskSet(12)
{
    ID=0;
    q1=_q1;
    srand(time(NULL));
}
int TJobStream::addTask(){
    int result = 0;
    if (rand() % 100 < q1)
        result = ++ID;
    return result;
}
int TJobStream::getIDtask() const
{
    return ID;
}
