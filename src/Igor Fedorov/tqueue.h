#ifndef TQUEUE_H
#define TQUEUE_H
#include "TStack.h"

template<class ValType>
class Tqueue : public TStack<ValType>
{
private:
    int Li; //ук на 1 эл
public:

    Tqueue(int Size = DefMemSize):TStack(Size),Li(0){    }
    Tqueue<ValType>(const Tqueue& qu);
    void Put(const ValType &val);
    ValType Get();
    ValType GetHighElem();
    void Print(){
        std::cout<<"|============================="<<std::endl;
        TStack::Print();
        std::cout<<"|============================="<<std::endl;
    }
};

template <class ValType> // конструктор копированияbnnbnnnnn
Tqueue<ValType>::Tqueue(const Tqueue<ValType> &qu):
  TStack<ValType>(qu){
    this->Li = qu.Li;
}

template <class ValType>// put
void Tqueue<ValType>::Put(const ValType &val)
{
    if(!IsFull()){
        if(Hi==MemSize-1){
            Hi = -1;
            TStack::Put(val);
        }
        else{
            TStack::Put(val);
        }
    }else
        SetRetCode(DataFull);
}

template <class ValType>// get
ValType Tqueue<ValType>::Get()
{
//    std::cout<<DataCount<<"-----------------------=|"<<std::endl;
    if (SetRetCode(GetRetCode()) != DataOK)
            throw GetRetCode();
    if (pMem == nullptr)
        SetRetCode(DataNoMem);
    else if (IsEmpty())
        SetRetCode(DataEmpty);
    else{
    if(Li==MemSize-1)
        Li = -1;
    ValType res = pMem[Li++];
    DataCount--;
    SetRetCode(DataOK);

    return res;
    }
}
template<class ValType>//get High element
ValType Tqueue<ValType>::GetHighElem()// возвращает копию первого по очереди
{
    if (pMem == nullptr)
           SetRetCode(DataNoMem);
    else if (IsEmpty())
            SetRetCode(DataEmpty);
    else
    {
            SetRetCode(DataOK);
            return pMem[Li];
    }
    if (SetRetCode(GetRetCode()) != DataOK)
            throw GetRetCode();
}


#endif // TQUEUE_H
