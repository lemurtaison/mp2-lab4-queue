#ifndef TPROC_H
#define TPROC_H

#include <iostream>
#include <thread>
#include "tqueue.h"
#include "tjobstream.h"
#include <time.h>

class TProc{
private:
    bool locked;
    int takts,q,tempID;
    int CompCount, CancelCount, WaitCount;
public:
    TJobStream Jstream;
    TProc(int q1,int q2,int takts);
    void work();
    void PrintResult() const;
    int getTakts();
    int getCountRefus();
    int getCountJobs();
    int getCountDowntime();
    int NumTask();
    int NotCompTask();
    int AverComlTask();
    int PwaitTakt();
};











#endif // TPROC_H
