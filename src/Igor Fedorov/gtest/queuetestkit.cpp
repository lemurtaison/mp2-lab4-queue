#include "gtest.h"
#include "../laba4/src/tqueue.h"

TEST(Tqueue, can_create_queue)
{
  ASSERT_NO_THROW(Tqueue<int> q(5));
}
TEST(Tqueue, can_get_elem)
{
  Tqueue<int> q(5);
  ASSERT_NO_THROW(q.Get());
}
TEST(Tqueue, can_put_elem)
{
   Tqueue<int> q(1);
  ASSERT_NO_THROW(q.Put(5));
}
TEST(Tqueue, put_work_isOK)
{
    Tqueue<int> q(1);
    q.Put(5);
  ASSERT_EQ(q.GetHighElem(),5);
}
TEST(Tqueue,get_work_isOK )
{
    Tqueue<int> q;
    q.Put(5);
    int a = q.Get();
    ASSERT_EQ(a,5);
}
TEST(Tqueue,isFull_and_isEmpty )
{
    Tqueue<int> q;
    for(int i =0;i<DefMemSize;i++)
        ASSERT_NO_THROW(q.Put(i));
    ASSERT_TRUE(q.IsFull());
    for(int i =0;i<DefMemSize;i++)
        ASSERT_NO_THROW(q.Get());
    ASSERT_TRUE(q.IsEmpty());
}
TEST(Tqueue, queue_isnt_empty)
{
    Tqueue <int> qu;
    qu.Put(1);
    EXPECT_FALSE(qu.IsEmpty());
}

TEST(Tqueue, ring_isCorrect)
{
    Tqueue<int> q;
    for(int i=0;i<24;i++)
        q.Put(i);
        int s;
    for(int i=0;i<10;i++)
        s += q.Get();
    for(int i=24;i<60;i++)
       ASSERT_NO_THROW(q.Put(i));
}
TEST(TQueue, first_in_first_out)
{
    Tqueue<int> q;
    q.Put(1);
    q.Put(2);
    q.Put(3);
    EXPECT_EQ(1,q.Get());
    EXPECT_EQ(2,q.Get());
    EXPECT_EQ(3,q.Get());
}
