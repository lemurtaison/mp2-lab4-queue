#include "grafice.h"
#include "ui_grafice.h"

#include <QMainWindow>
#include <QtCharts/QtCharts>
QT_CHARTS_USE_NAMESPACE


grafice::grafice(QWidget *parent) :QMainWindow(parent),
    ui(new Ui::grafice)
{
    ui->setupUi(this);

    series = new QLineSeries();
    chart = new QChart();
    axisX = new QValueAxis;
    axisY = new QValueAxis;
    axisX->setTitleText("Takt");
    axisY->setTitleText("Value");
    axisX->setTickCount(10);

    chartView = new QChartView(chart);
    //add data series ti the chart
    series->setName("line");//line name
    chart->legend()->setAlignment(Qt::AlignBottom);
    chart->setTitle("TProc.h statistic");
    chart->addSeries(series);
    //add axis to the chart
    chart->addAxis(axisX, Qt::AlignBottom);
    chart->addAxis(axisY, Qt::AlignLeft);
    chart->setAxisX(axisX,series);
    chart->setAxisY(axisY,series);
    //create new view
    //place it in this widget
    setCentralWidget(chartView);

}
void grafice::loadPict(int& tkt){
    axisY->setRange(0,series->at(tkt-1).y());
    axisX->setRange(0,tkt);
    chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    setCentralWidget(chartView);
}
void grafice::addData(int takt,int X){
    series->append(takt,X);
}
void grafice::connectData(){
    chartView->update();
}
void grafice::refreshData(){
    series->clear();
}

grafice::~grafice()
{
    delete ui;
}
