#ifndef GRAFICE_H
#define GRAFICE_H

#include <QWidget>
#include <iostream>
#include <QMainWindow>
#include <QtCharts/QtCharts>
QT_CHARTS_USE_NAMESPACE
namespace Ui {
class grafice;
}

class grafice : public QMainWindow
{
    Q_OBJECT

public:
    void connectData();
    void refreshData();
    explicit grafice(QWidget *parent = 0);
    void addData(int takt, int X);
    void loadPict(int& tkt);
    ~grafice();

private:
    QChartView *chartView;
    QValueAxis *axisX;
    QValueAxis *axisY;
    QChart *chart = new QChart();
    QLineSeries *series;

    Ui::grafice *ui;
};

#endif // GRAFICE_H
