#include"tproc.h"
#include <iostream>

using namespace std;

TProc::TProc(int q1, int q2,  int t):Jstream(q1),q(q2),takts(t)
{
    srand(time(NULL));
    locked = false;
    CompCount = 0;
    CancelCount = 0;
    WaitCount = 0;
    tempID = 0;
}

void TProc::work(void)
{
    int Id = Jstream.addTask();
//    std::cout<<Jstream.getIDtask()<<"|"<<tempID<<"|"<<Id<<std::endl;
    if(Id>tempID){
        tempID = Id;
//        std::cout<<"|============"<<Jstream.getIDtask()<<"==============="<<std::endl;

        if (Jstream.taskSet.IsFull())
            CancelCount++;
        else
            Jstream.taskSet.Put(Id);

    }
    if (locked){
        if ((rand()%100) < q)
            locked = false;
    }
    if(!locked)
    {
        if (Jstream.taskSet.IsEmpty()){
            WaitCount++;
            return;
        }
        else
        {
            int InProc = Jstream.taskSet.Get();
            locked = true;
            CompCount++;
            return;
        }
    }
}
int TProc::getCountDowntime() {
    return  WaitCount;
}
int TProc::getCountJobs() {
    return CompCount;
}
int TProc::getCountRefus() {
    return CancelCount;
}
int TProc::NumTask() {
    return Jstream.getIDtask();
}
int TProc::NotCompTask() {
    return Jstream.getIDtask() - CompCount;
}
int TProc::AverComlTask() {
    if(CompCount!=0){
//        cout << "aver takts/quest " << (float)(takts / CompCount) << endl;
        return (int)(takts / CompCount);
    }else{
//        cout << "aver takts/quest " << 0<< endl;
        return 0;
    }
}
int TProc::PwaitTakt() {
    if(takts!=0)
        return floor(WaitCount* 100 / takts);
     else
        return 0;
}
