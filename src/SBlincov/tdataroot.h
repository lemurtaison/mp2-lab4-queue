//
//  tdataroot.h
//  Queue
//
//  Created by SBlincov on 22.03.17.
//  Copyright © 2017 Blincov Sergey. All rights reserved.
//
#ifndef tdataroot_h

#define tdataroot_h
#define DefMemSize 20

typedef int    TElem;
typedef TElem* PTElem;
typedef int    TData;

enum TMemType { MEM_HOLDER, MEM_RENTER };

class TDataRoot{
	
protected:
	PTElem pMem;
	int MemSize;
	int DataCount;
	TMemType MemType;
	
	void SetMem(void *p, int Size);

public:
	virtual ~TDataRoot();
	TDataRoot(int Size = DefMemSize);
	virtual bool IsEmpty(void) const;
	virtual bool IsFull (void) const;
	virtual void  Put   (const TData &Val) = 0;
	virtual TData Get   (void)             = 0;
	
	virtual int  IsValid() = 0;
	virtual void Print()   = 0;
	
	friend class TMultiStack;
	friend class TSuperMultiStack;
	friend class TComplexMultiStack;
};

#endif /* tdataroot_h */
