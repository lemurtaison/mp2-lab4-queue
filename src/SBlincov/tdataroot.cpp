#include "tdataroot.h"

/*
 Description of exceptions:
 1 = Data no mem
 2 = Data is full
 */

TDataRoot::TDataRoot(int Size){
	if(Size<0)
		throw 1; //Data no mem
	else{
		
		DataCount=0;
		MemSize = Size;
		if (Size==0){
			pMem=nullptr;
			MemType = MEM_RENTER;
		}
		else{
			pMem = new TElem [Size];
			MemType = MEM_HOLDER;
		}
		
	}
}

TDataRoot::~TDataRoot(){
	delete [] pMem;
}

void TDataRoot :: SetMem(void *p, int Size) {
	if (Size < 0)
		throw 1;
	else if(MemType == MEM_HOLDER){
		MemSize = Size;
		PTElem tmp = new TElem[MemSize];
		p = pMem;
		pMem = tmp;
		tmp = (PTElem)p;
		for (int i = 0; i < DataCount; ++i)
			pMem[i] = tmp[i];
		delete[] tmp;
	}else{
		MemSize = Size;
		for(int i =0;i<DataCount;++i){
			((PTElem)p)[i] = pMem[i];
		}
		pMem = (PTElem)p;
		
	}
}
bool TDataRoot::IsEmpty() const
{
	return DataCount==0;
}

bool TDataRoot::isFill() const
{
	return DataCount==MemSize;
}
