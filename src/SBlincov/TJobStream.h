//
//  TJobStream.h
//  Queue
//
//  Created by SBlincov on 03.04.17.
//  Copyright � 2017 Blincov Sergey. All rights reserved.
//
#ifndef TJobStream_h
#define TJobStream_h

#include "TProc.h"
#include "TQueue.h"

#include <iostream>

class TJobStream{
private:
	short q1;
	TProc Proc1;
	TProc Proc2;
	TQueue<int> Queue1;
	TQueue<int> Queue2;
	int tacts;
	int idCurrentJob;
	int missedJobs;
	int missedTacts;
public:
	TJobStream(short q1Temp, short q2Temp);
	void newTact();
	void executeJobs();
	void showStatictics();
	int generateQuantityOfTacts(){
		srand(time(0));
		return rand();
	}
}

#endif /* TJobStream_h */
