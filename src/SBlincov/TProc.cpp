//
//  TProc.cpp
//  Queue
//
//  Created by SBlincov on 03.04.17.
//  Copyright © 2017 Blincov Sergey. All rights reserved.
//

#include "TProc.h"

void TProc::tact(){
	srand(time(0));
	if (rand() % 100 < q2)
		busyStatus = false;
}

bool TProc::isBusy(){
	return busyStatus;
}

void TProc::newJob(){
	busyStatus = true;
}
