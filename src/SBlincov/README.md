# Методы программирования 2: Очереди

## 1. Введение

Лабораторная работа направлена на практическое освоение динамической структуры данных **Очередь**. С этой целью в лабораторной работе изучаются различные варианты структуры хранения очереди и разрабатываются методы и программы решения задач с использованием очередей. В качестве области приложений выбрана тема эффективной организации выполнения потока заданий на вычислительных системах.

Очередь характеризуется таким порядком обработки значений, при котором вставка новых элементов производится в конец очереди, а извлечение – из начала. Подобная организация данных широко встречается в различных приложениях. В качестве примера использования очереди предлагается задача разработки системы имитации однопроцессорной ЭВМ. Рассматриваемая в рамках лабораторной работы схема имитации является одной из наиболее простых моделей обслуживания заданий в вычислительной системе и обеспечивает тем самым лишь начальное ознакомление с проблемами моделирования и анализа эффективности функционирования реальных вычислительных систем.

**Очередь (англ. queue)**, – схема запоминания информации, при которой каждый вновь поступающий ее элемент занимает крайнее положение (*конец очереди*). При выдаче информации из очереди выдается элемент, расположенный в очереди первым (*начало очереди*), а оставшиеся элементы продвигаются к началу; следовательно, элемент, поступивший первым, выдается первым. 

## 2. Цели работы

Для вычислительной системы (ВС) с одним процессором и однопрограммным последовательным режимом выполнения поступающих заданий требуется разработать программную систему для имитации процесса обслуживания заданий в ВС. При построении модели функционирования ВС должны учитываться следующие основные моменты обслуживания заданий:

- генерация нового задания;
- постановка задания в очередь для ожидания момента освобождения процессора;
- выборка задания из очереди при освобождении процессора после обслуживания очередного задания.

По результатам проводимых вычислительных экспериментов система имитации должна выводить информацию об условиях проведения эксперимента (интенсивность потока заданий, размер очереди заданий, производительность процессора, число тактов имитации) и полученные в результате имитации показатели функционирования вычислительной системы, в т.ч.

- количество поступивших в ВС заданий;
- количество отказов в обслуживании заданий из-за переполнения очереди;
- среднее количество тактов выполнения заданий;
- количество тактов простоя процессора из-за отсутствия в очереди заданий для обслуживания.

Показатели функционирования вычислительной системы, получаемые при помощи систем имитации, могут использоваться для оценки эффективности применения ВС; по результатам анализа показателей могут быть приняты рекомендации о целесообразной модернизации характеристик ВС (например, при длительных простоях процессора и при отсутствии отказов от обслуживания заданий желательно повышение интенсивности потока обслуживаемых заданий и т.д.).

## 3. Описание структуры данных Очередь

Напомним, что динамическая структура есть математическая структура, которой соответствует частично-упорядоченное (по включению) базовое множество **М**, операции вставки и удаления элементы которого являются структурами данных. При этом отношения включения индуцируются операциями преобразования структуры данных.

Таким образом, очередь есть динамическая структура, операции вставки и удаления переводят очередь из одного состояния в другое, при этом добавление новых элементов осуществляется в конец очереди, а извлечение – из начала очереди (дисциплина обслуживания «первым пришел – первым обслужен.

Важной задачей при реализации системы обслуживания очереди является выбор структуры хранения, обеспечивающей решение проблемы эффективного использования памяти без перепаковок и без использования связных списков (требующих дополнительных затрат памяти на указатели).

Как и в случае со стеком, в качестве структуры хранения очереди предлагается использовать одномерный (одноиндексный) массив, размещаемый в динамической области памяти. В связи с характером обработки значений, располагаемых в очереди, для указания хранимых в очереди данных необходимо иметь два указателя – на начало и конец очереди. Эти указатели увеличивают свое значение: один при вставке, другой при извлечении элемента. Таким образом, в ходе функционирования очереди может возникнуть ситуация, когда оба указателя достигнут своего наибольшего значения и дальнейшее пополнение очереди станет невозможным, несмотря на наличие свободного пространства в очереди. Одним из решений проблемы «движения» очереди является организация на одномерном массиве кольцевого буфера. Кольцевым буфером называется структура хранения, получаемая из вектора расширением отношения следования парой **p(an,a1)**. 

Структура хранения очереди в виде кольцевого буфера может быть определена как одномерный (одноиндексный) массив, размещаемый в динамической области памяти и расположение данных в котором определяется при помощи следующего набора параметров:

- **pMem** – указатель на память, выделенную для кольцевого буфера,
- **MemSize** – размер выделенной памяти,
- **MaxMemSize** – размер памяти, выделяемый по умолчанию, если при создании кольцевого буфера явно не указано требуемое количество элементов памяти,
 
- **DataCount** – количество запомненных в очереди значений,
- **Hi** – индекс элемента массива, в котором хранится последний элемент очереди,
- **Li** – индекс элемента массива, в котором хранится первый элемент очереди.

В связи с тем, что структура хранения очереди во многом аналогична структуре хранения стек, предлагается класс для реализации очереди построить наследованием от класса стек, описанного в лабораторной работе №3. При наследовании достаточно переопределить методы **Get** и **GetNextIndex**. В методе **Get** изменяется индекс для получения элемента (извлечение значений происходит из начала очереди), метод **GetNextIndex** реализует отношение следования на кольцевом буфере.

## 4. Реализация структуры данных очередь

Класс **TQueue** является наследником класса **TStack** и переопределяет следующие его методы:

- **GetNextIndex** - реализует отношение следования на кольцевом буфере.
- **Get** - элемент извлекаем из начала очереди.
- **Print** - при использовании кольцевого буфера, перебор элементов значительно отличается от случая со стеком.

Класс TStack ранее был реализован с помощью шаблонов, соответственно и TQueue будет реализован также с помощью шаблонов.

***Код TStack.h***
```
//  TStack.h
//  Queue
//
//  Created by SBlincov on 22.03.17.
//  Copyright © 2017 Blincov Sergey. All rights reserved.
//
#ifndef TStack_h
#define TStack_h

#include <iostream>
#include "tdataroot.h"
using namespace std;

#define MAX_MEM_SIZE 20

template <class T>
class TStack : public TDataRoot<T>{
protected:
	int Hi; //Last element of queue
	int GetNextIndex(int index) override;
	
public:
	TStack(int Size = MAX_MEM_SIZE) : TDataRoot<T>(Size), Hi(-1) {};
	TStack(const TStack<T>&);
	
	void Put(const T&) override;
	T Get() override
	
	void Print() override;
};

template <class T>
TStack<T>::TStack(const TStack<T> &st) : TDataRoot<T>(st){
	Hi = st.Hi;
}

template <class T>
void TStack<T>::Put(const T &data){
	if (pMem == nullptr)
		throw 1;
	else if (IsFull())
		throw 2;
	else{
		Hi = GetNextIndex(Hi);
		pMem[Hi] = data;
		DataCount++;
	}
}

template <class T>
T TStack<T>::Get(){
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	else if (IsEmpty())
		throw SetRetCode(DataEmpty);
	else{
		DataCount--;
		return pMem[Hi--];
	}
}

template <class T>
void TStack<T>::Print(){
	for (int i = Hi; i > -1; i--)
		cout<<pMem[i]<<endl;
}

template <class T>
int TStack<T>::GetNextIndex(int index){
	return ++index;
}

#endif /* TStack_h */
```

***Код TQueue.h***
```
//
//  TQueue.h
//  Queue
//
//  Created by SBlincov on 03.04.17.
//  Copyright © 2017 Blincov Sergey. All rights reserved.
//
#ifndef TQueue_h
#define TQueue_h

#include "tstack.h"
using namespace std;

template <class T>
class TQueue : public TStack<T>
{
protected:
	int Li; //The first element of queue
	int GetNextIndex(int index) override;
	
public:
	TQueue(int Size = MAX_MEM_SIZE) : TStack<T>(Size), Li(0) {};
	T Get() override;
	void Print() override;
};

template <class T>

int TQueue<T>::GetNextIndex(int index){
	return ++index % MemSize; //MemSize defined in tdataroot
}

template <class T>
T TQueue<T>::Get(){
	T result;
	if (pMem == nullptr)
		throw 1;
	else if (IsEmpty())
		throw 2;
	else{
		result = pMem[Li];
		Li = GetNextIndex(Li);
		DataCount--;
		return result;
	}
}

template <class T>
void TQueue<T>::Print()
{
	for (int i = Li, j = 0; j < DataCount; j++, i = GetNextIndex(i))
		cout<<pMem[i]<<endl;
}

#endif /* TQueue_h */

```

### 5 Реализация класса TProc

Класс TJobStream был выбран в качестве управляющего процессом выполнения заданий

***Код TProc.h***

```
//
//  TProc.h
//  Queue
//
//  Created by SBlincov on 03.04.17.
//  Copyright © 2017 Blincov Sergey. All rights reserved.
//
#ifndef TProc_h
#define TProc_h

#include <cstdlib>
#include <ctime>

class TProc{
private:
	bool busyStatus;
	short q2;
public:
	TProc(short feasibility): q2(feasibility){};
	bool isBusy();
	void tact();
	void newJob();
};

#endif /* TProc_h */


***Код TProc.cpp***
	
	//
//  TProc.cpp
//  Queue
//
//  Created by SBlincov on 03.04.17.
//  Copyright © 2017 Blincov Sergey. All rights reserved.
//

#include "TProc.h"

void TProc::tact(){
	srand(time(0));
	if (rand() % 100 < q2)
		busyStatus = false;
}

bool TProc::isBusy(){
	return busyStatus;
}

void TProc::newJob(){
	busyStatus = true;
}

```

### Реализация класса TJobStream

```
//
//  TJobStream.h
//  Queue
//
//  Created by SBlincov on 03.04.17.
//  Copyright й 2017 Blincov Sergey. All rights reserved.
//
#ifndef TJobStream_h
#define TJobStream_h

#include "TProc.h"
#include "TQueue.h"

#include <iostream>

class TJobStream{
private:
	short q1;
	TProc Proc1;
	TProc Proc2;
	TQueue<int> Queue1;
	TQueue<int> Queue2;
	int tacts;
	int idCurrentJob;
	int missedJobs;
	int missedTacts;
public:
	TJobStream(short q1Temp, short q2Temp);
	void newTact();
	void executeJobs();
	void showStatictics();
	int generateQuantityOfTacts(){
		srand(time(0));
		return rand();
	}
}

#endif /* TJobStream_h */
```

***Код TJobStream.cpp***

```
//
//  TJobStream.cpp
//  Queue
//
//  Created by SBlincov on 03.04.17.
//  Copyright © 2017 Blincov Sergey. All rights reserved.
//

#include "TJobStream.h"
using namespace std;

TJobStream::TJobStream(short q1Temp, short q2Temp) : q1(q1Temp), q2(q2Temp){
	srand(time(0));
	int queueLen = MAX_MEM_SIZE;
	TProc Proc1(feasibility);
	TProc Proc2(feasibility);
	TQueue Queue1(queueLen);
	TQueue Queue2(queueLen);
	idCurrentJob = missedJobs = tacts = missedTacts = 0;
}

void TJobStream::newTact(){
	if(Proc1.isBusy()){
		tacts++;
		Proc1.tact();
	}
	if(Proc2.isBusy() ){
		tacts++;
		Proc2.tact();
	}
	if(!Proc1.isBusy() ){
		if(!Queue1.isFill() ){
			Queue1.get();
			Proc1.newJob();
		} 
		else
			if (!Queue2.isFill() ){
				Queue2.get();
				Proc1.newJob();
			}
			else{
				missedTacts++;
				Proc1.tact();
			}
	}
	if(!Proc2.isBusy() ){
		if(!Queue1.isFill() ){
			Queue1.get();
			Proc2.newJob();
		} 
		else
			if (!Queue2.isFill() ){
				Queue2.get();
				Proc2.newJob();
			}
			else{
				missedTacts++;
				Proc2.tact();
			}
	}
}

void TJobStream::executeJobs(){
	int quantityOfTacts = generateQuantityOfTacts();
	for (int i=0;i<quantityOfTacts;i++){
		if (rand()%100 < q1){
			idCurrentJob++;
			if (!Queue1.isFill())
				Queue1.Put(idCurrentJob);
			else
				if(!Queue2.isFill() )
					Queue2.Put(idCurrentJob);
				else
				missedJobs++;
		}
		newTact();
	}
	while (!Queue1.isFill() || !Queue2.isFill() )
		newTact();
}

void TJobStream::showStatictics(){
	cout << "Quantity tacts: " << tacts + missedTacts <<endl;
	cout << "--Of them was miss" << missedTacts;
	cout << "Quantity jobs: " << idCurrentJob <<endl;
	std::cout << "--Of them was miss: " << missedJobs <<endl;
	cout << "Average quantity tacts per job: " << (double)tacts / (double)(idCurrentJob - missedJobs) <<endl;
}
```

## 6. Вывод

В результате выполнения лабораторной работы была изучена и реализована такая структура данных, как "Очередь". Эта структура данных была применена на практике, с ее помощью была реализована имитация однопроцессорной ЭВМ. Ее поведение на больших дистанциях оказалось весьма предсказуемым, поскольку в расчетах участвовали случайные величины. 