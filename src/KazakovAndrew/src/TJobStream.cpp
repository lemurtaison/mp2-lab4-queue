#ifndef __TJOBSTREAM_CPP__
#define __TJOBSTREAM_CPP__

#include <cstdlib>
#include <iostream>
#include "TJobStream.h"

TJobStream::TJobStream(int q1, int q2, int q3, int queueLen) : q1(q1), CPU(q2), q3(q3), jobsQueue(queueLen)  {

	if (q1 < 0 || q1 > MAX_FLOW_INTENSITY) {
		throw 1;
	}

	if (q3 < 0 || q3 > MAX_JOB_DURATION) {
		throw 2;
	}

	currID = stats_refuses = stats_useful_cycles = stats_all_cycles = 0;

}

void TJobStream::cycle() {

	++stats_all_cycles;

	if (!CPU.isWorking() && !jobsQueue.isEmpty()) {
		TJob job = jobsQueue.get();
		stats_useful_cycles += job.jobDuration;

		CPU.startNewJob();
	}
	
	CPU.cycle();

}

void TJobStream::startJobStream(int cycles) {
	
	for (int i = 0; i < cycles; i++) {

		// �������, ��� ������� ��������� ����� �������
		if (rand() % MAX_FLOW_INTENSITY < q1) {
			if (jobsQueue.isFull()) {
				++stats_refuses;
			} else {
				// ��������� ����� �������
				jobsQueue.put(TJob(++currID, rand() % MAX_JOB_DURATION));
			}
		}

		cycle();

	} // for

}

void TJobStream::showStats() {

	std::cout << "Cycles amount: " << (stats_all_cycles) << std::endl;
	std::cout << "Useless cycles: " << stats_all_cycles - stats_useful_cycles << " (" <<
		(double)(stats_all_cycles - stats_useful_cycles) / (double)(stats_all_cycles) * 100 << "%)" << std::endl;
	std::cout << "Jobs: " << currID << std::endl;
	std::cout << "Missed jobs: " << stats_refuses << " (" <<
		(double)stats_refuses / (double)(currID) * 100 << "%)" << std::endl;
	std::cout << "Cycles per job: " << (double)stats_all_cycles / (double)(currID - stats_refuses) << std::endl;

}

#endif