#ifndef __TQUEUE_H__
#define __TQUEUE_H__

#include "TStack.h"

template<class T = int>
class TQueue: public TStack<T> {
protected:
	int lowIndex;
	virtual int getNextIndex(int index) override;

public:
	TQueue(int size = DEF_MEM_SIZE);
	virtual T get() override;
	virtual void print() override;
};

template<class T>
TQueue<T>::TQueue(int size) : TStack(size), lowIndex(0) {
};

template<class T>
int TQueue<T>::getNextIndex(int index) {
	return ++index % memSize;
}

template<class T>
T TQueue<T>::get() {

	if (pMem == nullptr) {
		throw SetRetCode(DataNoMem);
	}
	else if (isEmpty()) {
		throw SetRetCode(DataEmpty);
	}
	else {
		int oldIndex = lowIndex;
		lowIndex = getNextIndex(lowIndex);
		dataCount--;
		return pMem[oldIndex];
	}
	
}

template<class T>
void TQueue<T>::print() {

	for (int i = 0, j = lowIndex; i < dataCount; j = getNextIndex(j), ++i) {
		std::cout << pMem[j] << " ";
	}
	std::cout << std::endl;

}

#endif