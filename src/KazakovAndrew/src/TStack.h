#ifndef __TSTACK_H__
#define __TSTACK_H__

#include <iostream>
#include "TDataRoot.h"

template<class T>
class TStack: public TDataRoot<T> {
protected:
	int highIndex;
	virtual int getNextIndex(int index);

public:
	TStack(int size = DefMemSize);
	virtual void put(const T&) override;
	virtual T get() override;
	virtual void print() override;
};

template<class T>
TStack<T>::TStack(int size) : TDataRoot(size), highIndex(-1) {

	if (size < 0) {
		throw 4;
	}

};

template<class T>
int TStack<T>::getNextIndex(int index) {
	return ++highIndex;
}

template<class T>
void TStack<T>::put(const T& val) {

	if (pMem == nullptr) {
		throw SetRetCode(DataNoMem);
	}
	else if (isFull()) {
		throw SetRetCode(DataFull);
	}
	else {
		// ���������� �������, � �� ������ ++highIndex, ����� �� ������������ ����� put � �������-��������
		highIndex = getNextIndex(highIndex);
		pMem[highIndex] = val;
		dataCount++;
	}

}

template<class T>
T TStack<T>::get() {

	if (pMem == nullptr) {
		throw SetRetCode(DataNoMem);
	}
	else if (isEmpty()) {
		throw SetRetCode(DataEmpty);
	}
	else {
		dataCount--;
		return pMem[highIndex--];
	}

}

template<class T>
void TStack<T>::print() {

	for (int i = 0; i < dataCount; i++) {
		std::cout << pMem[i] << " ";
	}
	std::cout << std::endl;

}

#endif