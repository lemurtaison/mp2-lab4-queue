// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tdataroot.h - Copyright (c) Гергель В.П. 28.07.2000 (06.08)
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - базовый (абстрактный) класс - версия 3.2
//   память выделяется динамически или задается методом SetMem

#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "TDataCom.h"

#define DEF_MEM_SIZE 25  // размер памяти по умолчанию
#define DataEmpty -101  // СД пуста
#define DataFull -102  // СД переполнена
#define DataNoMem -103  // нет памяти

enum TMemType {
	MEM_HOLDER,
	MEM_RENTER
};

template<class T>
class TDataRoot: public TDataCom {
protected:
  T* pMem;
  int memSize;
  int dataCount;
  TMemType memType;
  void setMem(void* p, int size);

public:
  virtual ~TDataRoot();
  TDataRoot(int size = DEF_MEM_SIZE);
  virtual bool isEmpty(void) const;
  virtual bool isFull(void) const;
  virtual void put(const T& val) = 0;
  virtual T get (void) = 0;
  virtual void print() = 0;
};

template<class T>
TDataRoot<T>::TDataRoot(int size) : TDataCom() {

	memSize = size;
	dataCount = 0;

	if (size == 0) {
		memType = MEM_RENTER;
	}
	else {
		memType = MEM_HOLDER;
		pMem = new T[memSize];
	}

}

template<class T>
TDataRoot<T>::~TDataRoot() {

	delete[] pMem;

}

template<class T>
void TDataRoot<T>::setMem(void* p, int Size) {

	if (memType == MEM_HOLDER) {
		delete[] pMem;
	}

	memType = MEM_RENTER;
	pMem = (T)p;
	memSize = Size;

}

template<class T>
bool TDataRoot<T>::isEmpty(void) const {

	return dataCount == 0;

}

template<class T>
bool TDataRoot<T>::isFull(void) const {

	return dataCount == memSize;

}

#endif