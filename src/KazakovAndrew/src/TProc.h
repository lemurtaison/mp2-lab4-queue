#ifndef __TPROC_H__
#define __TPROC_H__

// ������ ����, ������������, ��� ������������ ����� ��� �������� �������
// ����� ������������ �������� (������ �� q2). �� ������ ���� ����� ����
#define JOB_DURATION_DISABLED -1
#define MAX_CPU_PERFORMANCE 100

class TProc {
protected:
	bool isWorkingStatus;
	int q2; // "������������������" ����������, ����������� [0; MAX_CPU_PERFORMANCE]
	int jobDuration; // ������������ �������� ������� � ������

public:
	TProc(int q2);
	void cycle();
	void startNewJob(int duration = JOB_DURATION_DISABLED);
	bool isWorking();
};

#endif