#include <cstdlib>
#include <ctime>
#include <iostream>
#include "TJobStream.h"

int main() {
	srand(time(0));

	TJobStream jobStream(10, 90, 20);

	jobStream.startJobStream(5000);
	jobStream.showStats();

	return 0;
}