#ifndef __TPROC_H__
#define __TPROC_H__

#include "TJobStreamer.h"

enum STATUS {IDLE, BUSY};

void Emulate(const Uint, const Uint, const Uint);

#endif
