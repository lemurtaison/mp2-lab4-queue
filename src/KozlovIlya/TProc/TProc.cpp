#include "TProc.h"

void Emulate(const Uint tacts_val, const Uint queue_len, const Uint max_op_len)
{
	Uint current_queue_len = 0;
	Uint current_op_len = 0;
	Uint next_op = 0;
	TJobStreamer Stream(queue_len, max_op_len);
	STATUS ProcStat = IDLE;
	
	for (int i = 0; i < tacts_val; ++i)
	{
		if (current_queue_len > queue_len) Stream.AddMissed();
		else
		{
			Stream.PutOp();
			current_queue_len++;
		}

		if (ProcStat == IDLE)
		{
			if (next_op == 0) current_op_len = Stream.GetOp();
			else current_op_len = next_op;

			if (current_op_len > 0)
			{
				Stream.AddDone();
				ProcStat = BUSY;
				current_queue_len--;
			}
			else
			{
				Stream.AddIdle();
				current_queue_len--;
			}
		}
		else 
		{
			current_op_len--;
			if (next_op == 0)
				next_op == Stream.GetOp();
		}

		if (current_op_len == 0) ProcStat = IDLE;
	}

	Stream.ShowInfo();
}
