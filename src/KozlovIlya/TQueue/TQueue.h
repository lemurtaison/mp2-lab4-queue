#ifndef __TQUEUE_H__
#define __TQUEUE_H__

#include "TStack.h"

template<class VTP = int>
class TQueue : public TStack<VTP> 
{
protected:
	int LowIndex;
	virtual int GetNextIndex(int index) override;

public:
	TQueue(int Size = DefMemSize);
	virtual VTP Get() override;
	virtual void Print() override;
};

template<class VTP>
TQueue<VTP>::TQueue(int Size) : TStack(Size), LowIndex(0) {};

template<class VTP>
int TQueue<VTP>::GetNextIndex(int index) { return ++index % MemSize; }

template<class VTP>
VTP TQueue<VTP>::Get()
{

	if (pMem == nullptr) throw SetRetCode(DataNoMem);
	else if (IsEmpty()) throw SetRetCode(DataEmpty);

	else
	{
		int oldIndex = LowIndex;
		LowIndex = GetNextIndex(LowIndex);
		DataCount--;
		return pMem[oldIndex];
	}

}

template<class VTP>
void TQueue<VTP>::Print() {

	for (int i = 0, j = LowIndex; i < DataCount; j = GetNextIndex(j), ++i) {
		std::cout << pMem[j] << " ";
	}
	std::cout << std::endl;

}

#endif