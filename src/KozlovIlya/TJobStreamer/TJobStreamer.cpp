#include "TJobStreamer.h"

TJobStreamer::TJobStreamer(const Uint DataLen, const Uint MaxLen): datalen(DataLen), MAXLEN(MaxLen)
{ srand(time(0)); }

Uint TJobStreamer::GetOp() { return Chan.Get(); }

bool TJobStreamer::RandOp()
{
	Uint temp = rand() % 10 + 1;
	if (temp > 5) return true;
	else return false;
}

void TJobStreamer::PutOp()
{
	Uint temp;
	if (RandOp() == 1) temp = rand() % MAXLEN;
	else temp = 0;
	Chan.Put(temp);
}

void TJobStreamer::AddMissed() { missed++; }
void TJobStreamer::AddDone() { done++; }
void TJobStreamer::AddIdle() { idle++; }

void TJobStreamer::ShowInfo()
{
	std::cout << "Tacts missed: " << missed << std::endl;
	std::cout << "Tacts done: " << done << std::endl;
	std::cout << "Tacts idle: " << idle << std::endl;
}