#include "TQueue.h"

#include <stdlib.h>
#include <time.h>
#include <iostream>

typedef unsigned int Uint;

class TJobStreamer
{
protected:

	Uint MAXLEN;
	Uint missed=0;
	Uint done=0;
	Uint idle=0;
	Uint datalen;

	bool RandOp();

	TQueue<Uint> Chan;

public:
	TJobStreamer(const Uint, const Uint);

	Uint GetOp();
	void PutOp();

	void AddMissed();
	void AddDone();
	void AddIdle();

	void ShowInfo();
};