# Отчёт по выполненной работе "Структура данных Очередь"
## Введение

**Цель данной работы** — практическое освоение динамической структуры данных **Очередь**. С этой целью в лабораторной работе изучаются различные варианты структуры хранения очереди и разрабатываются методы и программы решения задач с использованием очередей. В качестве области приложений выбрана тема эффективной организации выполнения потока заданий на вычислительных системах.

Для начала вспомним определение очереди:

**Очередь** (англ. queue) – схема запоминания информации, при которой каждый вновь поступающий ее элемент занимает крайнее положение (конец очереди). При выдаче информации из очереди выдается элемент, расположенный в очереди первым (начало очереди), а оставшиеся элементы продвигаются к началу; следовательно, элемент, поступивший первым, выдается первым. 

## Разработка простой вычислительной системы на основе очереди
Очередь нами будет использована в первую очередь для реализации простой вычислительной системы с одним процессором. Опишем далее подробности того, что будет представлять из себя данная ВС.

Для ВС с одним процессором и однопрограммным последовательным режимом выполнения поступающих заданий требуется разработать программную систему для имитации процесса обслуживания заданий в ВС. При построении модели функционирования ВС должны учитываться следующие основные моменты обслуживания заданий:

- генерация нового задания;
- постановка задания в очередь для ожидания момента освобождения процессора;
- выборка задания из очереди при освобождении процессора после обслуживания очередного задания.

По результатам проводимых вычислительных экспериментов система имитации должна выводить информацию об условиях проведения эксперимента (интенсивность потока заданий, размер очереди заданий, производительность процессора, число тактов имитации) и полученные в результате имитации показатели функционирования вычислительной системы, в т.ч.

- количество поступивших в ВС заданий;
- количество отказов в обслуживании заданий из-за переполнения очереди;
- среднее количество тактов выполнения заданий;
- количество тактов простоя процессора из-за отсутствия в очереди заданий для обслуживания.

Показатели функционирования вычислительной системы, получаемые при помощи систем имитации, могут использоваться для оценки эффективности применения ВС; по результатам анализа показателей могут быть приняты рекомендации о целесообразной модернизации характеристик ВС (например, при длительных простоях процессора и при отсутствии отказов от обслуживания заданий желательно повышение интенсивности потока обслуживаемых заданий и т.д.).

## Реализация базовых классов
Здесь мы используем уже известную нам иеарархию классов TDataCom -> TDataRoot -> TStack (последний класс со стеком был немного модифицирован с целью реализации наследования от него).

### Класс TDataCom
```C++
#ifndef __DATACOM_H__
#define __DATACOM_H__

#define DataOK   0
#define DataErr -1

// TDataCom является общим базовым классом
class TDataCom {
protected:
  int RetCode; // Код завершения
  int SetRetCode(int ret) { return RetCode = ret; }

public:
  TDataCom(): RetCode(DataOK) {}
  virtual ~TDataCom() = 0 {}

  int GetRetCode() {
    int temp = RetCode;
    RetCode = DataOK;
    return temp;
  }
};

#endif
```
### Класс TDataRoot
```C++
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tdataroot.h - Copyright (c) Гергель В.П. 28.07.2000 (06.08)
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - базовый (абстрактный) класс - версия 3.2
//   память выделяется динамически или задается методом SetMem

#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "TDataCom.h"

#define DefMemSize 25  // размер памяти по умолчанию
#define DataEmpty -101  // СД пуста
#define DataFull -102  // СД переполнена
#define DataNoMem -103  // нет памяти

enum TMemType { MEM_HOLDER, MEM_RENTER };

template<class VTP>
class TDataRoot : public TDataCom {
protected:
	VTP* pMem;
	int MemSize;
	int DataCount;
	TMemType MemType;
	void SetMem(void* p, int Size);

public:
	virtual ~TDataRoot();
	TDataRoot(int Size = DefMemSize);
	virtual bool IsEmpty(void) const;
	virtual bool IsFull(void) const;
	virtual void Put(const VTP& val) = 0;
	virtual VTP Get(void) = 0;
	virtual void Print() = 0;
};

template<class VTP>
TDataRoot<VTP>::TDataRoot(int Size) : TDataCom()
{

	MemSize = Size;
	DataCount = 0;

	if (Size == 0)
	{
		MemType = MEM_RENTER;
	}
	else
	{
		MemType = MEM_HOLDER;
		pMem = new VTP[MemSize];
	}

}

template<class VTP>
TDataRoot<VTP>::~TDataRoot() { delete[] pMem; }


template<class VTP>
void TDataRoot<VTP>::SetMem(void* p, int Size)
{
	if (Size <= 0) throw SetRetCode(DataErr);

	else if (MemType == MEM_HOLDER)
	{
		if (Size > 0)
		{
			VTP *pTempMem = pMem;
			pMem = new VTP[Size];
			for (int i = 0; i < DataCount; ++i)
				pMem[i] = *(pTempMem + i);
			MemSize = Size;
			delete[]pTempMem;
		}
	}
	else if (MemType == MEM_RENTER)
	{
		MemSize = Size;
		for (int i = 0; i < DataCount; ++i)
			*((VTP*)(VTP*)p + i) = pMem[i];
		pMem = (VTP*)p;
	}
}

template<class VTP>
bool TDataRoot<VTP>::IsEmpty(void) const { return DataCount == 0; }

template<class VTP>
bool TDataRoot<VTP>::IsFull(void) const { return DataCount == MemSize; }

#endif
```
### Класс TStack
```c++
#ifndef __TSTACK_H__
#define __TSTACK_H__

#include <iostream>
#include "TDataRoot.h"

template<class VTP>
class TStack : public TDataRoot<VTP> {
protected:
	int HighIndex;
	virtual int GetNextIndex(int index);

public:
	TStack(int Size = DefMemSize);
	virtual void Put(const VTP&) override;
	virtual VTP Get() override;
	virtual void Print() override;
};

template<class VTP>
TStack<VTP>::TStack(int Size) : TDataRoot(Size), HighIndex(-1) { if (Size < 0) throw SetRetCode(DataNoMem); };

template<class VTP>
int TStack<VTP>::GetNextIndex(int index) { return ++HighIndex; }

template<class VTP>
void TStack<VTP>::Put(const VTP& val)
{

	if (pMem == nullptr) throw SetRetCode(DataNoMem);
	else if (IsFull())
	{
		SetMem(nullptr, MemSize + DefMemSize);
		pMem[++HighIndex] = val;
		DataCount++;
	}
	else 
	{
		HighIndex = GetNextIndex(HighIndex);
		pMem[HighIndex] = val;
		DataCount++;
	}

}

template<class VTP>
VTP TStack<VTP>::Get() {

	if (pMem == nullptr) throw SetRetCode(DataNoMem);
	else if (IsEmpty()) throw SetRetCode(DataEmpty);

	else
	{
		DataCount--;
		return pMem[HighIndex--];
	}

}

template<class VTP>
void TStack<VTP>::Print() {

	for (int i = 0; i < DataCount; i++) {
		std::cout << pMem[i] << " ";
	}
	std::cout << std::endl;

}

#endif
```
Очередь - всего лишь немного изменённый стек, поэтому класс TQueue с очередью будет наследовать от TStack. Сама очередь реализована в виде кольцевого буфера.

### Класс TQueue
```C++
#ifndef __TQUEUE_H__
#define __TQUEUE_H__

#include "TStack.h"

template<class VTP = int>
class TQueue : public TStack<VTP> 
{
protected:
	int LowIndex;
	virtual int GetNextIndex(int index) override;

public:
	TQueue(int Size = DefMemSize);
	virtual VTP Get() override;
	virtual void Print() override;
};

template<class VTP>
TQueue<VTP>::TQueue(int Size) : TStack(Size), LowIndex(0) {};

template<class VTP>
int TQueue<VTP>::GetNextIndex(int index) { return ++index % MemSize; }

template<class VTP>
VTP TQueue<VTP>::Get()
{

	if (pMem == nullptr) throw SetRetCode(DataNoMem);
	else if (IsEmpty()) throw SetRetCode(DataEmpty);

	else
	{
		int oldIndex = LowIndex;
		LowIndex = GetNextIndex(LowIndex);
		DataCount--;
		return pMem[oldIndex];
	}

}

template<class VTP>
void TQueue<VTP>::Print() {

	for (int i = 0, j = LowIndex; i < DataCount; j = GetNextIndex(j), ++i) {
		std::cout << pMem[j] << " ";
	}
	std::cout << std::endl;

}

#endif
```

### Тесты для класса TQueue

```C++
#include "gtest/gtest.h"
#include "TQueue.h"

// Creation
TEST(Queue, can_create_queue) {
	ASSERT_NO_THROW(TQueue queue);
}

TEST(Queue, can_create_queue_with_specified_length) {
	ASSERT_NO_THROW(TQueue queue(5));
}

TEST(Queue, cant_create_queue_with_negative_size) {
	ASSERT_ANY_THROW(TQueue queue(-5));
}

// Put/get
TEST(Queue, can_put_elem_in_queue_without_specifying_its_size) {
	TQueue queue;

	ASSERT_NO_THROW(queue.put(10));
}

TEST(Queue, can_put_elem_in_queue_with_specified_size) {
	TQueue queue(2);

	ASSERT_NO_THROW(queue.put(10));
}

TEST(Queue, can_get_elem) {
	TQueue queue(2);
	queue.put(7);

	ASSERT_NO_THROW(queue.get());
}

TEST(Queue, put_element_is_correct) {
	TQueue queue(2);
	queue.put(6);

	EXPECT_EQ(queue.get(), 6);
}

// Empty/full
TEST(Queue, empty_queue_is_empty) {
	TQueue queue(2);

	EXPECT_EQ(queue.isEmpty(), true);
}

TEST(Queue, not_full_queue_is_not_full) {
	TQueue queue(2);
	queue.put(3);

	EXPECT_EQ(queue.isFull(), false);
}

TEST(Queue, not_empty_queue_is_not_empty) {
	TQueue queue(2);
	queue.put(3);

	EXPECT_EQ(queue.isEmpty(), false);
}

TEST(Queue, full_queue_is_full) {
	TQueue queue(2);
	queue.put(3);
	queue.put(4);

	EXPECT_EQ(queue.isFull(), true);
}

// Other
TEST(Queue, cant_put_elem_when_queue_is_full) {
	TQueue queue(2);
	queue.put(3);
	queue.put(4);

	ASSERT_ANY_THROW(queue.put(5));
}
```
![](https://pp.userapi.com/c639630/v639630181/1640c/KzzpqwWEkn4.jpg)

## Вычислительная система
Вычислительная система - иммитация процессора, на который поступают задачи. Реализована данная система с помощью класса TJobStreamer, который помещает некоторые целые неотрицательные числа (заданные случайным образом) в очередь и извлекает их в случае необходимости (используется агрегация), и с помощью функции Emulate, которая имитирует процессор, который обрабатывает задачи по n тактов на задачу, где n - случайное число, извлеченное из очереди классом TJobStreamer.
### Класс TJobStreamer - описание и реализация
```C++
#include "TQueue.h"

#include <stdlib.h>
#include <time.h>
#include <iostream>

typedef unsigned int Uint;

class TJobStreamer
{
protected:

	Uint MAXLEN;
	Uint missed=0;
	Uint done=0;
	Uint idle=0;
	Uint datalen;

	bool RandOp();

	TQueue<Uint> Chan;

public:
	TJobStreamer(const Uint, const Uint);

	Uint GetOp();
	void PutOp();

	void AddMissed();
	void AddDone();
	void AddIdle();

	void ShowInfo();
};
```
```C++
#include "TJobStreamer.h"

TJobStreamer::TJobStreamer(const Uint DataLen, const Uint MaxLen): datalen(DataLen), MAXLEN(MaxLen)
{ srand(time(0)); }

Uint TJobStreamer::GetOp() { return Chan.Get(); }

bool TJobStreamer::RandOp()
{
	Uint temp = rand() % 10 + 1;
	if (temp > 5) return true;
	else return false;
}

void TJobStreamer::PutOp()
{
	Uint temp;
	if (RandOp() == 1) temp = rand() % MAXLEN;
	else temp = 0;
	Chan.Put(temp);
}

void TJobStreamer::AddMissed() { missed++; }
void TJobStreamer::AddDone() { done++; }
void TJobStreamer::AddIdle() { idle++; }

void TJobStreamer::ShowInfo()
{
	std::cout << "Tacts missed: " << missed << std::endl;
	std::cout << "Tacts done: " << done << std::endl;
	std::cout << "Tacts idle: " << idle << std::endl;
}
```
### TProc - описание и реализация
```C++
#ifndef __TPROC_H__
#define __TPROC_H__

#include "TJobStreamer.h"

enum STATUS {IDLE, BUSY};

void Emulate(const Uint, const Uint, const Uint);

#endif
```
```C++
#include "TProc.h"

void Emulate(const Uint tacts_val, const Uint queue_len, const Uint max_op_len)
{
	Uint current_queue_len = 0;
	Uint current_op_len = 0;
	Uint next_op = 0;
	TJobStreamer Stream(queue_len, max_op_len);
	STATUS ProcStat = IDLE;
	
	for (int i = 0; i < tacts_val; ++i)
	{
		if (current_queue_len > queue_len) Stream.AddMissed();
		else
		{
			Stream.PutOp();
			current_queue_len++;
		}

		if (ProcStat == IDLE)
		{
			if (next_op == 0) current_op_len = Stream.GetOp();
			else current_op_len = next_op;

			if (current_op_len > 0)
			{
				Stream.AddDone();
				ProcStat = BUSY;
				current_queue_len--;
			}
			else
			{
				Stream.AddIdle();
				current_queue_len--;
			}
		}
		else 
		{
			current_op_len--;
			if (next_op == 0)
				next_op == Stream.GetOp();
		}

		if (current_op_len == 0) ProcStat = IDLE;
	}

	Stream.ShowInfo();
}
```
### Демонстрационная программа
```C++
#include "TProc.h"

void main()
{
	const int VAL = 100;


	std::cout << "\t\t\t\t\t\tTQUEUE DEMO PROGRAM\n";
	int n;
	TQueue<int> A(VAL);
	for (int i = 0; i < VAL; i++)
	{
		A.Put(VAL - i);
	}
	std::cout << "\n\nPRINTING THE TQUEUE\n";
	A.Print();

	std::cout << "\n\nGETTING THE TQUEUE`S ELEMENTS\n";
	for (int i = 0; i < VAL; i++)
	{
		n = A.Get();
		std::cout << n << '\n';
	}

	std::cout << "\n\n\n\n";

	std::cout << "\t\t\t\t\t\tTPROC DEMO PROGRAM\n";
	Emulate(20,5,3);

	std::cout << "\n\n\n\n";
	system("pause");
}
```
![](https://pp.userapi.com/c639630/v639630181/1641e/GCeYvCjhF2I.jpg)
![](https://pp.userapi.com/c639630/v639630181/16415/BrL5_o1FD-U.jpg)