#include <stdlib.h>
#include <time.h>
#include <iostream>
using namespace std; 
class TJobStream
{
private:
	int auto_increment = 0; // ������� �������
	double q; // ����������� ���������� �������
public:
	TJobStream(){}
	TJobStream(double q);
	int CreateJob(); // ������� ��������� ������
	bool EndJob() const; // ������� ���������� ������
	bool TaskCount() const; // ������� ���������� ������
	int GetCount() const; // ����� ���� ������� 
};
TJobStream::TJobStream(double _q) 
{
	q = _q;
	srand(time(NULL));
}

int TJobStream::CreateJob()
{
	int result = 0;
	if (rand() % 100 < q * 100){
		result = ++auto_increment;
	}
	return result;
}

bool TJobStream::EndJob() const
{
	bool result = false;
	if (rand() % 100 < q * 100)
		result = true;
	return result;
}

int TJobStream::GetCount() const
{
	return auto_increment;
}

