#include "stdio.h"
#include <iostream>
#include <locale>
#include "TProc.h"
using namespace std;
#define tacts_size 500 // ���������� ������
#define queue_max_size 25 // ������������ ����� �������
#define proc1_q 0.3 // ����������� ���������� ������� �� ������ ����� ��� 1-�� ����������
#define proc2_q 0.45 // ����������� ���������� ������� �� ������ ����� ��� 1-�� ����������
#define new_task_q 0.75 // ����������� ��������� ������ ������� �� ������ �����
int main(){
	setlocale(LC_ALL, "rus");
	int failure_size = 0; // ����� �������
	int new_task;
	TQueue queue = TQueue(queue_max_size); // ������� �������
	TJobStream jobStream = TJobStream(new_task_q);
	TProc proc1 = TProc(proc1_q, &queue); // 1-�� ���������
	TProc proc2 = TProc(proc2_q, &queue); // 2-�� ��������� 
	for (int i = 0; i < tacts_size; i++){
		new_task = jobStream.CreateJob();
		if (queue.IsFull()){
			failure_size++;
		}
		else {
			if (new_task > 0){
				queue.Put(new_task);
			}
		}
		proc1.add_tact();
		proc2.add_tact();
	}
	cout << "���������� ����������� � �������������� ������� �������: " << jobStream.GetCount() << endl;
	cout << "������� �������: " << 100.0*failure_size / jobStream.GetCount() << "%" << endl;
	cout << "����� ������� ����������� 1-� �����������: " << proc1.get�ompletedSize() << endl;
	cout << "����� ������� ����������� 2-� �����������: " << proc2.get�ompletedSize() << endl;
	cout << "C������ ���������� ������ ���������� ������� 1-�� ����������: " << proc1.Productivity() << endl;
	cout << "C������ ���������� ������ ���������� ������� 2-�� ����������: " << proc2.Productivity() << endl;
	cout << "������� ������ ������� 1-�� ����������: " << 100.0 * proc1.getIdleSize() / tacts_size << "%" << endl;
	cout << "������� ������ ������� 2-�� ����������: " << 100.0 * proc2.getIdleSize() / tacts_size << "%" << endl;
	
	
	return 0;
}