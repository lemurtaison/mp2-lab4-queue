#ifndef __TDATSTACK__
#define __TDATSTACK__

#include "tdataroot.h"
#include <iostream>
using namespace std;

class TStack : public TDataRoot
{
private:
	int top;

public:
	TStack(int Size = DefMemSize) : TDataRoot(Size) { top = -1; }
	void Put(const TData & v); // �������� ��������
	TData Get(); // ������� ��������
	TData GetVertex(); // �������� �������� �� ������� �����
	int IsValid(); // ������������ ���������
	void Print(); // ������ ��������
};


void TStack::Put(const TData & v) { // �������� �������� 
	if (!IsValid())
		SetRetCode(DataNoMem);
	else if (IsFull()) {
		void* p = nullptr;
		SetMem(p, MemSize + DefMemSize);
		pMem[++top] = v;
		DataCount++;
	}
	else {
		pMem[++top] = v;
		DataCount++;
	}
}

TData TStack::Get() { // ������� ��������
	if (!IsValid()) 
		SetRetCode(DataNoMem);
	else if (IsEmpty()) 
		SetRetCode(DataEmpty);
	else {
		DataCount--;
		return pMem[top--];
	}
	return -1;
}

TData TStack::GetVertex() { // ������� ��������
	if (!IsValid())
		SetRetCode(DataNoMem);
	else if (IsEmpty())
		SetRetCode(DataEmpty);
	else {
		return pMem[top];
	}
	return -1;
}

int TStack::IsValid() { // ������������ ���������
	if (pMem == nullptr || MemSize < DataCount || DataCount < 0 || MemSize < 0) 
		return 0;
	return 1;
}

void TStack::Print() { // ������ ��������
	for (int i = 0; i < DataCount; i++)
		cout << pMem[i] << " ";
	cout << endl;
}

#endif