#include "TQueue.h"
#include "TJobStream.h"
#include <iostream>

class TProc
{
private:
	TQueue * queue;
	TJobStream jobStream;
	bool working = false;
	int tacts = 0;
	int completed_size = 0;
	int idle_size = 0;
public:
	TProc(double q, TQueue * queue);
	void add_tact(); // ��������� 1 ����
	double Productivity(); // ������������������ ����������
	int getIdleSize(); // ����� ������ �������
	int get�ompletedSize(); // ����� ����������� �������
}; 

TProc::TProc(double _q, TQueue * _queue)
{
	if (_q < 0 || _q > 1)
		throw("Q out of bounds exception");
	jobStream = TJobStream(_q);
	queue = _queue;
}
void TProc::add_tact()
{
	tacts++;
	if (!working){
		if (!(*queue).IsEmpty()){
			(*queue).Get();
			working = true;
		}
		else
			idle_size++;
	}
	if (working && jobStream.EndJob()){
		working = false;
		completed_size++;
	}
}
double TProc::Productivity(){
	return 1.0 * tacts / completed_size;
}
int TProc::getIdleSize(){
	return idle_size;
}
int TProc::get�ompletedSize(){
	return completed_size;
}