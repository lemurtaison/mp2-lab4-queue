#include "../Stack/tdatstack.h"
#include <iostream>
using namespace std;
class TQueue : public TStack
{
protected:
	int Li;
	virtual int GetNextIndex(int index);
public:
	TQueue(int Size = DefMemSize) : TStack(Size){ Li = 0; }
	virtual TData Get();
};

int TQueue::GetNextIndex(int index)
{
	return ++index % MemSize;
}

TData TQueue::Get() {
	TData temp = -1;
	if (pMem == 0) SetRetCode(DataNoMem);
	else if (IsEmpty()) SetRetCode(DataEmpty);
	else {
		temp = pMem[Li];
		Li = GetNextIndex(Li);
		DataCount--;
	}
	return temp;
}
