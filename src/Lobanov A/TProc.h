#ifndef __TPROC_H__
#define __TPROC_H__

#include <ctime>
#include <cstdlib>
#include "TJobStream.h"
#include "tqueue.h"

#define Max_Tacts 1000000

enum T_Status { ON_WORK, FREE };

class TProc
{
private:
    T_Status Status;
	TQueue que1;
	TQueue que2;
	TJobStream TJob;
    int ID; //����� �������� �������
	int q2; //����������� ���������� �������� ������� (0 - 99)
    int idletacts; //����� �������
    int CountTasks; //����������� �������
    int rejectcount; //����������� �������
public:
    TProc(int Q1, int Q2, int _size1, int _size2);
    void Process();
	void PrintReport() const;
};

#endif