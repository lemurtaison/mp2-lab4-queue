#ifndef __TPROC_CPP__
#define __TPROC_CPP__

#include "TProc.h"
#include <locale>
#include <iostream>

using namespace std;

TProc::TProc(int  Q1, int Q2, int _size1,  int _size2) : TJob(Q1), q2(Q2), que1(_size1), que2(_size2)
{
    ID = 0;
	idletacts = 0;
    CountTasks = 0;
    rejectcount = 0;
	srand(time(NULL));
	Status = FREE;
}

void TProc::Process()
{
	
	for ( int i = 0; i < Max_Tacts; i++)
	{
	    if (TJob.GenerateJob())
	    {
            ID++;
			if(!(que1.IsFull()))	    
				que1.Put(ID);		
		    else if (!(que2.IsFull()) && que1.IsFull()) 
				que2.Put(ID);
		    else 
			    rejectcount++;   
	     }
	     
		if (Status == ON_WORK )
		     if(rand() % 100 < q2)
			     Status = FREE;
	     
		 if (Status == FREE)  
	     {
	         if (!(que1.IsEmpty()))
			 {    
				 que1.Get();
			     CountTasks++;
			     Status = ON_WORK;
			 }
			 else if (!(que2.IsEmpty()) && que1.IsEmpty())
		     {
				 que2.Get();
			     CountTasks++;
			     Status = ON_WORK;
		     }
			 else
				 idletacts++;
	     }
	}
}
void TProc::PrintReport() const
{
    setlocale(LC_ALL, "rus");
	double arr;
	cout << endl;
	cout << "���������� ������ ����������: " << Max_Tacts << endl;
	cout << "����������� ����������� ������ �������: " << TJob.GetChance() << "%" << endl;
	cout << "����������� ���������� �������� �������: " << q2 << "%" << endl;
	cout << "���������� ����������� � �������������� ������� ������� � ������� ����� �������� ��������: " << TJob.GetTasks() << endl;
	cout << "���������� ������� ������������ �����������: " << CountTasks << endl;
	cout << "���������� ������� �� ������������ �����������: " << TJob.GetTasks() - CountTasks << endl;
	cout << "���������� ������� � ������������ ������� ��-�� ������������ �������: " << rejectcount << endl;
	arr = (rejectcount * 100.0) / (float)TJob.GetTasks();
	cout << "������� �������: " << arr << "%" << endl;
	arr = (Max_Tacts - idletacts) / CountTasks;
	cout << "C������ ���������� ������ �� ���������� ������ �������: " << arr << endl;
	cout << "���������� ������ �������: " << idletacts << endl;
	arr = (idletacts * 100) / Max_Tacts;
	cout << "������� �������: " << arr  << "%" << endl;
}

#endif

