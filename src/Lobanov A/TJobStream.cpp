#ifndef __TJOBSTREAM_CPP__
#define __TJOBSTREAM_CPP__

#include "TJobStream.h"

TJobStream::TJobStream(int Q1) 
{
	Jbs = 0;
	q1 = Q1;
}

bool TJobStream::GenerateJob()
{
	if (rand() % 100 < q1)
	{
		Jbs++;
		return true;
	}
	return false;
}

int TJobStream::GetTasks() const
{
	return Jbs;
}
int TJobStream::GetChance() const
{
	return q1;
}

#endif