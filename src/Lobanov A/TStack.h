#ifndef __TSTACK_H__
#define __TSTACK_H__

#include "tdataroot.h"

class TStack : public TDataRoot
{
protected:
	int Hi;
    virtual int GetNextIndex(int index);
public:
	TStack(int Size = DefMemSize) : TDataRoot(Size) { Hi = -1; }
	virtual void Put(const TData &Val);
	virtual TData Get(void);
	int Get_Size() { return MemSize; }
	void Print();
	virtual int IsValid();
};

#endif

