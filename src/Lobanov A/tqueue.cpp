#ifndef __TQUEUE_CPP__
#define __TQUEUE_CPP__

#include "tqueue.h"

int TQueue::GetNextIndex(int index)
{
    return ++index % MemSize;
}

TData TQueue::Get(void)
{
	TData temp = -1;
	if (pMem == nullptr)
	    throw SetRetCode(DataNoMem);
	else if (IsEmpty())	
		throw SetRetCode(DataEmpty);
	else {
	    temp = pMem[Li];
	    Li = GetNextIndex(Li);
	    DataCount--;
	}
    return temp;
}

#endif