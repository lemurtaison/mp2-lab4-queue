#ifndef __TJOBSTREAM_H__
#define __TJOBSTREAM_H__

#include <ctime>
#include <cstdlib>

class TJobStream
{
private:
    int q1; //����������� ��������� ������ ������� (0-99)
    int Jbs; //���-�� �������
public:
	TJobStream(int Q1);
	bool GenerateJob(); //���������� ����� �������
	int GetTasks() const; //���������� ����� ���-�� �������
	int GetChance() const; //���������� ����������� ��������� ������ �������
};

#endif