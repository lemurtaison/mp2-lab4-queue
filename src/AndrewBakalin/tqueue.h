#ifndef _TQUEUE_
#define _TQUEUE_

#include "tstack.h"

template <class T>
class TQueue : public TStack<T>
{
protected:
	int Li; // нижняя граница
	int GetNextIndex(int index) override;

public:
	TQueue(int Size = MaxMemSize) : TStack<T>(Size), Li(0) {};
	T Get() override; // получить элемент из очереди с его последующим удалением из нее
	void Print() override; // печать элементов из очереди
};

template <class T>
int TQueue<T>::GetNextIndex(int index)
{
	return ++index % MemSize;
}

template <class T>
T TQueue<T>::Get()
{
	T temp;

	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	else if (IsEmpty())
		throw SetRetCode(DataEmpty);
	else
	{
		temp = pMem[Li];
		Li = GetNextIndex(Li);
		DataCount--;
		return temp;
	}
}

template <class T>
void TQueue<T>::Print()
{
	for (int i = Li, j = 0; j < DataCount; j++, i = GetNextIndex(i))
		std::cout << pMem[i] << std::endl;
}

#endif