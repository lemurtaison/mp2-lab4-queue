#include "tproc.h"

void TProc::Tact()
{
	if (rand() % 100 < q2)
		OnWork = false;
}

void TProc::NewJob()
{
	OnWork = true;
}

bool TProc::IsWorking()
{
	return OnWork;
}