#include "tqueue.h"
#include <gtest/gtest.h>

TEST(TQueue, cant_create_queue_with_negative_size)
{
	ASSERT_ANY_THROW(TQueue<int>(-1));
}

TEST(TQueue, null_size_queue_is_created_correctly)
{
	ASSERT_NO_THROW(TQueue<int>(0));
}

TEST(TQueue, can_create_queue_with_positive_length)
{
	ASSERT_NO_THROW(TQueue<int>(10));
}

TEST(TQueue, can_create_copied_queue)
{
	TQueue<int> q1(2);

	ASSERT_NO_THROW(TQueue<int> q2 = q1);
}

TEST(TQueue, copied_queue_is_equal_to_source_one)
{
	TQueue<int> q1(2);

	q1.Put(1);
	q1.Put(2);

	TQueue<int> q2 = q1;

	EXPECT_EQ(1, (q2.Get() == 1) && (q2.Get() == 2));
}

TEST(TQueue, copied_queue_has_its_own_memory)
{
	TQueue<int> q1(2);

	q1.Put(1);
	q1.Put(2);

	TQueue<int> q2 = q1;

	q1.Get();

	EXPECT_EQ(1, q2.Get());
}

TEST(TQueue, can_put_element_when_queue_isnt_full)
{
	TQueue<int> q1(2);

	ASSERT_NO_THROW(q1.Put(1));
}

TEST(TQueue, cant_put_element_when_queue_is_full)
{
	TQueue<int> q(2);

	q.Put(1);
	q.Put(2);

	ASSERT_ANY_THROW(q.Put(3));
}

TEST(TQueue, can_get_element_when_queue_isnt_empty)
{
	TQueue<int> q(2);

	q.Put(2);

	ASSERT_NO_THROW(q.Get());
}

TEST(TQueue, cant_get_element_when_queue_is_empty)
{
	TQueue<int> q(2);

	ASSERT_ANY_THROW(q.Get());
}

TEST(TQueue, put_and_get_works_correctly)
{
	TQueue<int> q(1);

	q.Put(1);

	EXPECT_EQ(1, q.Get());
}

TEST(TQueue, IsEmpty_works_correctly)
{
	TQueue<int> q(1);

	EXPECT_EQ(1, q.IsEmpty());
}

TEST(TQueue, IsFull_works_correctly)
{
	TQueue<int> q(1);

	q.Put(1);

	EXPECT_EQ(1, q.IsFull());
}

TEST(TQueue, ring_buffer_works_correctly)
{
	TQueue<int> q(3);

	q.Put(1);
	q.Put(2);
	q.Put(3);
	q.Get();
	q.Put(4);

	EXPECT_EQ(1, q.Get() == 2 && q.Get() == 3 && q.Get() == 4);
}