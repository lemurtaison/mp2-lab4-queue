#include "tjobstream.h"

TJobStream::TJobStream(int _q1, int _q2, int QueueLen) : q1(_q1), Proc(_q2), Queue(QueueLen)
{
	ID = JobsMissed = Tacts = IdleTacts = 0;
	srand(time(NULL));
}

void TJobStream::MakeTact()
{
	if (!Proc.IsWorking() && !Queue.IsEmpty())
	{
		Queue.Get();
		Proc.NewJob();
	}

	if (!Proc.IsWorking())
		IdleTacts++;
	else
		Tacts++;

	Proc.Tact();
}

void TJobStream::Start(int NumOfTacts)
{
	for (int i = 0; i < NumOfTacts; i++)
	{
		if (rand() % 100 < q1)
		{
			ID++;
			if (!Queue.IsFull())
				Queue.Put(ID);
			else
				JobsMissed++;
		}

		MakeTact();
	}

	while (!Queue.IsEmpty())
		MakeTact();
}

void TJobStream::PrintStatictics()
{
	std::cout << "Total number of tacts: " << Tacts + IdleTacts << std::endl;
	std::cout << "Total number of jobs: " << ID << std::endl;
	std::cout << "Number of missing jobs: " << JobsMissed << " (" << (double)JobsMissed / (double)ID * 100 << "%)" << std::endl;
	std::cout << "Average number of tacts per job: " << (double)Tacts / (double)(ID - JobsMissed) << std::endl;
	std::cout << "Number of idle tacts: " << IdleTacts << " (" << (double)IdleTacts / (double)(Tacts + IdleTacts) * 100 << "%)" << std::endl;
}