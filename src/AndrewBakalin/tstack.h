#ifndef _TSTACK_
#define _TSTACK_

#include <iostream>
#include "tdataroot.h"

#define MaxMemSize 20

template <class T>
class TStack : public TDataRoot<T>
{
protected:
	int Hi; // ������� �������
	int GetNextIndex(int index) override; // �������� ��������� ������

public:
	TStack(int Size = MaxMemSize) : TDataRoot<T>(Size), Hi(-1) {};
	TStack(const TStack<T>&);

	void Put(const T&) override; // �������� ������� � ����
	T Get() override; // ����� ������� �� ����� � ��� ����������� ��������� �� ����

	void Print() override; // ������ ��������� �����
};

template <class T>
TStack<T>::TStack(const TStack<T> &st) : TDataRoot<T>(st)
{
	Hi = st.Hi;
}

template <class T>
void TStack<T>::Put(const T &data)
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	else if (IsFull())
		throw SetRetCode(DataFull);
	else
	{
		Hi = GetNextIndex(Hi);
		pMem[Hi] = data;
		DataCount++;
	}
}

template <class T>
T TStack<T>::Get()
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	else if (IsEmpty())
		throw SetRetCode(DataEmpty);
	else
	{
		DataCount--;
		return pMem[Hi--];		
	}
}

template <class T>
void TStack<T>::Print()
{
	for (int i = Hi; i > -1; i--)
		std::cout << pMem[i] << std::endl;
}

template <class T>
int TStack<T>::GetNextIndex(int index)
{
	return ++index;
}

#endif