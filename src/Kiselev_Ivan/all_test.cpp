#include "gtest.h"
#include "proceccor.h"
#include <iostream>

TEST(Quest, Create_New_Quest)
{
	EXPECT_NO_THROW(Quest quest(5));
}

TEST(Quest, Get_Time_Quest)
{
	Quest quest(5);
	EXPECT_EQ(quest.GetTime(), 5);
}

TEST(Queue, Create_New_Queue)
{
	EXPECT_NO_THROW(Queue queue(10));
}

TEST(Queue, Put_Elem)
{
	Queue queue(4);
	Quest quest(4);
	int miss = 1;
	EXPECT_NO_THROW(queue.Put(&quest, &miss));
}

TEST(Queue, Get_Elem)
{

	Queue queue(4);
	Quest quest(4);
	int miss = 1;
	queue.Put(&quest, &miss);
	EXPECT_EQ(queue.Get().GetTime(),4);
}

TEST(Queue, Queue_Is_Empty)
{
	Queue queue(4);
	Quest quest(4);
	int miss = 1;
	queue.Put(&quest, &miss);
	EXPECT_FALSE(queue.IsEmpty());
	EXPECT_FALSE(queue.IsFull());
}

TEST(Queue, Queue_Is_Full)
{
	Queue queue(2);
	Quest quest(4);
	int miss = 1;
	queue.Put(&quest, &miss);
	queue.Put(&quest, &miss);
	EXPECT_TRUE(queue.IsFull());
}

TEST(Queue, Get_Elem_From_Empty_Queue)
{
	Queue queue(2);
	Quest quest(4);
	int miss = 1;
	queue.Put(&quest, &miss);
	queue.Get();
	EXPECT_ANY_THROW(queue.Get());
}

TEST(Queue, Put_Elem_In_Full_Queue)
{
	Queue queue(2);
	Quest quest(4);
	int miss = 0;
	queue.Put(&quest, &miss);
	queue.Put(&quest, &miss);
	queue.Put(&quest, &miss);
	EXPECT_EQ(miss,1);
}

TEST(Queue, Get_How_Many_Elements)
{
	Queue queue(4);
	Quest quest(4);
	int miss = 1;
	queue.Put(&quest, &miss);
	queue.Put(&quest, &miss);
	queue.Put(&quest, &miss);
	EXPECT_EQ(queue.GetHMElements(), 3);
}
