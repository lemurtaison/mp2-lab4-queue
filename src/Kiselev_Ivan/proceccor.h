#include <queue.h>
#include <stdlib.h>
#include <ctime>
#include <Windows.h>
#include <conio.h>
#include <stddef.h>

using namespace std;

#ifndef _Proceccor_
#define _Proceccor_

const time_t TimeCycle = 1;		//����� �� ����(���������� ��� stoptime)
const int HMCycle = 1000;				//���������� ������, ������� ������ ��������� ���������
const int Procent = 17;				//����������� ��������� ������� �� ������ �����
const int TimeToComplete = 15;		//������������ ���������� ������ �� ���������� (�� 1 �� TimeToComplete)

void Cycle(void)		
{
	Sleep(TimeCycle);
};

class Proceccor
{
private:
	int NumberOfCycle = 0;
public:
	void Wait(Queue* q2ue,int *HMcompleted, int * HMquests,int* HMmiss);						//������� �������� �������
	void Perform(int time,int * HMquests,int* HMmiss,Queue * queue);											//������� "����������"
	void Working(Queue * q2ue);										//������ ���������� � ����� �����
	void QuestGeneration(Queue * q2ue,int *HMquests,int * HMmiss);	//��������� ������� � �������������� ����������� � �������
};

void Proceccor::Wait(Queue * q2ue ,int *HMcompleted , int * HMquests,int * HMmiss)
{
	if (q2ue->IsEmpty())
	{
		Cycle();
		NumberOfCycle += 1;
		QuestGeneration(q2ue, HMquests, HMmiss);
	}
	else
	{
		Perform(q2ue->Get().GetTime(),HMquests,HMmiss,q2ue);
		*(HMcompleted)+=1;
	};
}

void Proceccor::Perform(int time,int * HMquests,int* HMmiss,Queue * q2ue)
{
	for (int i = 0; i < time; i++)
	{
		Cycle();
		NumberOfCycle++;
		QuestGeneration(q2ue, HMquests, HMmiss);
	};
}

void Proceccor::Working(Queue * q2ue)
{	
	srand(time(NULL));
	int HMquests=0,HMcompleted=0,HMmiss=0;
	bool generations=0;
	while (NumberOfCycle < HMCycle)
	{				
		Wait(q2ue, &HMcompleted, &HMquests,&HMmiss);
	};
	cout << endl << "Quests created: " << HMquests << endl << "Quests comleted: " << HMcompleted << endl << "Quests missing: " << HMmiss << endl << "Total cycles: " << NumberOfCycle << endl;
}

void Proceccor::QuestGeneration(Queue * q2ue, int *HMquests, int* HMmiss)
{
	
	int random;
	random = rand() % 100;
	if ((random < Procent))
	{
		random = rand() % TimeToComplete + 1;	//����� ���������� �� 1 �� TimeToComplite ������	
		*(HMquests) += 1;
		Quest quest(random);
		q2ue->Put(&quest, HMmiss);
	}
}

#endif