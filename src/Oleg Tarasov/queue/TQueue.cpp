#include "stdafx.h"
#include "TQueue.h"

int TQueue::GetNextIndex(int index)
{
	return ++index % MemSize;
}

TData TQueue::Get() {
	TData temp = -1;
	if (pMem == 0) SetRetCode(DataNoMem);
	else if (IsEmpty()) SetRetCode(DataEmpty);
	else {
		temp = pMem[Li];
		Li = GetNextIndex(Li);
		DataCount--;
	}
	return temp;
}