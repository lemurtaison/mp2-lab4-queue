#ifndef _TQUEUE_H_
#define _TQUEUE_H_

#include "tdatstack.h"

class TQueue : public TStack {
protected:
	int Li;
	virtual int GetNextIndex(int index);

public:
	TQueue(int size = DefMemSize) : TStack(size) { Li = 0;  }
	virtual TData Get();
};

#endif