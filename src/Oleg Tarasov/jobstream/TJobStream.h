class TJobStream
{
private:
	int ID;
	int q1; // ����������� ����������� ������ ������� �� ����� (0 - 99)
	int q2; // ����������� ���������� �������� ������� (0 - 99)
public:
	TJobStream(int q1, int q2);
	int CreateJob();
	bool EndJob() const;
	int GetID() const;
};