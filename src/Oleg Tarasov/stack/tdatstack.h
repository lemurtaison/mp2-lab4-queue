#ifndef __T_DAT_STACK__
#define __T_DAT_STACK__

#include "tdataroot.h"

class TStack : public TDataRoot
{
private:
	int Hi;
	virtual int GetNextIndex(int index);

public:
	TStack(int Size = DefMemSize) : TDataRoot(Size), Hi(-1) { };
	virtual void Put(const TData &Val);
	virtual TData Pop();
	virtual TData Get();

	virtual int IsValid();
	virtual void Print();
	virtual int GetSize() { return DataCount; }
};

#endif