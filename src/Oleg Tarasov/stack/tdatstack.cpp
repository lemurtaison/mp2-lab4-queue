#include "stdafx.h"
#include <iostream>
#include "tdatstack.h"
using namespace std;

void TStack::Put(const TData &Val) {
	if (!IsValid()) SetRetCode(DataNoMem);
	else if (IsFull()) {
		SetMem(pMem, MemSize + 20);
	}
	else {
		Hi = GetNextIndex(Hi);
		pMem[Hi] = Val;
		DataCount++;
	}
}

TData TStack::Get() {
	if (!IsValid()) SetRetCode(DataNoMem);
	else if (IsEmpty()) SetRetCode(DataEmpty);
	else {
		return pMem[Hi];
	}
	return -1;
}

TData TStack::Pop() {
	if (!IsValid()) SetRetCode(DataNoMem);
	else if (IsEmpty()) SetRetCode(DataEmpty);
	else {
		DataCount--;
		return pMem[Hi--];
	}
	return -1;
}

int TStack::IsValid() {
	if (pMem == nullptr || MemSize < DataCount || DataCount < 0 || MemSize < 0) return 0;
	return 1;
}

void TStack::Print() {
	for (int i = 0; i < DataCount; i++)
		cout << pMem[i] << " ";
	cout << endl;
}

int TStack::GetNextIndex(int index)
{
	return ++index;
}