#include <iostream>
#include <locale>
#include "tproc.h"

using namespace std;

TProc::TProc(int _q1, int _q2, int _size, int _takts) : JobStream(_q1, _q2), JobQueue(_size), takts(_takts)
{
	setlocale(LC_ALL, "rus");
	unlocked = true;
	CountJobs = 0;
	CountRefusing = 0;
	CountDowntime = 0;
}

void TProc::Service(void)
{
	int Id;
	//���������� ������� � ��������� � ����� �������
	if (Id = JobStream.CreateJob())
	{
		JobQueue.Put(Id);
		//����� �� ������������
		if (JobQueue.GetRetCode() == DataFull)
			CountRefusing++;
	}

	//CPU ��������?
	if (unlocked)
	{
		InProc = JobQueue.Get();
		//������� �����
		if (JobQueue.GetRetCode() == DataEmpty)
			CountDowntime++;
		else
		{
			CountJobs++;
			unlocked = false;
		}
	}
	//����������� ������� � CPU?
	if (!unlocked)
		unlocked = JobStream.EndJob();
}


void TProc::PrintResult() const
{
	cout << endl;
	cout << "���������� ����������� � �� ������� � ������� ����� �������� ��������: " << JobStream.GetID() << endl;
	cout << "���������� ������� ������������ �����������: " << CountJobs << endl;
	cout << "���������� ������� �����������, �� �� ������������ �����������: " << JobStream.GetID() - CountJobs << endl;
	cout << "���������� ������� � ������������ ������� ��-�� ������������ �������: " << CountRefusing << endl;
	cout << "������� �������: " << ((JobStream.GetID()) ? ((float)CountRefusing * 100.0 / (float)JobStream.GetID()) : 0) << "%" << endl;
	cout << "C������ ���������� ������ ���������� �������: " << ((CountJobs) ? ((takts - CountDowntime) / CountJobs) : 0) << endl;
	cout << "���������� ������ �������: " << CountDowntime << endl;
	cout << "������� �������: " << ((takts) ? floor(CountDowntime * 100 / takts) : 0) << "%" << endl;
}