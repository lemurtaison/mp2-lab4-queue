#include "TQueue.h"
#include "TJobStream.h"

class TProc
{
private:
	TJobStream JobStream;
	TQueue JobQueue;
	bool unlocked;
	int takts;
	int CountJobs, CountRefusing, CountDowntime;
	int InProc;
public:
	TProc(int _q1, int _q2, int _size, int _takts);
	void Service();
	void PrintResult() const;
};
