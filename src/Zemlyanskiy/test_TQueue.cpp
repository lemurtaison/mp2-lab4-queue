#include "C:\mp2-lab4-queue\src\TQueue\TQueue.h"
#include "C:\mp2-lab4-queue\gtest\gtest.h"
#include <iostream>

TEST(TQueue, created_queue_is_empty)
{
	TQueue quk;

	EXPECT_TRUE(quk.IsEmpty());
}

TEST(TQueue, created_queue_is_not_nullptr)
{
	TQueue quk;

	EXPECT_NE(&quk, nullptr);
}

TEST(TQueue, queue_from_which_was_got_an_element_is_not_full)
{
	TQueue quk;
	for (int i = 0; i < DefMemSize; ++i)
		quk.Put(0);
	quk.Get();

	EXPECT_FALSE(quk.IsFull());
}

TEST(TQueue, queue_in_which_was_inserted_an_element_is_not_empty)
{
	TQueue quk;
	quk.Put(0);

	EXPECT_FALSE(quk.IsEmpty());
}

TEST(TQueue, can_put_in_queue_with_DefMemSize_size)
{
	TQueue quk;
	for (int i = 0; i < DefMemSize; ++i)
		quk.Put(0);

	EXPECT_NO_THROW(quk.Put(0));
}

TEST(TQueue, can_copy_queue)
{
	TQueue quk1;
	for (int i = 0; i < DefMemSize; ++i)
		quk1.Put(0);

	EXPECT_NO_THROW(TQueue quk2(quk1));
}

TEST(TQueue, copied_queue_is_equil_to_source_one)
{
	TQueue quk1;
	for (int i = 0; i < DefMemSize; ++i)
		quk1.Put(i);
	TQueue quk2(quk1);
	bool result = true;
	for (int i = 0; i < DefMemSize; ++i)
		if (quk1.Get() != quk2.Get())
		{
			result = false;
			break;
		}

	EXPECT_TRUE(result);
}

TEST(TQueue, copied_queue_has_its_own_memory)
{
	TQueue quk1;
	for (int i = 0; i < DefMemSize; ++i)
		quk1.Put(0);
	TQueue quk2(quk1);

	EXPECT_NE(&quk1, &quk2);
}