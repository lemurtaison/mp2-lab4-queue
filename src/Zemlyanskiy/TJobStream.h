#ifndef __TJOBSTREAM_H__
#define __TJOBSTREAM_H__
#include <ctime>
#include <cstdlib>

using namespace std;

class TJobStream
{
private:
	int ID;
	int q1;//вероятность поступления нового задания
	int q2;//вероятность выполнения нового задания
public:
	TJobStream(int _q1, int _q2) :ID(0), q1(_q1), q2(_q2)
	{
		srand(time(nullptr));
	}

	int CreateJob()
	{
		int res = 0;
		if (rand() % 100 < q1)
			res = ++ID;
		return res;
	}

	bool EndJob() 
	{
		bool res = false;
		if (rand() % 100 < q2)
			res = true;
		return res;
	}

	int GetID() 
	{
		return ID;
	}
};
#endif
