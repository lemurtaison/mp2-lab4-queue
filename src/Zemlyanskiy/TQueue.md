# Лабораторная работа №4. Очереди
## Введение
Лабораторная работа направлена на практическое освоение динамической структуры данных Очередь. С этой целью в лабораторной работе изучаются различные варианты структуры хранения очереди и разрабатываются методы и программы решения задач с использованием очередей. В качестве области приложений выбрана тема эффективной организации выполнения потока заданий на вычислительных системах.
### Определение
**Очередь** (англ. queue), – схема запоминания информации, при которой каждый вновь поступающий ее элемент занимает крайнее положение (конец очереди). При выдаче информации из очереди выдается элемент, расположенный в очереди первым (начало очереди), а оставшиеся элементы продвигаются к началу; следовательно, элемент, поступивший первым, выдается первым.
### Требования к лабораторной работе
Для вычислительной системы (ВС) с одним процессором и однопрограммным последовательным режимом выполнения поступающих заданий требуется разработать программную систему для имитации процесса обслуживания заданий в ВС. При построении модели функционирования ВС должны учитываться следующие основные моменты обслуживания заданий:
* генерация нового задания;
* постановка задания в очередь для ожидания момента освобождения процессора;
* выборка задания из очереди при освобождении процессора после обслуживания очередного задания.
По результатам проводимых вычислительных экспериментов система имитации должна выводить информацию об условиях проведения эксперимента (интенсивность потока заданий, размер очереди заданий, производительность процессора, число тактов имитации) и полученные в результате имитации показатели функционирования вычислительной системы, в т.ч.
* количество поступивших в ВС заданий;
* количество отказов в обслуживании заданий из-за переполнения очереди;
* среднее количество тактов выполнения заданий;
* количество тактов простоя процессора из-за отсутствия в очереди заданий для обслуживания.
Показатели функционирования вычислительной системы, получаемые при помощи систем имитации, могут использоваться для оценки эффективности применения ВС; по результатам анализа показателей могут быть приняты рекомендации о целесообразной модернизации характеристик ВС (например, при длительных простоях процессора и при отсутствии отказов от обслуживания заданий желательно повышение интенсивности потока обслуживаемых заданий и т.д.).
#### Дополнительное задание
В качестве дополнительного задания предлагается создать имитацию двухпроцессорной ВС на одной очереди. 
#### Реализация очереди
```c++
#ifndef __TQUEUE_H__
#define __TQUEUE_H__

#include<iostream>
#include "C:\mp2-lab4-queue\include\tdataroot.h"

using namespace std;

class TQueue :public TDataRoot
{
private:
	int Hi, Li;
public:
	TQueue(int Size = DefMemSize) : TDataRoot(Size), Li(0), Hi(-1) {}
	void Put(const TData &Val)
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsFull())
			SetRetCode(DataFull);
		else
		{
			if (Hi == MemSize - 1)	
				Hi = 0;
			else	
				Hi++;
			pMem[Hi] = Val;
			DataCount++;
		}
	}
	TData Get(void)
	{
		TData result = -1;
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsEmpty())
			SetRetCode(DataEmpty);
		else
		{
			if (Li == MemSize - 1)  
				Li = 0;
			else	Li++;

			result = pMem[Li];
			DataCount--;
		}
		return result;
	}
	void Print()
	{
		if (IsEmpty())
			cout << "Queue is empty";
		else if (Hi > Li)
			for (int i = Li; i <= Hi; i++)
				cout << pMem[i] << ' ';
		else {
			for (int i = Li; i < MemSize - 1; i++)
				cout << pMem[i] << ' ';
			for (int i = 0; i <= Hi; i++)
				cout << pMem[i] << ' ';
		}
		cout << endl;
	}

	int IsValid()
	{
		return 1;
	}
};
#endif 
```
#### Тестовое приложение для проверки работоспособности очереди
```c++
#include <iostream>
#include "C:\mp2-lab4-queue\src\TQueue\TQueue.h"

using namespace std;

void main()
{
	TQueue qu;
	int temp;
	setlocale(LC_ALL, "Russian");
	cout << "Тестирование программ поддержки структуры типа очереди"
	<< endl;
	for (int i = 0; i < 25; i++)
	{
		qu.Put(i);
		cout << "Положили значение " << i << " Код " << qu.GetRetCode()
		<< endl;
	}
	while (!qu.IsEmpty())
	{
		temp = qu.Get();
		cout << "Взяли значение " << temp << " Код " << qu.GetRetCode() 
		<< endl;
	}
}
```
![](https://pp.userapi.com/c639118/v639118362/12048/bvKLF1uXaC8.jpg)
#### Код тестов для очереди
``` c++
#include "C:\mp2-lab4-queue\src\TQueue\TQueue.h"
#include "C:\mp2-lab4-queue\gtest\gtest.h"
#include <iostream>
TEST(TQueue, created_queue_is_empty)
{
	TQueue quk;

	EXPECT_TRUE(quk.IsEmpty());
}

TEST(TQueue, created_queue_is_not_nullptr)
{
	TQueue quk;

	EXPECT_NE(&quk, nullptr);
}

TEST(TQueue, queue_from_which_was_got_an_element_is_not_full)
{
	TQueue quk;
	for (int i = 0; i < DefMemSize; ++i)
		quk.Put(0);
	quk.Get();

	EXPECT_FALSE(quk.IsFull());
}

TEST(TQueue, queue_in_which_was_inserted_an_element_is_not_empty)
{
	TQueue quk;
	quk.Put(0);

	EXPECT_FALSE(quk.IsEmpty());
}

TEST(TQueue, can_put_in_queue_with_DefMemSize_size)
{
	TQueue quk;
	for (int i = 0; i < DefMemSize; ++i)
		quk.Put(0);

	EXPECT_NO_THROW(quk.Put(0));
}

TEST(TQueue, can_copy_queue)
{
	TQueue quk1;
	for (int i = 0; i < DefMemSize; ++i)
		quk1.Put(0);

	EXPECT_NO_THROW(TQueue quk2(quk1));
}

TEST(TQueue, copied_queue_is_equil_to_source_one)
{
	TQueue quk1;
	for (int i = 0; i < DefMemSize; ++i)
		quk1.Put(i);
	TQueue quk2(quk1);
	bool result = true;
	for (int i = 0; i < DefMemSize; ++i)
		if (quk1.Get() != quk2.Get())
		{
			result = false;
			break;
		}

	EXPECT_TRUE(result);
}

TEST(TQueue, copied_queue_has_its_own_memory)
{
	TQueue quk1;
	for (int i = 0; i < DefMemSize; ++i)
		quk1.Put(0);
	TQueue quk2(quk1);

	EXPECT_NE(&quk1, &quk2);
}
```
### Успешное прохождение тестов
![](https://pp.userapi.com/c639118/v639118362/12050/r5eBEjwssrQ.jpg)
### Моделирование
Для моделирования момента появления нового задания можно использовать значение датчика случайных чисел. Если значение датчика меньше некоторого порогового значения q1, 0<=q1<=1, то считается, что на данном такте имитации в вычислительную систему поступает новое задание (тем самым параметр q1 можно интерпретировать как величину, регулирующую интенсивность потока заданий – новое задание генерируется в среднем один раз за (1/q1) тактов).
Моделирование момента завершения обработки очередного задания также может быть выполнено по описанной выше схеме. При помощи датчика случайных чисел формируется еще одно случайное значение, и если это значение меньше порогового значения q2, 0<=q2<=1,то принимается, что на данном такте имитации процессор завершил обслуживание очередного задания и готов приступить к обработке задания из очереди ожидания (тем самым параметр q2 можно интерпретировать как величину, характеризующую производительность процессора вычислительной системы – каждое задание обслуживается в среднем за (1/q2) тактов ).
Возможная простая схема имитации процесса поступления и обслуживания заданий в вычислительной системе состоит в следующем.
* Каждое задание в системе представляется некоторым однозначным идентификатором (например, порядковым номером задания).
* Для проведения расчетов фиксируется (или указывается в диалоге) число тактов работы системы.
* На каждом такте опрашивается состояние потока задач и процессор.
* Регистрация нового задания в вычислительной системе может быть сведена к запоминанию идентификатора задания в очередь ожидания процессора. Ситуацию переполнения очереди заданий следует понимать как нехватку ресурсов вычислительной системы для ввода нового задания (отказ от обслуживания).
* Для моделирования процесса обработки заданий следует учитывать, что процессор может быть занят обслуживанием очередного задания, либо же может находиться в состоянии ожидания (простоя).
* В случае освобождения процессора предпринимается попытка его загрузки. Для этого извлекается задание из очереди ожидания.
* Простой процессора возникает в случае, когда при завершении обработки очередного задания очередь ожидающих заданий оказывается пустой.
* После проведения всех тактов имитации производится вывод характеристик вычислительной системы.
#### Реализация потока задач
```c++
#ifndef __TJOBSTREAM_H__
#define __TJOBSTREAM_H__
#include <ctime>
#include <cstdlib>

using namespace std;

class TJobStream
{
private:
	int ID;
	int q1;//вероятность поступления нового задания
	int q2;//вероятность выполнения нового задания
public:
	TJobStream(int _q1, int _q2) :ID(0), q1(_q1), q2(_q2)
	{
		srand(time(nullptr));
	}
	int CreateJob()
	{
		int res = 0;
		if (rand() % 100 < q1)
			res = ++ID;
		return res;
	}
	bool EndJob() 
	{
		bool res = false;
		if (rand() % 100 < q2)
			res = true;
		return res;
	}
	int GetID() 
	{
		return ID;
	}
};
#endif
```
#### Реализация имитации однопроцессорной ВС
```c++
#ifndef __OldTProc_H__
#define __OldTProc_H__
#include <iostream>
#include <locale>
#include "C:\mp2-lab4-queue\src\TQueue\TQueue.h"
#include "C:\mp2-lab4-queue\src\TJobStream\TJobStream.h"

using namespace std;

class OldTProc
{
private:
	TJobStream JobStream;
	TQueue Queue;
	bool unlocked_first_proc;
	int takts;
	int TaskJobs, TaskRefusing,TaskDownTime_1;
	int FirstProc, SecondProc;
public:
	OldTProc(int _q1, int _q2, int _size,
		int _takts) :JobStream(_q1, _q2), Queue(_size),
		takts(_takts)
	{
		setlocale(LC_ALL, "Russian");
		unlocked_first_proc = true;
		TaskJobs = 0;
		TaskRefusing = 0;
		TaskDownTime_1 = 0;
	}
	void Service()
	{
		int Task;
		if (Task = JobStream.CreateJob())
		{
			Queue.Put(Task);
			//Если очередь полна, то скипаем задачу 
			if (Queue.GetRetCode() == DataFull)
			{
				TaskRefusing++;
			}
		}
		if (unlocked_first_proc)
		{
			FirstProc = Queue.Get();
			//Если очередь пуста, то второй процессор простаивает; 
			if (Queue.GetRetCode() == DataEmpty)
				TaskDownTime_1++;
			else
			{
				TaskJobs++;
				unlocked_first_proc = false;
			}
		}
		if (!unlocked_first_proc)
			unlocked_first_proc = JobStream.EndJob();
	}
	void PrintInfo()
	{
		for (int i = 0; i<takts; i++)
		{
			Service();
		}
		cout << endl;
		cout << "Количество поступивших в ВС заданий 
		        в течение всего процесса имитации: " 
		     << JobStream.GetID() << endl;
		cout << "Количество заданий обработанных процессором: " 
		     << TaskJobs << endl;
		cout << "Количество заданий поступивших, 
		         но не обработанных процессором: " 
		     << JobStream.GetID() - TaskJobs << endl;
		cout << "Количество отказов в обслуживании заданий 
		         из-за переполнения очереди: " 
		         << TaskRefusing << endl;
		cout << "Количество тактов простоя процессора: " 
		     << TaskDownTime_1 << endl;
		cout << "Процент простоя первого процессора: " 
		     << ((takts) ? floor(TaskDownTime_1 * 100 / takts) : 0) 
		     << "%" << endl;
	}
};
#endif
```

```c++
#include <iostream> 
#include "C:\mp2-lab4-queue\src\TQueue\TQueue.h" 
#include "OldTProc.h" 

using namespace std;

int main()
{
	OldTProc proc(20, 20, 30, 1000000);
	proc.PrintInfo();
	system("pause");
	return 0;
}
```
#### Успешная имитация однопроцессорной ВС
![](https://pp.userapi.com/c639118/v639118362/12060/tdnt-d18uHw.jpg)
#### Реализация имитации двухпроцессорной ВС
```c++
#ifndef __TProc_H__
#define __TProc_H__
#include <iostream>
#include <locale>
#include "C:\mp2-lab4-queue\src\TQueue\TQueue.h"
#include "C:\mp2-lab4-queue\src\TJobStream\TJobStream.h"

using namespace std;

class TProc
{
private:
	TJobStream JobStream;
	TQueue Queue;
	bool unlocked_first_proc;
	bool unlocked_second_proc;
	int takts;
	int TaskJobs, TaskRefusing, TaskDownTime_1, TaskDownTime_2;
	int FirstProc, SecondProc;
public:
	TProc(int _q1, int _q2, int _size,
		int _takts) :JobStream(_q1, _q2), Queue(_size),
		takts(_takts)
	{
		setlocale(LC_ALL, "Russian");
		unlocked_first_proc = true;
		unlocked_second_proc = true;
		TaskJobs = 0;
		TaskRefusing = 0;
		TaskDownTime_1 = 0;
		TaskDownTime_2 = 0;
	}
	void Service()
	{
		int Task;
		if (Task = JobStream.CreateJob())
		{
			Queue.Put(Task);
			//Если очередь полна, то скипаем задачу 
			if (Queue.GetRetCode() == DataFull)
			{
				TaskRefusing++;
			}
		}
		if (unlocked_first_proc)
		{
			FirstProc = Queue.Get();
			//Если очередь пуста, то второй процессор простаивает; 
			if (Queue.GetRetCode() == DataEmpty)
				TaskDownTime_1++;
			else
			{
				TaskJobs++;
				unlocked_first_proc = false;
			}
		}
		if (!unlocked_first_proc)
		{
			SecondProc = Queue.Get();
			//Если очередь пуста, то второй процессор простаивает; 
			if (Queue.GetRetCode() == DataEmpty)
				TaskDownTime_2++;
			else
			{
				TaskJobs++;
				unlocked_second_proc = false;
			}
		}
		if (!(unlocked_second_proc&&unlocked_second_proc))
		{
			unlocked_first_proc = JobStream.EndJob();
			unlocked_second_proc = JobStream.EndJob();
		}
	}

	void PrintInfo()
	{
		for (int i = 0; i<takts; i++)
		{
			Service();
		}
		cout << endl;
		cout << "Количество поступивших в ВС заданий в течение всего 
		         процесса имитации: "
		     << JobStream.GetID() << endl;
		cout << "Количество заданий обработанных процессорами: " << TaskJobs << endl;
		cout << "Количество заданий поступивших, 
		         но не обработанных процессорами: " 
		     << JobStream.GetID() - TaskJobs << endl;
		cout << "Количество отказов в обслуживании заданий 
		         из-за переполнения очереди: " 
		     << TaskRefusing << endl;
		cout << "Количество тактов простоя первого процессора: " 
		     << TaskDownTime_1 << endl;
		cout << "Количество тактов простоя второго процессора: " 
		     << TaskDownTime_2 << endl;
		cout << "Процент простоя первого процессора: " 
		     << ((takts) ? floor(TaskDownTime_1 * 100 / takts) : 0) 
		     << "%" << endl;
		cout << "Процент простоя второго процессора: " 
		     << ((takts) ? floor(TaskDownTime_2 * 100 / takts) : 0) 
		     << "%" << endl;
	}
};
#endif
```
#### Успешная имитация двухпроцессорной ВС
![](https://pp.userapi.com/c639118/v639118362/12071/zX7ZwV0ANZY.jpg)
### Выводы
**Выполнив данную работу мы научились:**
- Реализации очереди
- Имитации вычислительной системы



