#include "gtest.h"
#include "TQueue.h"

TEST(TQueue, created_queue_is_empty)
{
	TQueue q;
	EXPECT_TRUE(q.IsEmpty());
}

TEST(TQueue, cant_create_queue_with_negative_size)
{	
	ASSERT_ANY_THROW(TQueue q(-1));
}

TEST(TQueue, can_put_in_queue)
{
	TQueue q;	
	ASSERT_NO_THROW(q.Put(5));
}

TEST(TQueue, can_put_in_full_queue)
{
	TQueue q;
	for (int i = 0; !q.IsFull(); ++i)	
		q.Put(i);
	int size1 = q.GetEmptyCell();
	q.Put(1);
	int size2 = q.GetEmptyCell();
	EXPECT_NE(size1, size2);
}

TEST(TQueue, can_get_from_queue)
{
	TQueue q;
	q.Put(5);
	EXPECT_EQ(5, q.Get());
}

TEST(TQueue, cant_get_from_empty_queue)
{
	TQueue q;	
	ASSERT_ANY_THROW(q.Get());
}

TEST(TQueue, queue_after_put_isnt_empty)
{
	TQueue q;
	q.Put(1);
	EXPECT_FALSE(q.IsEmpty());
}

TEST(TQueue, queue_after_all_gets_is_empty)
{
	TQueue q;
	q.Put(1);
	q.Get();
	EXPECT_TRUE(q.IsEmpty());
}

TEST(TQueue, queue_after_all_puts_is_full)
{
	TQueue q;
	while (!q.IsFull())
		q.Put(1);
	EXPECT_TRUE(q.IsFull());
}

TEST(TQueue, ring_buffer_working)
{
	TQueue q;
	while (!q.IsFull())
		q.Put(1);
	q.Get();
	q.Put(1);
	EXPECT_EQ(0, q.GetCurIndex());	
}
