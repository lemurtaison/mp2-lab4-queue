#include "TStack.h"
#include <iostream>
using namespace std;

TStack::TStack(const TStack& CopyStack)
{
	MemSize = CopyStack.MemSize;
	DataCount = CopyStack.DataCount;	
	pMem = new TElem[MemSize];
	for (int i = 0; i < MemSize; ++i)
	{
		pMem[i] = CopyStack.pMem[i];
	}
}

void TStack::Put(const TData& Val)
{
	if (pMem == nullptr) 
	{ 
		throw SetRetCode(DataNoMem); 
	}
	else if (IsFull())
	{
		/* void*p = nullptr;
		SetMem(p, MemSize + DefMemSize);
		Hi = GetNextIndex(Hi);
		pMem[Hi] = Val;
		DataCount++;*/		
		throw SetRetCode(DataFull);
	}
	else
	{
		Hi = GetNextIndex(Hi);
		pMem[Hi] = Val;
		DataCount++;
	}
}

TData TStack::Get(void)
{
	if (pMem == nullptr)	
		throw SetRetCode(DataNoMem);	
	else if (IsEmpty())	
		throw SetRetCode(DataEmpty);	
	else
	{
		DataCount--;
		return pMem[Hi--];
	}
}

void TStack::Print()
{
	if (DataCount == 0) { cout << "Stack is empty!"; }
	for (int i = 0; i < DataCount; ++i)
	{
		cout << pMem[i] << " ";
	}
	cout << endl;
}