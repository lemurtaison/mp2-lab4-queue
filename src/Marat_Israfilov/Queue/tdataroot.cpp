#include "tdataroot.h"

TDataRoot::TDataRoot(int Size) : TDataCom()
{
	if (Size < 0)
		throw SetRetCode(DataNoMem);
	else
	{
		MemSize = Size;
		DataCount = 0;
		if (Size == 0)
		{
			pMem = nullptr;
			MemType = MEM_RENTER;
		}
		else
		{
			pMem = new TElem[MemSize];
			MemType = MEM_HOLDER;
		}
	}
}

TDataRoot::~TDataRoot()
{
	if (MemType == MEM_HOLDER)
		delete[] pMem;
	pMem = nullptr;
}

void TDataRoot::SetMem(void *p, int Size)
{
	if (Size < 0)
		throw SetRetCode(DataNoMem);
	else if (MemType == MEM_HOLDER)
	{
		MemSize = Size;
		PTElem tmp = pMem;
		pMem = new TElem[MemSize];
		for (int i = 0; i < DataCount; ++i)
		{
			pMem[i] = tmp[i];
		}
		delete[] tmp;
	}
	else
	{
		MemSize = Size;
		for (int i = 0; i < DataCount; ++i)
		{
			*((PTElem)p + i) = pMem[i];
		}
		pMem = (PTElem)p;
	}
}

bool TDataRoot::IsEmpty(void) const
{
	return DataCount == 0;
}

bool TDataRoot::IsFull(void) const
{
	return DataCount == MemSize;
}