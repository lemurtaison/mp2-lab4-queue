#ifndef __TQUEUE_H__
#define __TQUEUE_H__

#include "TStack.h"

class TQueue : public TStack
{
protected:
	int Li;
	virtual int GetNextIndex(int);
public:
	TQueue(int Size = DefMemSize) : TStack(Size) { Li = 0; }
	virtual TData Get() override;
	virtual int GetEmptyCell() { return MemSize - DataCount; }
	virtual void Print();
	int GetCurIndex() { return Hi; }
};
#endif
