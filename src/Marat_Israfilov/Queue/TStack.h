#ifndef __TSTACK_H__
#define __TSTACK_H__

#include "tdataroot.h"

class TStack : public TDataRoot
{
protected:
	int Hi;
public:
	TStack(int Size = DefMemSize) : TDataRoot(Size) { Hi = -1; }
	TStack(const TStack&);
	virtual int GetNextIndex(int index) { return ++index; }
	virtual void  Put(const TData&);
	virtual TData Get(void);
	virtual TData Current_Element() const { return pMem[DataCount - 1]; }	
	virtual void Print();
};
#endif
