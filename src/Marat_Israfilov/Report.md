# Методы программирования 2: Очереди

 
## 1. Введение

 

 Лабораторная работа направлена на практическое освоение динамической структуры данных **Очередь**. С этой целью в лабораторной работе изучаются различные варианты структуры хранения очереди и разрабатываются методы и программы решения задач с использованием очередей. В качестве области приложений выбрана тема эффективной организации выполнения потока заданий на вычислительных системах.

 

 Очередь характеризуется таким порядком обработки значений, при котором вставка новых элементов производится в конец очереди, а извлечение – из начала. Подобная организация данных широко встречается в различных приложениях. В качестве примера использования очереди предлагается задача разработки системы имитации однопроцессорной ЭВМ. Рассматриваемая в рамках лабораторной работы схема имитации является одной из наиболее простых моделей обслуживания заданий в вычислительной системе и обеспечивает тем самым лишь начальное ознакомление с проблемами моделирования и анализа эффективности функционирования реальных вычислительных систем.

 

 **Очередь (англ. queue)**, – схема запоминания информации, при которой каждый вновь поступающий ее элемент занимает крайнее положение (*конец очереди*). При выдаче информации из очереди выдается элемент, расположенный в очереди первым (*начало очереди*), а оставшиеся элементы продвигаются к началу; следовательно, элемент, поступивший первым, выдается первым. 

 
## 2. Цели работы

 

  1. Реализовать класс `TQueue` на основе имеующегося класса `TStack`.

 2. Реализовать тесты для проверки работоспособности класса `TQueue`

 3. Спроектировать модель вычислительной системы с двумя потоками и одним процессором на основе классов `TJobStream` `TProc`.
 

## 3. Описание структуры данных Очередь

 

 Напомним, что динамическая структура есть математическая структура, которой соответствует частично-упорядоченное (по включению) базовое множество **М**, операции вставки и удаления элементы которого являются структурами данных. При этом отношения включения индуцируются операциями преобразования структуры данных.

 

 Таким образом, очередь есть динамическая структура, операции вставки и удаления переводят очередь из одного состояния в другое, при этом добавление новых элементов осуществляется в конец очереди, а извлечение – из начала очереди (дисциплина обслуживания «первым пришел – первым обслужен.

 

 Важной задачей при реализации системы обслуживания очереди является выбор структуры хранения, обеспечивающей решение проблемы эффективного использования памяти без перепаковок и без использования связных списков (требующих дополнительных затрат памяти на указатели).

 

 Как и в случае со стеком, в качестве структуры хранения очереди предлагается использовать одномерный (одноиндексный) массив, размещаемый в динамической области памяти. В связи с характером обработки значений, располагаемых в очереди, для указания хранимых в очереди данных необходимо иметь два указателя – на начало и конец очереди. Эти указатели увеличивают свое значение: один при вставке, другой при извлечении элемента. Таким образом, в ходе функционирования очереди может возникнуть ситуация, когда оба указателя достигнут своего наибольшего значения и дальнейшее пополнение очереди станет невозможным, несмотря на наличие свободного пространства в очереди. Одним из решений проблемы «движения» очереди является организация на одномерном массиве кольцевого буфера. Кольцевым буфером называется структура хранения, получаемая из вектора расширением отношения следования парой **p(an,a1)**. 

 

 ![](/images/1.PNG)

 

 Структура хранения очереди в виде кольцевого буфера может быть определена как одномерный (одноиндексный) массив, размещаемый в динамической области памяти и расположение данных в котором определяется при помощи следующего набора параметров:

 

 - **pMem** – указатель на память, выделенную для кольцевого буфера,

 - **MemSize** – размер выделенной памяти,

 - **MaxMemSize** – размер памяти, выделяемый по умолчанию, если при создании кольцевого буфера явно не указано требуемое количество элементов памяти,

  

 - **DataCount** – количество запомненных в очереди значений,

 - **Hi** – индекс элемента массива, в котором хранится последний элемент очереди,

 - **Li** – индекс элемента массива, в котором хранится первый элемент очереди.

 

 ![](/images/2.PNG)

 

 В связи с тем, что структура хранения очереди во многом аналогична структуре хранения стек, предлагается класс для реализации очереди построить наследованием от класса стек, описанного в лабораторной работе №3. При наследовании достаточно переопределить методы **Get** и **GetNextIndex**. В методе **Get** изменяется индекс для получения элемента (извлечение значений происходит из начала очереди), метод **GetNextIndex** реализует отношение следования на кольцевом буфере.

 
## 4. Класс *TQueue*

 

 Класс `TQueue` наследуется от класса `TStack`.

 Методы, которые необходимо переопределить:

 - `GetNextIndex`

 - `Get`

 - `Print` 
 

### Объявление:
 
```c++
#ifndef __TQUEUE_H__
#define __TQUEUE_H__

#include "TStack.h"

class TQueue : public TStack
{
protected:
	int Li;
	virtual int GetNextIndex(int) override;
public:
	TQueue(int Size = DefMemSize) : TStack(Size) { Li = 0; }
	virtual TData Get() override;
	virtual int GetEmptyCell() { return MemSize - DataCount; }
	virtual void Print() override;
	int GetCurIndex() { return Hi; }
};
#endif
```
 
### Реализация:
 
```c++
#include "TQueue.h"
#include <iostream>
using namespace std;

int TQueue::GetNextIndex(int index)
{
	return ++index % MemSize;
}

TData TQueue::Get()
{
	TData tmp;
	if (pMem == nullptr) 
		throw SetRetCode(DataNoMem);
	else if (IsEmpty()) 
		throw SetRetCode(DataEmpty);
	else
	{
		tmp = pMem[Li];
		Li = GetNextIndex(Li);
		DataCount--;
	}
	return tmp;
}

void TQueue::Print()
{
	for (int i = 0; i<DataCount; i++)
		cout << pMem[(i + Li) % MemSize] << ' ';
	cout << endl;
}
```

### Тесты:

```c++
TEST(TQueue, created_queue_is_empty)
{
	TQueue q;
	EXPECT_TRUE(q.IsEmpty());
}

TEST(TQueue, cant_create_queue_with_negative_size)
{	
	ASSERT_ANY_THROW(TQueue q(-1));
}

TEST(TQueue, can_put_in_queue)
{
	TQueue q;	
	ASSERT_NO_THROW(q.Put(5));
}

TEST(TQueue, can_put_in_full_queue)
{
	TQueue q;
	for (int i = 0; !q.IsFull(); ++i)	
		q.Put(i);
	int size1 = q.GetEmptyCell();
	q.Put(1);
	int size2 = q.GetEmptyCell();
	EXPECT_NE(size1, size2);
}

TEST(TQueue, can_get_from_queue)
{
	TQueue q;
	q.Put(5);
	EXPECT_EQ(5, q.Get());
}

TEST(TQueue, cant_get_from_empty_queue)
{
	TQueue q;	
	ASSERT_ANY_THROW(q.Get());
}

TEST(TQueue, queue_after_put_isnt_empty)
{
	TQueue q;
	q.Put(1);
	EXPECT_FALSE(q.IsEmpty());
}

TEST(TQueue, queue_after_all_gets_is_empty)
{
	TQueue q;
	q.Put(1);
	q.Get();
	EXPECT_TRUE(q.IsEmpty());
}

TEST(TQueue, queue_after_all_puts_is_full)
{
	TQueue q;
	while (!q.IsFull())
		q.Put(1);
	EXPECT_TRUE(q.IsFull());
}

TEST(TQueue, ring_buffer_working)
{
	TQueue q;
	while (!q.IsFull())
		q.Put(1);
	q.Get();
	q.Put(1);
	EXPECT_EQ(0, q.GetCurIndex());	
}
```

![](https://bytebucket.org/Marat_Israfilov/mp2-lab4-queue/raw/af03510a3076478a199aa30d38938e4c69dd27d5/src/Marat_Israfilov/images/Tests.png)
 
## 5. Разработка вычеслтельной системы
 
 
### 5.1 Условия и ограничения

 Сделаем следующие основные допущения:

 1 При планировании очередности обслуживания заданий возможность задания приоритетов не учитывается.

 2 Моменты появления новых заданий и моменты освобождения процессора рассматриваются как случайные события.
 

### 5.2 Класс *TJobStream*

 Этот класс реализует поток задач:
 - Создание задачи;
 - Заполнение очередей;
 - Извлечение задачи из очереди;
 
#### Объявление:
 
```c++
#ifndef __TJOBSTREAM_H__
#define	__TJOBSTREAM_H__

#include "TQueue.h"
#include <stdlib.h>
#include <time.h> 

#define Q11 0.4
#define Q12 0.4

class TJobStream
{
protected:
	TQueue queue1;
	TQueue queue2;
	double q11, q12;
	long TaskNumber;
	long RejectedTasks;
	long CompletedTasks;
	int NumQueue1;
	int NumQueue2;
public:
	TJobStream(double q1= Q11, double q2 = Q12, int size_q1 = DefMemSize, int size_q2 = DefMemSize) : queue1(size_q1), queue2(size_q2)
	{
		q11 = q1;
		q12 = q2;
		srand(time(NULL));
		TaskNumber = 1;
		RejectedTasks = 0;
		CompletedTasks = 0;
		NumQueue1 = 0;
		NumQueue2 = 0;
	}

	void QueuesFill();
	double CreateTask();
	bool CanTakeTask();
	void TakeTasks();
	long GetTaskNumber() { return TaskNumber; }
	long GetRejectedTasks() { return RejectedTasks; }
	long GetCompletedTasks() { return CompletedTasks; }
};
#endif
```
 
#### Реализация:
 
```c++
#include "TJobStream.h"

double TJobStream::CreateTask()
{
	double result = (double)(rand() % 101) / 100.0;
	if (result < Q11 && result < Q12)
	{		
			NumQueue1 = 1;		
			NumQueue2 = 1;
		return result;
	} 
	else if (result < Q11)
	{
		return result;
		NumQueue1 = 1;
	}
	else if (result < Q12)
	{
		return result;
		NumQueue2 = 1;
	}
	else
		return -1.0;
}

void TJobStream::QueuesFill()
{
	if (CreateTask() != -1.0)
	{
		if (NumQueue1 == 1)
		{
			if (!queue1.IsFull())
			{ 
				queue1.Put(TaskNumber);
				TaskNumber++;
			}
			else
			{
				RejectedTasks++;
				TaskNumber++;
			}
		}
		if (NumQueue2 == 1)
		{
			if (!queue2.IsFull())
			{
				queue2.Put(TaskNumber);
				TaskNumber++;
			}
			else
			{
				RejectedTasks++;
				TaskNumber++;
			}
		}
	}
}

bool TJobStream::CanTakeTask()
{
	return !(queue1.IsEmpty() && queue2.IsEmpty());
}

void TJobStream::TakeTasks()
{
	if (queue1.GetEmptyCell() <= queue2.GetEmptyCell())
	{
		if (!queue1.IsEmpty())
			queue1.Get();
		else
			queue2.Get();
		CompletedTasks++;
	}
	else if (queue2.GetEmptyCell() <= queue1.GetEmptyCell())
	{
		if (!queue2.IsEmpty())
			queue2.Get();
		else
			queue1.Get();
		CompletedTasks++;
	}
}
```
 
### 5.3 Класс *TProc*

 Этот класс реализует работу процессора:
 - Выполнение заданий;
 - Создание отчета; 

#### Объявление:
 
```c++
#ifndef __TPROC_H__
#define	__TPROC_H__

#include "TJobStream.h"


#define Q2 0.75
#define DefNumTacts 1000000

class TProc
{
protected:
	double q2;
	long TactsNumber;
	long IdleTacts;
	TJobStream stream;
public:
	TProc(long TacNum = DefNumTacts, double q = Q2) : stream(0.5, 0.5, 5, 5)
	{
		q2 = q;
		TactsNumber = TacNum;
		IdleTacts = 0;		
	}
	void DoWork();
	bool isIdle();
	void GetReport();
};
#endif
```
 
#### Реализация:
 
```c++
#include "TProc.h"
#include <iostream>
using namespace std;

bool TProc::isIdle()
{
	if ((double)(rand() % 101) / 100.0 < q2)
		return true;
	else
		return false;
}

void TProc::DoWork()
{
	for (long i = 0; i < TactsNumber; ++i)
	{
		stream.QueuesFill();
		if (isIdle())
		{
			if (stream.CanTakeTask())
				stream.TakeTasks();
			else
				IdleTacts++;
		}
	}
}

void TProc::GetReport()
{
	double tmp;
	setlocale(LC_ALL, "rus");
	cout << "Количество тактов работы процессора: " << TactsNumber << endl;
	cout << "Количество поступивших в вычислительную систему заданий: " << stream.GetTaskNumber() << endl;
	tmp = stream.GetRejectedTasks() * 100 / stream.GetTaskNumber();
	cout << "Количество отказов в обслуживании заданий: " << stream.GetRejectedTasks() << " (" << tmp << "%)" << endl;
	tmp = (TactsNumber - IdleTacts) / stream.GetCompletedTasks();
	cout << "Среднее количество тактов выполнения задания: " << tmp << endl;
	tmp = IdleTacts * 100 / TactsNumber;
	cout << "Количество тактов простоя процессора: " << IdleTacts << " (" << tmp << "%)" << endl;
}
```

## 6. Исследование вычислительной системы

 В данной работе предполагается что имеется две очереди, поэтому в первом исследовании изменяемыми характерстиками были размеры очередей:
 
 ![](https://bytebucket.org/Marat_Israfilov/mp2-lab4-queue/raw/af03510a3076478a199aa30d38938e4c69dd27d5/src/Marat_Israfilov/images/CS1.png)
 
 На данном изображении видно, что малые размеры очередей совсем не эффективны!
 Если суммарный размер обеих очередей 30, то при равномерном распределении очередей(т.е. 15 и 15), получается более эффективная работа, чем при неравномерном распределении. 
 Наиболее эффективна работа вычислительной системы при больших размерах очередей и их равномерном распределении.
 
 Во втором исследовании изменяемые характеристики были интенсивности появления нового задания:
 
 ![](https://bytebucket.org/Marat_Israfilov/mp2-lab4-queue/raw/af03510a3076478a199aa30d38938e4c69dd27d5/src/Marat_Israfilov/images/CS2.png)
 
Предпологается что две очереди не связаны между собой и возникновене заданий может быть сразу у двух очередей одновременно(имитация многопоточности), поэтому в случае если задания будут появлятся на каждом такте в обеих очередях, процессор просто физически не сможет их обработать, т.к. количество задач будет превышать количество тактов в два раза.

В третьем исследовании изменяемая характеристика была производительность процессора:

![](https://bytebucket.org/Marat_Israfilov/mp2-lab4-queue/raw/af03510a3076478a199aa30d38938e4c69dd27d5/src/Marat_Israfilov/images/CS3.png)

При определенной частоте появления заданий в очередях видно, что при малой производительности процесса, задания выполняются дольше, следовательно количество пропущеных заданий увеличивается. При относительно высокой производительности количество пропущеных заданий значительно снижается, но появляются такты простоя, когда процессор бездействует.

## 7. Вывод

 В данной работе был разработан классы:
 - __TQueue__ - структура данных "Очередь" наследуемая от класса `TStack`;
 - __TJobStream__ - класс реализующий поток задач;
 - __TProc__ - класс реализующий процессор;
 
 При проведени исследований работы вычислительной системы, были сделаны выводы о том, что чем больше размеры очередей и их распределение, тем лучше. Так же на основе этих же исследований можно сделать вывод, что большая интенсивность появления новых задач может привести к неизбежным отказам, поэтому лучше выбрать умеренную интенсивность на обоих потоках.
