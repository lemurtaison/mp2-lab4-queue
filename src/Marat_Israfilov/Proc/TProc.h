#ifndef __TPROC_H__
#define	__TPROC_H__

#include "TJobStream.h"


#define Q2 0.75
#define DefNumTacts 1000000

class TProc
{
protected:
	double q2;
	long TactsNumber;
	long IdleTacts;
	TJobStream stream;
public:
	TProc(long TacNum = DefNumTacts, double q = Q2) : stream(0.5, 0.5, 5, 5)
	{
		q2 = q;
		TactsNumber = TacNum;
		IdleTacts = 0;		
	}
	void DoWork();
	bool isIdle();
	void GetReport();
};
#endif

