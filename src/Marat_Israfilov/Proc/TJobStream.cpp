#include "TJobStream.h"

double TJobStream::CreateTask()
{
	double result = (double)(rand() % 101) / 100.0;
	if (result < Q11 && result < Q12)
	{		
			NumQueue1 = 1;		
			NumQueue2 = 1;
		return result;
	} 
	else if (result < Q11)
	{
		return result;
		NumQueue1 = 1;
	}
	else if (result < Q12)
	{
		return result;
		NumQueue2 = 1;
	}
	else
		return -1.0;
}

void TJobStream::QueuesFill()
{
	if (CreateTask() != -1.0)
	{
		if (NumQueue1 == 1)
		{
			if (!queue1.IsFull())
			{ 
				queue1.Put(TaskNumber);
				TaskNumber++;
			}
			else
			{
				RejectedTasks++;
				TaskNumber++;
			}
		}
		if (NumQueue2 == 1)
		{
			if (!queue2.IsFull())
			{
				queue2.Put(TaskNumber);
				TaskNumber++;
			}
			else
			{
				RejectedTasks++;
				TaskNumber++;
			}
		}
	}
}

bool TJobStream::CanTakeTask()
{
	return !(queue1.IsEmpty() && queue2.IsEmpty());
}

void TJobStream::TakeTasks()
{
	if (queue1.GetEmptyCell() <= queue2.GetEmptyCell())
	{
		if (!queue1.IsEmpty())
			queue1.Get();
		else
			queue2.Get();
		CompletedTasks++;
	}
	else if (queue2.GetEmptyCell() <= queue1.GetEmptyCell())
	{
		if (!queue2.IsEmpty())
			queue2.Get();
		else
			queue1.Get();
		CompletedTasks++;
	}
}
