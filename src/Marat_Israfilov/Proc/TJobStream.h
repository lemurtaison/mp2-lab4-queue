#ifndef __TJOBSTREAM_H__
#define	__TJOBSTREAM_H__

#include "TQueue.h"
#include <stdlib.h>
#include <time.h> 

#define Q11 0.4
#define Q12 0.4

class TJobStream
{
protected:
	TQueue queue1;
	TQueue queue2;
	double q11, q12;
	long TaskNumber;
	long RejectedTasks;
	long CompletedTasks;
	int NumQueue1 = 0;
	int NumQueue2 = 0;
public:
	TJobStream(double q1= Q11, double q2 = Q12, int size_q1 = DefMemSize, int size_q2 = DefMemSize) : queue1(25), queue2(25)
	{
		q11 = q1;
		q12 = q2;
		srand(time(NULL));
		TaskNumber = 1;
		RejectedTasks = 0;
		CompletedTasks = 0;
	}

	void QueuesFill();
	double CreateTask();
	bool CanTakeTask();
	void TakeTasks();
	long GetTaskNumber() { return TaskNumber; }
	long GetRejectedTasks() { return RejectedTasks; }
	long GetCompletedTasks() { return CompletedTasks; }
};
#endif
