#include "TProc.h"
#include <iostream>
using namespace std;

bool TProc::isIdle()
{
	if ((double)(rand() % 101) / 100.0 < q2)
		return true;
	else
		return false;
}

void TProc::DoWork()
{
	for (long i = 0; i < TactsNumber; ++i)
	{
		stream.QueuesFill();
		if (isIdle())
		{
			if (stream.CanTakeTask())
				stream.TakeTasks();
			else
				IdleTacts++;
		}
	}
}

void TProc::GetReport()
{
	double tmp;
	setlocale(LC_ALL, "rus");
	cout << "���������� ������ ������ ����������: " << TactsNumber << endl;
	cout << "���������� ����������� � �������������� ������� �������: " << stream.GetTaskNumber() << endl;
	tmp = stream.GetRejectedTasks() * 100 / stream.GetTaskNumber();
	cout << "���������� ������� � ������������ �������: " << stream.GetRejectedTasks() << " (" << tmp << "%)" << endl;
	tmp = (TactsNumber - IdleTacts) / stream.GetCompletedTasks();
	cout << "������� ���������� ������ ���������� �������: " << tmp << endl;
	tmp = IdleTacts * 100 / TactsNumber;
	cout << "���������� ������ ������� ����������: " << IdleTacts << " (" << tmp << "%)" << endl;
}
