#ifndef __TJOBSTREAM_CPP__
#define __TJOBSTREAM_CPP__

#include "TJobStream.h"

TJobStream::TJobStream(unsigned long _MaxTact)
{
	srand(time(NULL));
	Task = 0;
	SetMaxTact(_MaxTact);
}

void TJobStream::SetMaxTact(const unsigned long _MaxTact)
{
	if (0 <= _MaxTact)
		MaxTact = _MaxTact;
	else
		throw Error_MaxTact;
}
const unsigned long TJobStream::GetMaxTact(void)
{
	return MaxTact;
}
float TJobStream::GenQ() 
{ 
	return (float)(rand() % 100 + 1) / 100;
};


#endif