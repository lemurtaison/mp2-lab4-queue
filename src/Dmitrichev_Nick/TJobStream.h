#ifndef __TJOBSTREAM_H__
#define __TJOBSTREAM_H__

#include <cstdlib>
#include <ctime>

#define StandartMaxTact 100000
#define Error_MaxTact 1
class TJobStream
{
protected:
    float Q1;
	unsigned long int Task, MaxTact;
	float GenQ();
public:
	TJobStream(unsigned long _MaxTact=StandartMaxTact);
	void SetMaxTact(const unsigned long);
	const unsigned long GetMaxTact(void);
};
#endif
