#ifndef __TPROC_CPP__
#define __TPROC_CPP__

#include "TProc.h"

TProc::TProc(float _Q1, float _Q2, unsigned long _MaxTact) :TJobStream(_MaxTact)
{
	PauseTact = Declained = Complited = 0;
	Tact = 1;
	SetQ1(_Q1);
	SetQ2(_Q2);
}
TProc::TProc()
{
	PauseTact = Declained = Complited = 0;
	Tact = 1;
	SetQ1(StandartQ1);
	SetQ2(StandartQ2);
	SetMaxTact(StandartMaxTact);
}
void TProc::SetQ1(const float _Q1)
{
	if (0.0 <= _Q1 && _Q1 <= 1.0)
		Q1 = _Q1;
	else
		throw Error_Q1;
}
const float TProc::GetQ1(void)
{
	return Q1;
}

void TProc::SetQ2(const float &_Q2)
{
	if (0.0 <= _Q2 && _Q2 <= 1.0)
		Q2 = _Q2;
	else
		throw Error_Q2;
}
const float TProc::GetQ2(void)
{
	return Q2; 
}
void TProc::GenerateJobProc(const bool show = false)
{
	TQueue<int> CurrentTasks;
	float TempQ = 0.0;

	while (Tact < MaxTact)
	{
		TempQ = GenQ();
		if (TempQ < Q1)
		{
			if (!CurrentTasks.IsFull())
			{
				Task++;
				if (show)
					std::cout << "Tact: " << Tact << " - Task " << Task << " added in the queue." << std::endl;
				CurrentTasks.Put(Task);
			}
			else
				Declained++;
		}
		if (CurrentTasks.IsEmpty())
		{
			if (show)
				std::cout << "Tact: " << Tact << " - Idle time " << std::endl;
			PauseTact++;
		}
		Tact++;
		TempQ = GenQ();
		if ((TempQ < Q2) && (!CurrentTasks.IsEmpty()))
		{
			Complited = CurrentTasks.Get();
			if (show)
				std::cout << "Tact: " << Tact << " - Task " << Complited << " completed." << std::endl;
			Tact++;
		}
	}
}
void TProc::Info()
{
	std::cout << "Tacts:\t\t\t\t\t" << MaxTact << std::endl;
	std::cout << "Generation of a new task (Q1):\t\t" << Q1 << std::endl;
	std::cout << "Completing of a task(Q2):\t\t" << Q2 << std::endl;
	std::cout << "Generated tasks:\t\t\t" << Complited + Declained << std::endl;
	std::cout << "Completed tasks:\t\t\t" << Complited << "	(" << (int)(Complited * 100 / (Declained + Complited)) << "%)\n";
	std::cout << "Declained tasks:\t\t\t" << Declained << std::endl;
	std::cout << "Idle times:\t\t\t\t" << PauseTact << "	(" << (int)(PauseTact * 100 / MaxTact) << "%)\n";
	std::cout << "Average number of tacts for task:\t" << (int)(Tact - PauseTact) / Complited << std::endl;
}

#endif