#include "gtest.h"
#include "tqueue.h"



TEST(TQueue, created_queue_is_empty)
{
	TQueue<int> qu;

	EXPECT_TRUE(qu.IsEmpty());
}

TEST(TQueue, queue_isnt_empty_if_element_was_put)
{
	TQueue<int> qu;
	qu.Put(1);

	EXPECT_FALSE(qu.IsEmpty());
}

TEST(TQueue, can_put_element_in_full_queue)
{
	TQueue<int> qu(2);
	for (int i = 0;i < 2;++i)
		qu.Put(i);

	EXPECT_NO_THROW(qu.Put(1));
}

TEST(TQueue, cant_get_element_from_empty_queue)
{
	TQueue<int> qu;

	EXPECT_ANY_THROW(qu.Get());
}

TEST(TQueue, queue_with_one_element_become_empty_after_getting_element)
{
	TQueue<int> qu;
	qu.Put(1);
	qu.Get();

	EXPECT_TRUE(qu.IsEmpty());
}

TEST(TQueue, first_in_first_out)
{
	TQueue<int> qu;
	qu.Put(1);
	qu.Put(2);
	qu.Put(3);

	EXPECT_EQ(1,qu.Get());
}

TEST(TQueue, ring_buffer_works)
{
	TQueue<int> qu;
	int Vector[20];
	int i;
	bool _check = true;
	for (i = 0;i < 20;++i)
		Vector[i] = i+20;
	for (i = 0;i < 40;++i)
		qu.Put(i);
	for (i = 0;i < 20;++i)
		qu.Get();
	for (i = 0;i < 20;++i)
	{
		if (Vector[i] != qu.Get())
		{
			_check = false;
			break;
		}
	}

	EXPECT_TRUE(_check);
}