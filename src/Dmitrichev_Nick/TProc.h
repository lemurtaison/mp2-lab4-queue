#ifndef __TPROC_H__
#define __TPROC_H__

#define Error_Q1 1
#define StandartQ1 0.5
#define Error_Q2 2
#define StandartQ2 0.5


#include "tqueue.h"
#include "TJobStream.h"

class TProc:public TJobStream
{
protected:
	float Q1,Q2;
	unsigned long Tact, PauseTact, Declained, Complited;
public:
	TProc(float, float, unsigned long);
	TProc();
	void SetQ1(const float);
	const float GetQ1(void);
	void SetQ2(const float&);
	const float GetQ2(void);
	void GenerateJobProc(const bool);
	void Info(void);
};
#endif
