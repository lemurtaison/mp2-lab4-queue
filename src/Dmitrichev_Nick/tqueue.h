#ifndef __TQUEUE_H__
#define __TQUEUE_H__

#include "tstack.h"

template<class ValType>
class TQueue :public TStack<ValType>
{
protected:
	int Li;//the beginning
	virtual int GetNextIndex(int index)
	{
		return ++index%MemSize;
	}
	virtual void refresh()
	{
		ValType* _pMem;
		_pMem = pMem;
		pMem = new ValType[MemSize];
		for (int i = 0;i < DataCount;++i)
			pMem[i] = _pMem[(Li + i)%MemSize];
		delete[]_pMem;
		Li = 0;Hi = MemSize;
	}
public:
	TQueue(int Size = DefMemSize) :TStack(Size),Li(0) {};
	//"Put()" in the tstack
	//must change "Get()" without changing "Put()"
	//"IsEmpty()" and "IsFull()" in the tdataroot
	ValType Get()
	{
		ValType temp;
		if (pMem == 0) SetRetCode(DataNoMem);
		else if (IsEmpty()) SetRetCode(DataEmpty);
		else {
			temp = pMem[Li];
			Li = GetNextIndex(Li);
			DataCount--;
		}
	if (SetRetCode(GetRetCode()) != DataOK)
		throw GetRetCode();
	else
		return temp;	
	}
	void Print()
	{
		for(int i=0;i<DataCount;i++)
			std::cout<< pMem[(i+Li)%MemSize] << ' ';
		printf("\n");
	}
};


#endif

