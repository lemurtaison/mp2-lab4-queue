#ifndef __TSTACK_H__
#define __TSTACK_H__

#include <iostream>
#include "TDataRoot.h"

template <class ValType>
class TStack :public TDataRoot<ValType>
{
protected:
	int Hi;// ������� �����
	int  IsValid()// ������������ ���������
	{
		int res = 0;
		if (pMem == nullptr)
			res++;
		if (MemSize < DataCount)
			res += 2;
		return res;
	}
	virtual int GetNextIndex(int index)
	{
		return index++;
	}
	virtual void refresh() = 0 {}
public:
//������������
	TStack(int Size = DefMemSize) :TDataRoot(Size), Hi(-1){};// ����������� �� ��������� � � ����������
	TStack(const TStack& St)// ����������� �����������
	{
		MemSize = St.MemSize;
		DataCount = St.DataCount;
		Hi = St.Hi;
		pMem = new ValType[MemSize];
		for (int i = 0;i < DataCount;++i)
			pMem[i] = St.pMem[i];

		SetRetCode(DataOK);
	}

//������
	void  Put(const ValType &Val)// �������� ��������
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsFull())// ��������� ���� �� DefMemSize ���������, ���� �� ������
		{
			refresh();
			void *p = nullptr;
			SetMem(p, MemSize + DefMemSize);
			pMem[Hi] = Val;
			DataCount++;

			SetRetCode(DataOK);
		}
		else// ������� ������� ��������
		{
			Hi = GetNextIndex(Hi);
			pMem[Hi] = Val;
			DataCount++;

			SetRetCode(DataOK);
		}

		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}
	ValType Get()// ������� ��������
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsEmpty())
			SetRetCode(DataEmpty);
		else // �������� ������� � ������� �����
		{
			ValType res = pMem[Hi--];
			DataCount--;
			if ((Hi>DefMemSize - 1) && (Hi%DefMemSize == 0)) // ���� ���� ������ ������� DefMemSize ����������� ������
			{
				void *p = nullptr;
				SetMem(p, MemSize - DefMemSize);
			}
			SetRetCode(DataOK);
			return res;
		}

		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}
	ValType GetHighElem()// �������� �������� � �������, �� �������� �� �����
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsEmpty())
			SetRetCode(DataEmpty);
		else
		{
			SetRetCode(DataOK);
			return pMem[Hi];
		}

		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}

	virtual void Print()// ������ ��������
	{
		if (pMem != nullptr)
		{
			for (int i = 0;i < DataCount;++i)
				std::cout << pMem[i] << ' ';
			printf("\n");

			SetRetCode(DataOK);
		}
		else
			throw SetRetCode(DataNoMem);

		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}

};

#endif
