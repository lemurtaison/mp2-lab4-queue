#pragma once
#include "Queue.h"
#include "TJobStream.h"
class Proc{
private:
	TJobStream JobStream;
	Queue Que;
	bool unlocked;
	int takts;
	int CountJob,Refusing,DownTime;
	int InProc;
public:
	Proc( int _q1, int _q2, int _size, int _tasks);
	void Service();
	void PrintResult();
};