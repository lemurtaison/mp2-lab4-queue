#include <iostream>
#include "Proc.h"

using namespace std;

Proc::Proc( int _q1, int _q2, int _size, int _takts):JobStream(_q1,_q2), 
												Que(_size), takts(_takts),
												unlocked(true)
{
	CountJob = 0;
	Refusing = 0;
	DownTime = 0;
}
void Proc:: Service(){
	int Id;
	if(Id =  JobStream.CreateJob())
	{
		Que.Put(Id);
		//���� ������� �����, �� ������� ������
		if(Que.GetRetCode()==DataFull)
			Refusing++;

	}
	if (unlocked){
		InProc = Que.Get();
		//���� ������� �����, �� ��������� �����������;
		if(Que.GetRetCode()==DataEmpty)
			DownTime++;
		else
		{
			CountJob++;
			unlocked = false;
		}
	}
	if(!unlocked)
		unlocked = JobStream.EndJob();
}
void Proc:: PrintResult(){
	cout<<endl;
}