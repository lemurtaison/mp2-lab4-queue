#pragma once

class TJobStream{
private:
int ID, q1,q2;

public:
	TJobStream(int q1, int q2);
	int CreateJob();
	bool EndJob();
	int GetID();

};