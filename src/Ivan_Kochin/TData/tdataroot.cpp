#include <stdio.h>
#include "tdataroot.h"

/*
PTelem - ��������� �� int ��� �������� �������
TData - int

PTelem PMem - ������ ��� �����
MemSize - ���� ���-�� ��������� (���-�� ���������� ������)
DataCount - ������� �����
MemType - MEM_HOLDER ��� MEM_RENTER ������ �� ����� ��������� ������
RetCode - ��� ����������
*/


/*��������������� �����������
RetCode == 0 �� ���������*/
TDataRoot::TDataRoot(int Size) :TDataCom(){
	if(Size<0)
		throw SetRetCode(DataNoMem);
	else{
	
		DataCount=0;
		MemSize = Size;
	if (Size==0){
		pMem=nullptr;
		MemType = MEM_RENTER;
	}
	else
	{
		pMem = new TElem [Size];
		MemType = MEM_HOLDER;
	}
		
	}
}

/*����������� �����������*/
TDataRoot::~TDataRoot(){
	delete [] pMem;
}

/*������� ������ ��� MEM_RENTER */
void TDataRoot :: SetMem(void *p, int Size) {
		if (Size < 0) 
			throw SetRetCode(DataNoMem);
		else if(MemType == MEM_HOLDER)
		{
			MemSize = Size;
			PTElem tmp = new TElem[MemSize];//������� ����� ������ ������� �������
			p = pMem;//��������� ������ ������ � void
			pMem = tmp;//����������� pMem ������ ������
			tmp = (PTElem)p; //�������� void � ������� ����
			for (int i = 0; i < DataCount; ++i)
			{
				pMem[i] = tmp[i];//����������� �����������
			}
			delete[] tmp;//������� ��������
		}else{
		MemSize = Size;
		for(int i =0;i<DataCount;++i){
			((PTElem)p)[i] = pMem[i];
		}
		pMem = (PTElem)p;
		
		}
}
/*������� �������� ������� � �������*/
bool TDataRoot::IsEmpty() const
{
	return DataCount==0;
}

bool TDataRoot::IsFull() const
{
	return DataCount==MemSize;
}
