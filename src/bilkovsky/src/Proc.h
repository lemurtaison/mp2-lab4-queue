#include "JobStream.h"
#include "C:\temp\mp2-lab4-queue\sln\queue\queue\queue.h"
#include <iostream>
using namespace std;

class Proc
{
private:
	JobStream Job;
	Queue queueH,queueL;
	int takts;
	int JobCount;
	int RefuseCountH,RefuseCountL;
	int DownTime;
	bool work;
public:
	Proc(int size,int a, int b, int count);
	void DoJob();
	void Print();
	int GetTakts() { return takts;}
};
Proc::Proc(int size,int a, int b, int count) : queueH(size),queueL(size), Job(a, b), takts(count)
{
	JobCount = 0;
	RefuseCountH = 0;
	RefuseCountL = 0;
	DownTime = 0;
	work = false;
}
void Proc::DoJob()
{
	if(Job.Task())
	{
		if(Job.GetPr() == 1)
			queueH.Put(Job.GetId());
		else
			queueL.Put(Job.GetId());
		if(queueH.GetRetCode() == DataFull)
			RefuseCountH++;
		if(queueL.GetRetCode() == DataFull)
			RefuseCountL++;
	}
	if(!work)
	{
		queueH.Get();
		if(queueH.GetRetCode() == DataEmpty)
		{
			queueL.Get();
			if(queueL.GetRetCode() == DataEmpty)
				DownTime++;
			else
			{
				JobCount++;
				work = true;
			}
		}
		else
		{
			JobCount++;
			work = true;
		}
	}
	if(work)
		work = !Job.IsDone();
}
void Proc::Print()
{
	cout <<" Count of takts " <<takts <<endl;
	cout <<" Tasks generated " <<Job.GetId() <<endl;
	cout <<" Tasks done " <<JobCount <<endl;
	cout <<" Count of downtime takts " <<DownTime <<endl;
	cout <<" Count of refused tasks " <<RefuseCountH + RefuseCountL <<endl;
	cout <<" Count of refused tasks with high priority " <<RefuseCountH <<endl;
	cout <<" Count of refused tasks with low priority " <<RefuseCountL <<endl;
	cout <<" Count of downtime takts in % " <<((double)DownTime / (double)takts) * 100 <<endl;
	cout <<" Count of refused tasks in % " <<((double)(RefuseCountH + RefuseCountL) / (double)Job.GetId()) * 100 <<endl;
}
