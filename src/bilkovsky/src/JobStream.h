#include <stdlib.h>
#include <time.h>
#include <iostream>
using namespace std;
class JobStream
{
private:
	int pr;
	int id;
	int q1;
	int q2;
public:
	JobStream(int a = 20, int b = 20);
	int Task();
	bool IsDone();
	int GetId(){ return id;}
	int GetPr() { return pr; }
};
JobStream::JobStream(int a, int b)
{
	id = 0;
	pr = 0;
	q1 = a;
	q2 = b;
	srand(time(NULL));
}
int JobStream::Task()
{
	int temp = 0;
	if(rand() % 100 < q1)
	{
		pr = rand() % 2 + 1;
		id++;
		temp = id;
	}
	return temp;
}
bool JobStream::IsDone()
{
	bool res = false;
	if(rand() % 100 < q2)
		res = true;
	return res;
}
