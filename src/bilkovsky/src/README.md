# Лабораторная работа №4 : Очередь

## Цели и задачи

Лабораторная работа направлена на практическое освоение динамической структуры данных Очередь. С этой целью в лабораторной работе изучаются различные варианты структуры хранения очереди и разрабатываются методы и программы решения задач с использованием очередей. В качестве области приложений выбрана тема эффективной организации выполнения потока заданий на вычислительных системах.
Очередь характеризуется таким порядком обработки значений, при котором вставка новых элементов производится в конец очереди, а извлечение – из начала. Подобная организация данных широко встречается в различных приложениях. В качестве примера использования очереди предлагается задача разработки системы имитации однопроцессорной ЭВМ. Рассматриваемая в рамках лабораторной работы схема имитации является одной из наиболее простых моделей обслуживания заданий в вычислительной системе и обеспечивает тем самым лишь начальное ознакомление с проблемами моделирования и анализа эффективности функционирования реальных вычислительных систем.
Очередь (англ. queue), – схема запоминания информации, при которой каждый вновь поступающий ее элемент занимает крайнее положение (конец очереди). При выдаче информации из очереди выдается элемент, расположенный в очереди первым (начало очереди), а оставшиеся элементы продвигаются к началу; следовательно, элемент, поступивший первым, выдается первым.

### Поставленные задачи
Для вычислительной системы (ВС) с одним процессором и однопрограммным последовательным режимом выполнения поступающих заданий требуется разработать программную систему для имитации процесса обслуживания заданий в ВС.
По результатам проводимых вычислительных экспериментов система имитации должна выводить информацию об условиях проведения эксперимента и полученные в результате имитации показатели функционирования вычислительной системы.

###Дополнительное задание
Реализовать очередь с приоритетмами.

## Реализация

### Реализация Очереди, класс Queue:
** Используемые методы и параметры **
Put(const TData &el) - Ставит элемент в конец очереди
Get() - Вынимает элемент из начала очереди
hi,li - указатели на последний и первый элемент массива соответственно

 '''
#ifndef __QUEUE__
#define __QUEUE__

#include "C:\temp\mp2-lab4-queue\sln\queue\queue\tdataroot.h"

class Queue : public TDataRoot
{
	private:
		int hi,li;
	public:
		Queue(int Size = DefMemSize);
		void Put(const TData &el);
		int Get();
		int GetNextIndex(int ind);
		int IsValid();
		void Print();
		int GetSize() { return DataCount; }
};
Queue::Queue(int Size) : TDataRoot( Size )
{
	hi = -1;
	li = 0;
}
int Queue::GetNextIndex(int ind)
{
	return ++ind % MemSize;
}
void Queue::Put(const TData &el)
{
	if(!IsValid())
		SetRetCode(DataNoMem);
	else
		if(IsFull())
		{
			SetRetCode(DataFull);
		}
		else
		{
			hi = GetNextIndex(hi);
			pMem[hi] = el;
			DataCount++;
		}
}
int Queue::Get()
{
	int temp = -1;
	if(!IsValid())
		SetRetCode(DataNoMem);
	else
		if(IsEmpty())
			SetRetCode(DataEmpty);
		else
		{
			temp = pMem[li];
			li = GetNextIndex(li);
			DataCount--;
		}
		return temp;
}
int Queue::IsValid()
{
	if(pMem == nullptr || MemSize < DataCount || DataCount < 0 || MemSize < 0)
		return 0;
	return 1;
}
void Queue::Print()
{
	for(int i = 0; i < DataCount; i++)
	{
		cout <<pMem[(li + i) % MemSize] <<" ";
	}
	cout << endl;
}
#endif
'''
### Тестирование очереди

'''
#include "C:\temp\mp2-lab4-queue\sln\queue\queue\queue.h"
#include <gtest.h>


TEST(Queue, can_fill_queue_to_the_max)
{
  Queue Q(3);
  for(int i = 0; i < 3; i++)
	  Q.Put(i);
  EXPECT_EQ(3, Q.GetSize());
}
TEST(Queue, can_fill_queue_not_to_the_max)
{
  Queue Q(3);
  for(int i = 0; i < 2; i++)
	  Q.Put(i);
  EXPECT_EQ(2, Q.GetSize());
}
TEST(Queue, can_get_element)
{
  Queue Q(3);
  for(int i = 0; i < 3; i++)
	  Q.Put(i);
  int a = Q.Get();
  int b = Q.Get();
  int c = Q.Get();
  EXPECT_EQ(a, 0);
  EXPECT_EQ(b, 1);
  EXPECT_EQ(c ,2);
}
TEST(Queue, can_put_element)
{
  Queue Q(3);
  for(int i = 0; i < 3; i++)
	  Q.Put(i);
  EXPECT_EQ(0, Q.Get());
  EXPECT_EQ(1, Q.Get());
  EXPECT_EQ(2, Q.Get());
}
TEST(Queue, buffer_cycle)
{
  Queue Q(3);
  for(int i = 0; i < 3; i++)
	  Q.Put(i);
  EXPECT_EQ(0 , Q.Get());
  Q.Put(4);
  EXPECT_EQ(1, Q.Get());
  EXPECT_EQ(2, Q.Get());
  EXPECT_EQ(4, Q.Get());
}
TEST (Queue, data_no_mem)
{
	Queue Q(0);
	Q.Get();
	EXPECT_EQ(DataNoMem, Q.GetRetCode());
	Q.Put(1);
	EXPECT_EQ(DataNoMem, Q.GetRetCode());
}
TEST (Queue, data_is_empty)
{
	Queue Q(3);
	Q.Get();
	EXPECT_EQ(DataEmpty, Q.GetRetCode());
	Q.Put(1);
	EXPECT_EQ(DataOK, Q.GetRetCode());
}
TEST (Queue, data_is_full)
{
	Queue Q(3);
	for(int i = 0; i < 3; i++)
		Q.Put(i);
	Q.Put(3);
	EXPECT_EQ(DataFull, Q.GetRetCode());
	Q.Get();
	EXPECT_EQ(DataOK, Q.GetRetCode());
	Q.Put(4);
	EXPECT_EQ(DataOK, Q.GetRetCode());
}
'''

## Реализация имитиация процессора

**Описание алгоритма**

Для моделирования момента появления нового задания используется значение датчика случайных чисел. Если значение датчика меньше некоторого порогового значения q1, 0<=q1<=99, то считается, что на данном такте имитации в вычислительную систему поступает новое задание.
Моделирование момента завершения обработки очередного задания также выполняется по описанной выше схеме. При помощи датчика случайных чисел формируется еще одно случайное значение, и если это значение меньше порогового значения q2, 0<=q2<=99, то принимается, что на данном такте имитации процессор завершил обслуживание очередного задания и готов приступить к обработке задания из очереди ожидания.

В процессоре используется две очереди для заданий с высоким и низким приоритетами. Со второй процессор начинает работать только если первая очередь пуста.

### Реализация потока заданий, класса JobStream
** Используемые методы и параметры **
q1 - вероятность поступления нового задания в %
q2 - вероятность выполенения текущего задания на этом такте в %
pr - приоритет задания ( 1 - высокий , 2 - низкий)
id - идентификатор, которым каждое задание однозначно определяется
Task() - метод генерирующий задание
IsDone() - метод проверяющий выполнено ли задание
'''
#include <stdlib.h>
#include <time.h>
#include <iostream>
using namespace std;
class JobStream
{
private:
	int pr;
	int id;
	int q1;
	int q2;
public:
	JobStream(int a = 20, int b = 20);
	int Task();
	bool IsDone();
	int GetId(){ return id;}
	int GetPr() { return pr; }
};
JobStream::JobStream(int a, int b)
{
	id = 0;
	pr = 0;
	q1 = a;
	q2 = b;
	srand(time(NULL));
}
int JobStream::Task()
{
	int temp = 0;
	if(rand() % 100 < q1)
	{
		pr = rand() % 2 + 1;
		id++;
		temp = id;
	}
	return temp;
}
bool JobStream::IsDone()
{
	bool res = false;
	if(rand() % 100 < q2)
		res = true;
	return res;
}
'''

## Реализация процессора, класс Proc
** Используемые методы и параметры **
queueH,queueL - очереди с высоким и низким приоритетом соответственно
JobCount - счетчик количества выполенных заданий
RefuseCountH,RefuseCountL - счетчики отклоененных заданий с разными приоритетами
DownTime - Счетчик тактов простоя процессора
work - индикатор занятости процессора
queue
'''
#include "JobStream.h"
#include "C:\temp\mp2-lab4-queue\sln\queue\queue\queue.h"
#include <iostream>
using namespace std;

class Proc
{
private:
	JobStream Job;
	Queue queueH,queueL;
	int takts;
	int JobCount;
	int RefuseCountH,RefuseCountL;
	int DownTime;
	bool work;
public:
	Proc(int size,int a, int b, int count);
	void DoJob();
	void Print();
	int GetTakts() { return takts;}
};
Proc::Proc(int size,int a, int b, int count) : queueH(size),queueL(size), Job(a, b), takts(count)
{
	JobCount = 0;
	RefuseCountH = 0;
	RefuseCountL = 0;
	DownTime = 0;
	work = false;
}
void Proc::DoJob()
{
	if(Job.Task())
	{
		if(Job.GetPr() == 1)
			queueH.Put(Job.GetId());
		else
			queueL.Put(Job.GetId());
		if(queueH.GetRetCode() == DataFull)
			RefuseCountH++;
		if(queueL.GetRetCode() == DataFull)
			RefuseCountL++;
	}
	if(!work)
	{
		queueH.Get();
		if(queueH.GetRetCode() == DataEmpty)
		{
			queueL.Get();
			if(queueL.GetRetCode() == DataEmpty)
				DownTime++;
			else
			{
				JobCount++;
				work = true;
			}
		}
		else
		{
			JobCount++;
			work = true;
		}
	}
	if(work)
		work = !Job.IsDone();
}
void Proc::Print()
{
	cout <<" Count of takts " <<takts <<endl;
	cout <<" Jobs generated " <<Job.GetId() <<endl;
	cout <<" Jobs done " <<JobCount <<endl;
	cout <<" Count of downtime takts " <<DownTime <<endl;
	cout <<" Count of refused tasks " <<RefuseCountH + RefuseCountL <<endl;
	cout <<" Count of refused tasks with high priority " <<RefuseCountH <<endl;
	cout <<" Count of refused tasks with low priority " <<RefuseCountL <<endl;
}
'''

Вывод: в результате работы были получены навыки работы со структурой данных Очередь. Были проведенны исследования зависимости кпд процессора при изменении таких параметров как: вероятность поступления новго задания, размер очереди, вероятность выполнения задания.