#include "C:\temp\mp2-lab4-queue\sln\queue\queue\queue.h"
#include <gtest.h>


TEST(Queue, can_fill_queue_to_the_max)
{
  Queue Q(3);
  for(int i = 0; i < 3; i++)
	  Q.Put(i);
  EXPECT_EQ(3, Q.GetSize());
}
TEST(Queue, can_fill_queue_not_to_the_max)
{
  Queue Q(3);
  for(int i = 0; i < 2; i++)
	  Q.Put(i);
  EXPECT_EQ(2, Q.GetSize());
}
TEST(Queue, can_get_element)
{
  Queue Q(3);
  for(int i = 0; i < 3; i++)
	  Q.Put(i);
  int a = Q.Get();
  int b = Q.Get();
  int c = Q.Get();
  EXPECT_EQ(a, 0);
  EXPECT_EQ(b, 1);
  EXPECT_EQ(c ,2);
}
TEST(Queue, can_put_element)
{
  Queue Q(3);
  for(int i = 0; i < 3; i++)
	  Q.Put(i);
  EXPECT_EQ(0, Q.Get());
  EXPECT_EQ(1, Q.Get());
  EXPECT_EQ(2, Q.Get());
}
TEST(Queue, buffer_cycle)
{
  Queue Q(3);
  for(int i = 0; i < 3; i++)
	  Q.Put(i);
  EXPECT_EQ(0 , Q.Get());
  Q.Put(4);
  EXPECT_EQ(1, Q.Get());
  EXPECT_EQ(2, Q.Get());
  EXPECT_EQ(4, Q.Get());
}
TEST (Queue, data_no_mem)
{
	Queue Q(0);
	Q.Get();
	EXPECT_EQ(DataNoMem, Q.GetRetCode());
	Q.Put(1);
	EXPECT_EQ(DataNoMem, Q.GetRetCode());
}
TEST (Queue, data_is_empty)
{
	Queue Q(3);
	Q.Get();
	EXPECT_EQ(DataEmpty, Q.GetRetCode());
	Q.Put(1);
	EXPECT_EQ(DataOK, Q.GetRetCode());
}
TEST (Queue, data_is_full)
{
	Queue Q(3);
	for(int i = 0; i < 3; i++)
		Q.Put(i);
	Q.Put(3);
	EXPECT_EQ(DataFull, Q.GetRetCode());
	Q.Get();
	EXPECT_EQ(DataOK, Q.GetRetCode());
	Q.Put(4);
	EXPECT_EQ(DataOK, Q.GetRetCode());
}

