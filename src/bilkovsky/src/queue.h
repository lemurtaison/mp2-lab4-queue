#ifndef __QUEUE__
#define __QUEUE__

#include "C:\temp\mp2-lab4-queue\sln\queue\queue\tdataroot.h"

class Queue : public TDataRoot
{
	private:
		int hi,li;
	public:
		Queue(int Size = DefMemSize);
		void Put(const TData &el);
		int Get();
		int GetNextIndex(int ind);
		int IsValid();
		void Print();
		int GetSize() { return DataCount; }
};
Queue::Queue(int Size/* = DefMemSize*/) : TDataRoot( Size )
{
	hi = -1;
	li = 0;
}
int Queue::GetNextIndex(int ind)
{
	return ++ind % MemSize;
}
void Queue::Put(const TData &el)
{
	if(!IsValid())
		SetRetCode(DataNoMem);
	else
		if(IsFull())
		{
			SetRetCode(DataFull);
		}
		else
		{
			hi = GetNextIndex(hi);
			pMem[hi] = el;
			DataCount++;
		}
}
int Queue::Get()
{
	int temp = -1;
	if(!IsValid())
		SetRetCode(DataNoMem);
	else
		if(IsEmpty())
			SetRetCode(DataEmpty);
		else
		{
			temp = pMem[li];
			li = GetNextIndex(li);
			DataCount--;
		}
		return temp;
}
int Queue::IsValid()
{
	if(pMem == nullptr || MemSize < DataCount || DataCount < 0 || MemSize < 0)
		return 0;
	return 1;
}
void Queue::Print()
{
	for(int i = 0; i < DataCount; i++)
	{
		cout <<pMem[(li + i) % MemSize] <<" ";
	}
	cout << endl;
}
#endif