#include "TJobStream.h"
#include <fstream>
#include <time.h>

using namespace std;

TJobStream::TJobStream(int _q1, int _pr, int _qsize) : TQueue(_qsize)
{
	lastID = 0;
	q1 = _q1;
	pr = _pr;
	srand(time(NULL));
}

void TJobStream::CreateJob()
{
	int _q1 = (rand() % 100);
	int _pr = (rand() % 100);
	if (_q1 < q1)
	{
		lastID++;
		if (!IsFull())
			if (_pr >= pr)
				Put(lastID);
			else
			{
				PutInHead(lastID);
				highprioritycount++;
			}
		else
		{
			if (_pr < pr)
			{
				PutInHead(lastID);
				highprioritycount++;
				throwawaycount++;
			}
			refusalcount++;
		}
	}
}
