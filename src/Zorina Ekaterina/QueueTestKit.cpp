#include "tqueue.h"

#include <gtest.h>

TEST(TQueue, can_create_queue_with_positive_length) 
{
	ASSERT_NO_THROW(TQueue q(5));
}

TEST(TQueue, throws_when_create_queue_with_negative_length)
{
	ASSERT_ANY_THROW(TQueue q(-1));
}

TEST(TQueue, created_queue_is_empty) 
{
	TQueue q(4);

	EXPECT_TRUE(q.IsEmpty());
}

TEST(TQueue, can_put_element)
{
	TQueue q;
	q.Put(0);

	EXPECT_FALSE(q.IsEmpty());
}

TEST(TQueue, can_get_element)
{
	TQueue q;
	int num = 6;
	q.Put(num);

	EXPECT_EQ(num, q.Get());
}

TEST(TQueue, cant_get_from_empty_queue)
{
	TQueue q;

	EXPECT_ANY_THROW(q.Get());
}

TEST(TQueue, can_put_element_to_full_queue)
{
	TQueue q(1);
	q.Put(1);
	q.Put(2);

	EXPECT_EQ(1, q.Get());
}

TEST(TQueue, get_returns_first_placed_elem)
{
	TQueue q;
	q.Put(1);
	q.Put(2);
	q.Put(3);

	EXPECT_EQ(1, q.Get());
}

TEST(TQueue, can_put_elem_in_head)
{
	TQueue q;
	q.Put(1);
	q.PutInHead(2);

	EXPECT_EQ(2, q.Get());
}
