#include "tstack.h"
#include <iostream>
using namespace std;


void TStack::Put(const TData &val) 
{
	if (pMem == nullptr) 
		throw SetRetCode(DataNoMem);
	else if (IsFull())
		SetMem(pMem, MemSize + DefMemSize);
	pMem[++Li] = val;
	DataCount++;
}

TData TStack::Get(void)
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	if (IsEmpty())	
		throw SetRetCode(DataEmpty);
	DataCount--;
	return pMem[Li--];
}

TData TStack::Top()
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	if (IsEmpty())
		SetRetCode(DataEmpty);
	return pMem[Li];
}

void TStack::Print() 
{
	if (DataCount > 0)
		for (int i = 0; i < DataCount; i++)
			cout << pMem[i] << " ";
	cout << endl;
}