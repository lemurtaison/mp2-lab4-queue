// Динамические структуры данных - стек

#ifndef _DATSTACK_H_
#define _DATSTACK_H_

#include "tdataroot.h"

class TStack: public TDataRoot
{
	protected:
		int Hi; // индекс последнего элемента структуры
		virtual int GetNextIndex(int Index);
	public: 
		TStack(int Size = DefMemSize): TDataRoot(Size)
		{
			Hi = -1;
		}
		virtual void Push(const TData &Val); // положить в стек
		virtual TData Pop(void); //взять из стека
		virtual TData TopElem(void); // посмотреть значение вершины стека
	protected:
		// служебные методы
		virtual int IsValid(void);	// тестирование структуры
		virtual void Print(void);	// печать значений
};
#endif