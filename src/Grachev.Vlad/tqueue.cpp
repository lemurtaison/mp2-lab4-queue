// ������������ ��������� ������ -  �������

#include <stdio.h>
#include "tqueue.h"

void TQueue::Put(const TData &Val) //�������� ������� � �������
{
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if (IsFull())
			SetRetCode(DataFull);
	else
	{
		Hi = GetNextIndex(Hi);
		pMem[Hi] = Val;
		DataCount++;
	}
}

/*-------------------------------------------*/

TData TQueue::Get(void) // ����� ������� �� �������
{
	TData temp = -1;
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
	if (IsEmpty())
		SetRetCode(DataEmpty);
	else
	{
		Li = GetNextIndex(Li);
		temp = pMem[Li];
		DataCount--;
	}
	return temp;
}

/*-------------------------------------------*/

int TQueue::GetNextIndex(int index) // �������� ��������� �������� �������
{
	if (index + 1 == MemSize)
		return 0;
	else
		return ++index;
}