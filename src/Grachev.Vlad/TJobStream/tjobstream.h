// �����, ����������� ����� �������

#ifndef _TJOBSTRM_H_
#define _TJOBSTRM_H_

#include <iostream>
#include <locale>
#include "tproc.h"

using namespace std;

class TJobStream
{
	private:
		TProc CPunit;
		long tactsNum; // ����������������� ������ � ������
		long taskID; // ������������� ������� / ����� ����� ������� 
		long cmpltd_tasks = 0; // ����� ����������� �������
		long idle_tacts = 0; // ����� ������ �������
		long deniels = 0; // ����� ������� � ������������
		int intense; // ����������� ��������� ������� �� ������� �����
	public:
		TJobStream(double proc_perf, int pipeline_len, long tactsNum, int intns) : 
			CPunit(pipeline_len, proc_perf), tactsNum(tactsNum), intense(intns) {};
		void StartStream(); // ��������� �����
		void PrintReport(); // ������ ������ � ������
};
#endif